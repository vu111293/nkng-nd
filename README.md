# loctroi

LOCTROI project on the FLUTTER platform

## How to build Android

1. Make clean and refresh pub libs
2. `flutter build appbundle --release --flavor client -t lib/main-client.dart`
3. `flutter build appbundle --release --flavor doctor -t lib/main-doctor.dart`

## How to build iOS

1. Clean and refresh pub libs
2. cd ios and pod get/install
3. cd ..
4. `flutter build ios --flavor client`
5. Using xcode or build and release

// **\* PLEASE RUN COMMAND BELOW FOR REBUILD MODELS \*\***
// flutter packages pub run build_runner build --delete-conflicting-outputs

flutter build apk --release --flavor client -t lib/main-client.dart
flutter build apk --release --flavor doctor -t lib/main-doctor.dart
