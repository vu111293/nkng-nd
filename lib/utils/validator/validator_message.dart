// Validation messages
final invalidEmailMessage = 'Invalid email';
final invalidPasswordMessage = 'Invalid password';
final invalidPasswordFormatMessage = 'Password requires minimum 6 characters';

final failedLoginMessage = 'Your email or password is not correct';

final invalidPhoneNumberFormatMessage = 'Phone number is wrong format';
final invalidPhoneNumberMessage = 'Invalid phone number';

final invalidIdNoFormatMessage = 'Id no is wrong format';
final invalidIdNoMessage = 'Invalid Id no';

