import 'package:loctroi/utils/app_store.dart';

class MapUtils {
  static openMap(double latitude, double longitude) async {
    String googleUrl = 'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not open the map.';
    }
  }
}

class Utils {
  static String securityPhone(String phone) {
    print('security nè');
    String phoneResult = phone.trim();
    int maxIndex = phoneResult.length;
    String strX = '';
    for (int i = 0; i < maxIndex - 3; i++) {
      strX += 'x';
    }
    phoneResult = strX + phoneResult.substring(maxIndex - 3, maxIndex);

    return phoneResult;
  }
}

bool compareContainsNoneAccent(String a, String b) {
  return removeDiacritics(a.toLowerCase()).contains(removeDiacritics(b.toLowerCase()));
}
