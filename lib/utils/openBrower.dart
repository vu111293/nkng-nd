import 'package:loctroi/utils/app_store.dart';

///not support simulator IOS
openWebBrowserhURL(url) async {
  if (url != '') {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
