import 'package:loctroi/utils/app_store.dart';
import 'package:timeago/timeago.dart' as timeago;

String formatTime(String time) {
  var result = timeago.format(DateTime.parse(time).toLocal(), locale: 'vi');
  if (result == "một thoáng trước") {
    return "khoảng một phút trước";
  }

  return result;
}

String formatDate(String date) {
  if (date != null && date.isNotEmpty) {
    return new DateFormat("dd/MM/yyyy").format(DateTime.parse(date))?.toString();
  } else {
    return '';
  }
}
