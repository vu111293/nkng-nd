library app_store;

export 'dart:async';
export 'dart:convert';
export 'dart:io';
export 'dart:math';
export 'package:webview_flutter/webview_flutter.dart';

export 'package:cached_network_image/cached_network_image.dart';
export 'package:carousel_slider/carousel_slider.dart';
export 'package:device_info/device_info.dart';
export 'package:extended_image/extended_image.dart';
export 'package:firebase_messaging/firebase_messaging.dart';
export 'package:flutter/foundation.dart';
export 'package:flutter/gestures.dart';
export 'package:flutter/material.dart';
export 'package:flutter/services.dart' hide MessageHandler;
export 'package:flutter_spinkit/flutter_spinkit.dart';
export 'package:font_awesome_flutter/font_awesome_flutter.dart';
export 'package:get_it/get_it.dart';
export 'package:google_fonts/google_fonts.dart';
export 'package:google_maps_flutter/google_maps_flutter.dart';
export 'package:image_picker/image_picker.dart';
export 'package:intl/intl.dart' hide TextDirection;

///apis
export 'package:loctroi/apis/core/auth_service.dart';
export 'package:loctroi/apis/net/image_service.dart';
export 'package:loctroi/apis/net/intro_team_service.dart';
export 'package:loctroi/apis/net/noti_service.dart';
export 'package:loctroi/apis/net/post_service.dart';
export 'package:loctroi/apis/net/user_service.dart';

///blocs
export 'package:loctroi/blocs/application_bloc.dart';
export 'package:loctroi/blocs/bloc_provider.dart';

///component
export 'package:loctroi/component/button/button_icon_change.dart';
export 'package:loctroi/component/button/button_icon_circle.dart';
export 'package:loctroi/component/button/button_normal.dart';
export 'package:loctroi/component/button/qrcode_button.dart';
export 'package:loctroi/component/circle_image/circle_image.dart';
export 'package:loctroi/component/data_empty.dart';
export 'package:loctroi/component/painter/custom_painter.dart';
export 'package:loctroi/component/scale_animation.dart';

///constants
export 'package:loctroi/constants/strings.dart';

///helper
export 'package:loctroi/helper/phone_auth_helper.dart';

///models
export 'package:loctroi/models/business_models.dart';
export 'package:loctroi/models/filter_models.dart';
export 'package:loctroi/models/notification_model.dart';
export 'package:loctroi/models/notification_model.dart';
export 'package:loctroi/models/response_models.dart';
export 'package:loctroi/models/user.dart';

///style
export 'package:loctroi/style/colors.dart';
export 'package:loctroi/style/responsive/app_size.dart';
export 'package:loctroi/style/responsive/responsive_size.dart';
export 'package:loctroi/style/text_style.dart';

///ui
export 'package:loctroi/ui/customs/base_header.dart';
export 'package:loctroi/ui/customs/dialog.dart';
export 'package:loctroi/ui/customs/doctor_filter_question.dart';
export 'package:loctroi/ui/customs/fieldInfo_widget.dart';
export 'package:loctroi/ui/customs/iconBack.dart';
export 'package:loctroi/ui/customs/webview_screen.dart';
export 'package:loctroi/ui/customs/zoom_webview.dart';
export 'package:loctroi/ui/pages/document/document_detail_screen.dart';
export 'package:loctroi/ui/pages/document/document_list.dart';
export 'package:loctroi/ui/pages/exchange_gifts_page.dart';
export 'package:loctroi/ui/pages/gift_redemption_history_page.dart';
export 'package:loctroi/ui/pages/home_screen.dart';
export 'package:loctroi/ui/pages/login/change_password_screen.dart';
export 'package:loctroi/ui/pages/login/confirm_number_phone_screen.dart';
export 'package:loctroi/ui/pages/login/login_screen.dart';
export 'package:loctroi/ui/pages/login/update_info_login_screen.dart';
export 'package:loctroi/ui/pages/profile/detail_notification_screen.dart';
export 'package:loctroi/ui/pages/profile/info_user_screen.dart';
export 'package:loctroi/ui/pages/profile/menu_screen.dart';
export 'package:loctroi/ui/pages/profile/notification_screen.dart';
export 'package:loctroi/ui/pages/purchase_history_page.dart';
export 'package:loctroi/ui/pages/spin_wheel_page.dart';
export 'package:loctroi/ui/route/routing.dart';

///utils
export 'package:loctroi/utils/app_store.dart';
export 'package:loctroi/utils/currency_utils.dart';
export 'package:loctroi/utils/formatTime.dart';
export 'package:loctroi/utils/hex_color.dart';
export 'package:loctroi/utils/network_check.dart';
export 'package:loctroi/utils/openBrower.dart';
export 'package:loctroi/utils/scale_util.dart';
export 'package:loctroi/utils/utils.dart';

///widgets
export 'package:loctroi/widgets/image_picker.dart';
export 'package:loctroi/widgets/pic_swiper.dart';
export 'package:loctroi/widgets/zoom_image_asset.dart';

///libs
export 'package:package_info/package_info.dart';
export 'package:provider/provider.dart';
export 'package:provider/single_child_widget.dart';
export 'package:qr_flutter/qr_flutter.dart';
export 'package:rxdart/rxdart.dart' hide Notification;
export 'package:rxdart/subjects.dart';
export 'package:store_redirect/store_redirect.dart';
export 'package:toast/toast.dart';

export 'package:bot_toast/bot_toast.dart';
export 'package:flutter_localizations/flutter_localizations.dart';
export 'package:loctroi/ui/pages/splash_screen.dart';
export 'package:loctroi/ui/route/navigator_key.dart';
export 'package:shared_preferences/shared_preferences.dart';
export 'package:path_provider/path_provider.dart';
export 'package:connectivity/connectivity.dart';
export 'package:url_launcher/url_launcher.dart';
export 'package:diacritic/diacritic.dart';
export 'package:firebase_crashlytics/firebase_crashlytics.dart';
export 'package:loctroi/ui/pages/advisory_screen.dart';
export 'package:loctroi/ui/pages/document/document_screen.dart';
export 'package:loctroi/style/arc_clipper.dart';
export 'package:flutter_html/flutter_html.dart';
export 'package:logger/logger.dart';
export 'package:loctroi/utils/logCode.dart';
