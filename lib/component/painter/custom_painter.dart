import 'package:loctroi/utils/app_store.dart';

class MyPainter extends CustomPainter {
  final ResponsiveSize reSize;

  MyPainter({this.reSize});

  @override
  void paint(Canvas canvas, Size size) {
    final height = size.height;
    final width = size.width;
    final space = reSize.height * 0.185;
    final paint = Paint()
      ..color = PColor.primary
      ..imageFilter
      ..strokeWidth = 0.5;

    canvas.drawLine(
      Offset(width / 2, reSize.setByHeight(100)),
      Offset(width / 2, height - reSize.setByHeight(40)),
      paint,
    );

    canvas.drawLine(
      Offset(reSize.setByWidth(40), height - space),
      Offset(width - reSize.setByWidth(40), height - space),
      paint,
    );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
