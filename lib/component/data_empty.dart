import 'package:loctroi/constants/assets.dart';
import 'package:loctroi/style/responsive/app_size.dart';
import 'package:loctroi/utils/app_store.dart';
import 'package:flutter/material.dart';

class EmptyDataWidget extends StatelessWidget {
  final title;
  EmptyDataWidget(this.title);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: SizeConfig.heightMultiplier * 15,
          ),
          Image(
            image: AssetImage(Assets.dataEmpty),
            height: SizeConfig.heightMultiplier * 15,
          ),
          SizedBox(
            height: SizeConfig.heightMultiplier * 1.5,
          ),
          Opacity(
            child: Text(
              title,
              style: TextStyle(
                fontSize: SizeConfig.textMultiplier * 2.2,
              ),
            ),
            opacity: .5,
          ),
        ],
      ),
    );
  }
}
