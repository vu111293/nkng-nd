import 'package:loctroi/utils/app_store.dart';

class PButtonRoundBorder extends StatelessWidget {
  String text;
  double height;
  Color color;
  Function onPress;
  PButtonRoundBorder(this.text, this.height, this.color, {this.onPress});
  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      height: height,
      child: RaisedButton(
        color: color,
        shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
        onPressed: onPress ?? () {},
        child: Text(
          text,
          style: TextStyle(
              fontSize: SizeConfig.textMultiplier * 2.6, color: PColor.lightText, fontWeight: FontWeight.w400),
        ),
      ),
    );
  }
}
