import 'package:loctroi/utils/app_store.dart';
import 'package:dio/dio.dart';

class PButtonQrCode extends StatelessWidget {
  final ResponsiveSize size;
  double width;
  final VoidCallback onPressed;

  PButtonQrCode({Key key, this.size, this.width, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    width = size.width / 3.75 + size.setByHeight(12);

    return InkWell(
      onTap: () {
        onPressed();
      },
      child: Container(
        padding: EdgeInsets.all(size.setByHeight(12)),
        width: size.width / 3.75,
        height: size.width / 3.5,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
              blurRadius: 8,
              offset: Offset.zero,
              color: Colors.grey.withOpacity(.5),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Image.asset(
              'assets/qrcode.png',
              fit: BoxFit.cover,
              width: size.width / 6,
            ),
            Text(
              'Quét mã',
              style: TextStyle(
                fontSize: size.setByHeight(16),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
