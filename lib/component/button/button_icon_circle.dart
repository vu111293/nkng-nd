import 'package:loctroi/utils/app_store.dart';

class PButtonIconCircle extends StatelessWidget {
  final VoidCallback onPressed;
  final IconData icon;
  Color color;
  Color iconColor;
  double size;

  PButtonIconCircle({
    @required this.onPressed,
    @required this.icon,
    this.color = Colors.transparent,
    this.iconColor = Colors.white,
    this.size = 36,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(size / 2),
        ),
        color: color,
      ),
      height: size,
      width: size,
      child: FlatButton(
        padding: const EdgeInsets.all(1),
        shape: const CircleBorder(),
        color: color,
        onPressed: onPressed,
        child: Icon(
          icon,
          size: size * 0.6,
          color: iconColor,
        ),
      ),
    );
  }
}
