import 'package:loctroi/style/btn_style.dart';
import 'package:loctroi/utils/app_store.dart';

class PButtonNormal extends StatelessWidget {
  final VoidCallback onPressed;
  final double width;
  final double height;
  final Color color;
  final String title;
  final TextStyle textStyle;
  final double radius;
  final double elevation;
  final Color disabledColor;
  final IconData icon;

  PButtonNormal({
    @required this.onPressed,
    this.title,
    this.textStyle,
    this.height,
    this.width,
    this.color = PColor.primary,
    this.radius = 4,
    this.elevation,
    this.disabledColor,
    this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      child: RaisedButton(
        disabledColor: disabledColor,
        onPressed: onPressed,
        color: color,
        elevation: elevation,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(radius),
        ),
        child: Row(
          children: <Widget>[
            icon == null ? SizedBox.shrink() : Icon(icon, color: textStyle.color ?? Colors.black),
            icon == null
                ? SizedBox.shrink()
                : SizedBox(
                    width: SizeConfig.widthMultiplier,
                  ),
            Text(
              title,
              style: textStyle ?? PButtonStyle.normal(),
            ),
          ],
        ),
      ),
    );
  }
}
