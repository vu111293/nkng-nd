import 'package:loctroi/utils/app_store.dart';

class PButtonIconChange extends StatelessWidget {
  final VoidCallback onPressed;
  final IconData icon1;
  final IconData icon2;
  final bool isChange;
  final double size;

  PButtonIconChange({
    @required this.onPressed,
    @required this.icon1,
    @required this.icon2,
    @required this.isChange,
    this.size = 36,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size,
      width: size,
      child: FlatButton(
        padding: const EdgeInsets.all(1),
        shape: const CircleBorder(),
        onPressed: onPressed,
        child: isChange
            ? Icon(
                icon2,
                size: size * 0.6,
                color: Colors.white,
              )
            : Icon(
                icon1,
                size: size * 0.6,
                color: Colors.white,
              ),
      ),
    );
  }
}
