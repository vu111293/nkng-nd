import 'package:loctroi/utils/app_store.dart';

class PButtonCancel extends StatelessWidget {
  PButtonCancel({this.onPressed});
  Function onPressed;
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: PColor.alert,
      shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
      onPressed: onPressed ?? () {},
      child: Text(
        "Hủy",
        style: TextStyle(color: PColor.lightText, fontWeight: FontWeight.w500),
      ),
    );
  }
}
