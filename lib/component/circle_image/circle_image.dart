import 'package:flutter/material.dart';

class PCircleImage extends StatelessWidget {
  final double size;
  final ImageProvider image;
  final BoxFit fit;
  final Function onPressed;

  PCircleImage({
    this.size = 40,
    @required this.image,
    this.fit = BoxFit.cover,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
            image: image,
            fit: fit,
          ),
        ),
      ),
    );
  }
}
