import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';
import 'package:loctroi/apis/net/file_service.dart';
import 'package:loctroi/models/base_model.dart';
import 'package:loctroi/models/user.dart';

class FileBloc {
  FileService _fileService;

  BehaviorSubject _fileSubject = new BehaviorSubject();

  Observable get fileObservable => _fileSubject.stream;

  FileBloc() {
    _fileService = FileService();
  }

  Future<String> uploadImage(String localUri) async {
    return await _fileService.uploadFile(localUri, 'image');
  }

  Future<String> uploadVideo(String localUri) async {
    return await _fileService.uploadFile(localUri, 'video');
  }

  dispose() {
    _fileSubject.close();
  }
}
