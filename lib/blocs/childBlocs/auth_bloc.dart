import 'package:loctroi/apis/net/business_service.dart';
import 'package:loctroi/models/business_models.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';
import 'package:loctroi/apis/net/auth_service.dart';
import 'package:loctroi/models/base_model.dart';
import 'package:loctroi/models/user.dart';

import 'file_bloc.dart';

class AuthBloc {
  AuthService _authService;
  UserModel currentUser;
  BehaviorSubject _authSubject = new BehaviorSubject();
  Observable get authObservable => _authSubject.stream;

  final _userSubject = BehaviorSubject<Farmer>();

  // signal
  Function(Farmer) get updateUserAction => _userSubject.sink.add;

  // trigger
  Stream<Farmer> get userInfoEvent => _userSubject.stream;
  Farmer get getUser => _userSubject.stream.value;

  final _unreadCount$ = BehaviorSubject<int>();
  Stream<int> get unreadStream => _unreadCount$.stream;
  Sink<int> get unreadSink => _unreadCount$.sink;

  AuthBloc() {
    _authService = AuthService();
    currentUser = null;
    _authSubject.sink.add(currentUser);
  }

  void getUnread() {
    getUnreadCount().then((onValue) => _unreadCount$.sink.add(onValue));
  }

  Future<UserModel> loginWithFacebookAccounntKit(String token) async {
    UserModel temp = await _authService.loginWithFacebookAccounntKit(token);
    _authSubject.sink.add(temp);
    return temp;
  }

  Future<UserModel> loginWithEmailAndPassword(String email, String password) async {
    UserModel temp = await _authService.loginWithEmailAndPassword(email, password);
    _authSubject.sink.add(temp);
    return temp;
  }

  Future<UserModel> registerWithEmailAndPassword(String email, String password) async {
    UserModel temp = await _authService.registerWithEmailAndPassword(email, password);
    print("Temp ne: " + temp.toString());
    currentUser = temp;
    print("Current ne: " + currentUser.toString());
    _authSubject.sink.add(currentUser);
    return temp;
  }

  Future<bool> forgetPassword(String email) async {
    bool temp = await _authService.forgetPassword(email);
    return temp;
  }

  Future<AccessToken> getAccessToken() async {
//    AccessToken temp = await _authService.getAccessToken();
//    if (currentUser == null) {
//      currentUser = temp.user;
//      _authSubject.sink.add(currentUser);
//    }
    return null;
  }

  Future<UserModel> getCurrentUser() async {
    AccessToken temp = await getAccessToken();
    return temp.user;
  }

  Future<bool> isLoggedIn() async {
    AccessToken temp = await getAccessToken();
    return temp != null && temp.id != null && temp.id.isNotEmpty;
  }

  Future<bool> updateUserData(AccessToken accessToken, String id) async {
    bool updateSucces = await _authService.updateUserData(accessToken, id);
    return updateSucces;
  }

  void logout() {
    _authService.logout();
    currentUser = null;
    _authSubject.sink.add(currentUser);
  }

  Future updateSettingUser(String id, Map<String, dynamic> body) async {
    UserModel newUpdateSettingUser = await _authService.updateSettingUser(id, body);
    currentUser = newUpdateSettingUser;
    _authSubject.sink.add(newUpdateSettingUser);
  }

  Future<bool> uploadUserInformationToServer(FileBloc fileBloc) async {
    print("iden3: " + currentUser.toString());
    //Upload image if needed
    if (currentUser.identity_video != null &&
        currentUser.identity_video.isNotEmpty &&
        !currentUser.identity_video.contains("http")) {
      print("video ok ne");
      currentUser.identity_video = await fileBloc.uploadVideo(currentUser.identity_video);
    }
    if (currentUser.avatar != null && currentUser.avatar.isNotEmpty && !currentUser.avatar.contains("http")) {
      currentUser.avatar = await fileBloc.uploadImage(currentUser.avatar);
    }
    if (currentUser.avatar_company != null &&
        currentUser.avatar_company.isNotEmpty &&
        !currentUser.avatar_company.contains("http")) {
      currentUser.avatar_company = await fileBloc.uploadImage(currentUser.avatar_company);
    }
    if (currentUser.national_id_front_image != null &&
        currentUser.national_id_front_image.isNotEmpty &&
        !currentUser.national_id_front_image.contains("http")) {
      currentUser.national_id_front_image = await fileBloc.uploadImage(currentUser.national_id_front_image);
    }
    if (currentUser.national_id_back_image != null &&
        currentUser.national_id_back_image.isNotEmpty &&
        !currentUser.national_id_back_image.contains("http")) {
      currentUser.national_id_back_image = await fileBloc.uploadImage(currentUser.national_id_back_image);
    }

    print("iden4: " + currentUser.toString());

    //Upload to server
    bool isSuccess = await _authService.uploadUserInformationToServer(currentUser);
    if (isSuccess) {
      _authSubject.sink.add(currentUser);
    }
    return isSuccess;
  }

  void updateIndentityInformationTemporarily(String frontImage, String backImage, String video) async {
    //currentUser = await getCurrentUser();
    print("IDEN: " + currentUser.toString());
    if (currentUser != null) {
      print("video temp: " + video);
      currentUser.identity_video = video;
      currentUser.national_id_front_image = frontImage;
      currentUser.national_id_back_image = backImage;
      print("currrent user: " + currentUser.toString());
    } else {
      print("Lỗi");
      print("currrent user: " + currentUser.toString());
    }
  }

  void updateCompanyInformationtemperarily(String avatar, String name, String email, String phoneNumber) async {
    currentUser = await getCurrentUser();
    if (currentUser != null) {
      currentUser.avatar_company = avatar;
      currentUser.name_company = name;
      currentUser.contact_email_company = email;
      currentUser.contact_phone_company = phoneNumber;
    }
  }

  void updateProfileTemporarily(String avatar, String name, String sex, String location, String cityCode,
      String identityNumber, String email, DateTime dob, String experience, String workingHour, String income) async {
    if (avatar.isEmpty) {
      currentUser = await getCurrentUser();
    }
    print('IDEN1: ' + currentUser.toString());
    print(
        ' phat $avatar - $name - $sex - $location - $identityNumber - $email - $dob - $experience - $workingHour - $income');
    if (currentUser != null) {
      print("OK");
      currentUser.avatar = avatar;
      currentUser.fullname = name;
      currentUser.sex = sex;
      currentUser.local = location;
      currentUser.city_code = cityCode;
      currentUser.national_id = identityNumber;
      currentUser.email = email;
      currentUser.date_of_birth = dob.toString();
      currentUser.experience = experience;
      currentUser.working_hour = workingHour;
      currentUser.expected_salary = income;
      print('IDEN2: ' + currentUser.toString());
      _authSubject.sink.add(currentUser);
    } else {
      print("Lõi");
    }
  }

  Future<int> getUnreadCount() async {
    List<NotificationData> listNoti = await BusinessService().getNotificationList();
    int unseen = 0;
    for (var i = 0; i < listNoti.length; i++) {
      if (listNoti[i].seen != null) {
        if (!listNoti[i].seen) {
          unseen++;
        }
      }
    }
    return unseen;
  }

  dispose() {
    _userSubject.close();
    _authSubject.close();
    _unreadCount$.close();
  }
}
