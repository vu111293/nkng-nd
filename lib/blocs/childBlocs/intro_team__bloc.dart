import 'package:loctroi/apis/net/intro_team_service.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';

class IntroTeamBloc {
  IntroTeamService _introTeamService;

  ///observable:
  BehaviorSubject _introTeamSubject = new BehaviorSubject();

  ///getter:
  Observable get introTeamObservable => _introTeamSubject.stream;

  IntroTeamBloc() {
    _introTeamService = IntroTeamService();
  }

  Future loadIntroTeam() async {
    String tempList = await _introTeamService.getIntroTeam();
    print("phat ket qua" + tempList.toString());
    return tempList;
  }

  dispose() {
    _introTeamSubject.close();
  }
}
