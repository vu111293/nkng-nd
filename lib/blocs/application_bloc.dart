import 'dart:async';

import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:get_it/get_it.dart';
import 'package:loctroi/apis/net/business_service.dart';
import 'package:loctroi/apis/net/post_service.dart';
import 'package:loctroi/app.dart';
import 'package:loctroi/blocs/bloc_provider.dart';
import 'package:loctroi/blocs/childBlocs/intro_team__bloc.dart';
import 'package:loctroi/blocs/childBlocs/main_screen_bloc.dart';
import 'package:loctroi/models/business_models.dart';
import 'package:loctroi/models/filter_models.dart';
import 'package:loctroi/models/notification_model.dart';
import 'package:loctroi/models/response_models.dart';
import 'package:loctroi/utils/utils.dart';
import 'package:rx_command/rx_command.dart';
import 'package:rxdart/rxdart.dart';

import 'childBlocs/auth_bloc.dart';

class ApplicationBloc implements BlocBase {
  AuthBloc _authBloc;
  IntroTeamBloc _introTeamBloc;
  int countChangedMonney = 1;

  RxCommand<void, bool> setupCommand;
  Observable<Exception> setupExceptionStream;
  RxCommand<void, void> networkMonitorCommand;
  RxCommand<bool, bool> networkStatusCommand;
  RxCommand<int, int> moneyChagedCommand;

  final _setupStateSubject = PublishSubject<String>();
  final _doctorSubject = BehaviorSubject<List<LDoctor>>();
  final _hospitalSubject = BehaviorSubject<List<LHospital>>();
  final _treeSubject = BehaviorSubject<List<LTree>>();
  final _popularQuestionSubject = BehaviorSubject<List<LQuestionResponse>>();
  final _myQuestionSubject = BehaviorSubject<List<LQuestionResponse>>();
  final _questionFilterSubject = BehaviorSubject<ClientQuestionFilter>.seeded(ClientQuestionFilter(keyword: '', isMyQuestion: false));
  final _questionDoctorFilterSubject = BehaviorSubject<DoctorQuestionFilter>.seeded(DoctorQuestionFilter(
      keyword: '', hospitalId: '', doctorId: '', plantId: '', sort: 0, myQuestion: false, noneAnswer: false));

  final _documentListSubject = BehaviorSubject<List<LPost>>();
  final _curriculumListSubject = BehaviorSubject<List<LPost>>();
  final _technicalListSubject = BehaviorSubject<List<LPost>>();
  final _diagramListSubject = BehaviorSubject<List<LPost>>();
  final _experientSubject = BehaviorSubject<List<LPost>>();
  final _newsSubject = BehaviorSubject<List<LPost>>();

  final _deviceIdSubject = BehaviorSubject<String>();
  final _notiUpdateQuestionSubject = BehaviorSubject<String>();
  final _actionInNotify = BehaviorSubject<NotifyAction>();

  final _orderListSubject = BehaviorSubject<List<OrderDetail>>();
  final _bonusExchangePointListSubject = BehaviorSubject<List<ExchangePoint>>();
  final _bannerListSubject = BehaviorSubject<List<BannerAds>>();
  final _notificationListSubject = BehaviorSubject<List<NotificationData>>.seeded([]);
  final _awardListSubject = BehaviorSubject<List<Award>>();

  //  Farmer
  Stream<List<OrderDetail>> get orderListStream => _orderListSubject.stream;
  Stream<List<ExchangePoint>> get bonusExchangePointListStream => _bonusExchangePointListSubject.stream;
  Stream<List<BannerAds>> get bannerListStream => _bannerListSubject.stream;
  Stream<List<NotificationData>> get notificationListStream => _notificationListSubject.stream;
  Stream<List<Award>> get awardListStream => _awardListSubject.stream;

  // signal
  Function(String) get addSetupStateAction => _setupStateSubject.sink.add;
  Function(List<LDoctor>) get replaceDoctorAction => _doctorSubject.sink.add;
  Function(List<LHospital>) get replaceHospitalAction => _hospitalSubject.sink.add;
  Function(List<LTree>) get replaceTreeAction => _treeSubject.sink.add;
  Function(List<LQuestionResponse>) get replacePopularQuestionAction => _popularQuestionSubject.sink.add;
  Function(List<LQuestionResponse>) get replaceMyQuestionAction => _myQuestionSubject.sink.add;
  Function(ClientQuestionFilter) get filterQuestion => _questionFilterSubject.add;
  Function(DoctorQuestionFilter) get filterDoctorQuestion => _questionDoctorFilterSubject.add;
  Function(NotifyAction) get addNotifyActionWhenOpen => _actionInNotify.add;

  // signal for documents
  Function(List<LPost>) get replaceDocumentAction => _documentListSubject.sink.add;
  Function(List<LPost>) get replaceCurriculumAction => _curriculumListSubject.sink.add;
  Function(List<LPost>) get replaceTechnicalAction => _technicalListSubject.sink.add;
  Function(List<LPost>) get replaceDiagramAction => _diagramListSubject.sink.add;

  Function(String) get setDeviceIdAction => _deviceIdSubject.sink.add;
  Function(String) get pushNotiUpdateQuestionAction => _notiUpdateQuestionSubject.sink.add;

  // trigger
  Stream<String> get setupStateEvent => _setupStateSubject.stream;
  Stream<List<LDoctor>> get doctorListEvent => _doctorSubject.stream;
  Stream<List<LHospital>> get hospitalListEvent => _hospitalSubject.stream;
  Stream<List<LTree>> get treeListEvent => _treeSubject.stream;

  // triger document
  Stream<List<LPost>> get documentStream => _documentListSubject.stream;
  Stream<List<LPost>> get curriculumStream => _curriculumListSubject.stream;
  Stream<List<LPost>> get technicalStream => _technicalListSubject.stream;
  Stream<List<LPost>> get diagramStream => _diagramListSubject.stream;
  Stream<List<LPost>> get experientStream => _experientSubject.stream;
  Stream<List<LPost>> get newsStream => _newsSubject.stream;

  Stream<String> get notiUpdateQuestionStream => _notiUpdateQuestionSubject;

  NotifyAction get actionInNoti => _actionInNotify.stream.value;
  List<LDoctor> get getDoctors => _doctorSubject.stream.value;
  List<LHospital> get getHospitals => _hospitalSubject.stream.value;
  List<LTree> get getTrees => _treeSubject.stream.value;

  List<LPost> get getDocumentList => _documentListSubject.stream.value;
  List<LPost> get getCurriculumList => _curriculumListSubject.stream.value;
  List<LPost> get getTechnicalList => _technicalListSubject.stream.value;
  List<LPost> get getDiagramList => _diagramListSubject.stream.value;
  List<LPost> get getExperientList => _experientSubject.stream.value;
  List<LPost> get getNewsList => _newsSubject.stream.value;
  DoctorQuestionFilter get getDoctorFilterLatest => _questionDoctorFilterSubject.stream.value;
  List<LQuestionResponse> get myQuestionList => _myQuestionSubject.stream.value;
  List<LQuestionResponse> get allQuestionList => _popularQuestionSubject.stream.value;

  Stream<List<LQuestionResponse>> get popularQuestionsEvent => _popularQuestionSubject.stream;
  Stream<List<LQuestionResponse>> get myQuestionsEvent => _myQuestionSubject.stream;

  Stream<List<LQuestionResponse>> get myQuestionFilterResultEvent => Observable.combineLatest2(
          myQuestionsEvent, _questionFilterSubject.stream.debounceTime(Duration(milliseconds: 500)),
          (List<LQuestionResponse> e1, ClientQuestionFilter e2) {
        if (e2.isMyQuestion) {
          if (e2.keyword.isEmpty) return _myQuestionSubject.stream.value;
          return _myQuestionSubject.stream.value
              .where((item) => compareContainsNoneAccent(item.title, e2.keyword))
              .toList();
        } else {
          return _myQuestionSubject.stream.value;
        }
      });

  Stream<List<LQuestionResponse>> get popularQuestionFilterResultEvent => Observable.combineLatest2(
          popularQuestionsEvent, _questionFilterSubject.stream.debounceTime(Duration(milliseconds: 500)),
          (List<LQuestionResponse> e1, ClientQuestionFilter e2) {
        if (e2.isMyQuestion == false) {
          if (e2.keyword.isEmpty) return _popularQuestionSubject.stream.value;
          return _popularQuestionSubject.stream.value
              .where((item) => compareContainsNoneAccent(item.title, e2.keyword))
              .toList();
        } else {
          return _popularQuestionSubject.stream.value;
        }
      });

  Stream<List<LQuestionResponse>> get doctorQuestionFilterResultEvent =>
      Observable.combineLatest2<List<LQuestionResponse>, DoctorQuestionFilter, DoctorQuestionFilter>(
          popularQuestionsEvent, _questionDoctorFilterSubject.debounceTime(Duration(milliseconds: 500)), (e1, e2) {
        return e2;
      }).transform(
          StreamTransformer<DoctorQuestionFilter, List<LQuestionResponse>>.fromHandlers(handleData: (event, sink) {
        List<LQuestionResponse> snap = _popularQuestionSubject.stream.value.where((item) {
          // Filter by my question
          if (event.myQuestion && item.doctor != getProfile.id) return false;
          // Filter by noneAnswer
          if (event.noneAnswer && item.doctorCommented == true) return false;

          // Filter by hospital ID
          bool hospitalMatch = event.hospitalId?.isEmpty == true ? true : item.hospital == event.hospitalId;
          if (hospitalMatch == false) return false;

          // Filter by  doctor ID
          bool doctorMatch = event.doctorId?.isEmpty == true ? true : item.doctor == event.doctorId;
          if (doctorMatch == false) return false;

          // Filter by plant ID
          bool plantMatch = event.plantId?.isEmpty == true ? true : item.plant == event.plantId;
          if (plantMatch == false) return false;

          // Filter by keyword search
          bool kwMatch = event.keyword?.isEmpty == true ? true : compareContainsNoneAccent(item.title, event.keyword);
          if (kwMatch == false) return false;

          return true;
        }).toList();

        snap.sort((a, b) => event.sort == 0 ? b.updatedAt.compareTo(a.updatedAt) : a.updatedAt.compareTo(b.updatedAt));
        sink.add(snap);
      }));

  String get deviceId => _deviceIdSubject.stream.value;

  // support
  addMyQuestionAction(LQuestionResponse q) {
    List<LQuestionResponse> qList = List.from(_myQuestionSubject.stream.value);
    qList.insert(0, q);
    replaceMyQuestionAction(qList);
  }

  @override
  void dispose() {
    _deviceIdSubject.close();
    _actionInNotify.close();
    _notiUpdateQuestionSubject.close();
    _questionDoctorFilterSubject.close();
    _diagramListSubject.close();
    _questionFilterSubject.close();
    _popularQuestionSubject.close();
    _myQuestionSubject.close();
    _setupStateSubject.close();
    _doctorSubject.close();
    _hospitalSubject.close();
    _treeSubject.close();
    _authBloc.dispose();
    _introTeamBloc.dispose();
    _documentListSubject.close();
    _curriculumListSubject.close();
    _technicalListSubject.close();
    _experientSubject.close();
    _newsSubject.close();
    _orderListSubject.close();
    _bonusExchangePointListSubject.close();
    _bannerListSubject.close();
    _notificationListSubject.close();
    _awardListSubject.close();
  }

  ApplicationBloc() {
    _authBloc = new AuthBloc();
    _introTeamBloc = new IntroTeamBloc();

    moneyChagedCommand = RxCommand.createSync((value) {
      countChangedMonney = countChangedMonney == null ? value : countChangedMonney + value;
      return countChangedMonney;
    });
  }

  AuthBloc getAuthBloc() {
    return _authBloc;
  }

  IntroTeamBloc getIntroTeamBloc() {
    return _introTeamBloc;
  }

  MainScreenBloc _mainScreenBloc;
  MainScreenBloc get mainScreenBloc {
    if (_mainScreenBloc == null) {
      _mainScreenBloc = MainScreenBloc();
    }
    return _mainScreenBloc;
  }

  bool get isDoctor => false;
  Farmer get getProfile => getAuthBloc()?.getUser;

  loadBaseData() {
    _authBloc.getUnread();
    Future.wait([
      BusinessService().getDoctorList(),
      BusinessService().getHospitalList(),
      BusinessService().getTreeList(),
    ]).then((res) {
      List<LDoctor> doctorList = res[0].cast<LDoctor>();
      replaceDoctorAction(doctorList);

      List<LHospital> hospitalList = res[1].cast<LHospital>().map<LHospital>((item) {
        List<LDoctor> doctors = doctorList.where((doctor) => doctor.hospital == item.id).toList();
        return item.copyWith(doctors: doctors);
      }).toList();

      // Push center hostital to top
      hospitalList.sort((a,b) {
        return a.type.compareTo(b.type);
      });

      replaceHospitalAction(hospitalList);
      replaceTreeAction(res[2]);
      addSetupStateAction('done');
    }).catchError((e) {
      print(e.toString());
      addSetupStateAction('error');
      throw e;
    });
  }

  Future loadUserData() async {
//    await loadRemoteConfig();
    List<dynamic> ret = await Future.wait([
      BusinessService().getOrderList(),
      BusinessService().getBonusExchangePointList(),
      BusinessService().getBannerList(),
      BusinessService().getNotificationList(),
      BusinessService().getAwardList(),
      PostService().getAllPost()
    ])
    .catchError((e) {
      print(e.toString());
      throw e;
    });
    _orderListSubject.sink.add(ret[0]);
    _bonusExchangePointListSubject.sink.add(ret[1]);
    _bannerListSubject.sink.add(ret[2]);
    _notificationListSubject.sink.add(ret[3]);
    _awardListSubject.sink.add(ret[4]);
    _newsSubject.sink.add(ret[5]);
    return Future.value();
  }


  Future<bool> loadRemoteConfig() async {
    try {
      AppConfig config = GetIt.instance<AppConfig>();

      final RemoteConfig remoteConfig = await RemoteConfig.instance;
//      final defaults = <String, dynamic>{
//        'subscriptionPrice': 500,
//        'onDemandPrice': 500,
//        'maximumRenewCount': 3100,
//      };
//      await remoteConfig.setDefaults(defaults);
      await remoteConfig.fetch(expiration: const Duration(seconds: 0));
      await remoteConfig.activateFetched();

      String webviewURL = remoteConfig.getString('webviewURL');
      String browserURL = remoteConfig.getString('browserURL');
      String path = remoteConfig.getString('pathURL');
      String version = remoteConfig.getString('versionURL');

      config.browserURL = browserURL;
      config.webviewURL = webviewURL;
      config.pathURL = path;
      config.versionURL = version;
    } catch (e) {
      return false;
    }

    return true;
  }
}
