import 'dart:convert';

import 'package:loctroi/apis/core/auth_service.dart';
import 'package:loctroi/apis/core/base_service.dart';
import 'package:loctroi/models/business_models.dart';
import 'package:loctroi/models/request_models.dart';
import 'package:loctroi/models/response_models.dart';
import 'package:loctroi/models/user.dart';
import 'package:loctroi/utils/app_store.dart';
import 'package:loctroi/utils/cache_utils.dart';

import '../loopback_config.dart';

class BusinessService extends BaseLoopBackApi {
  final LoopBackAuth auth;

  BusinessService() : auth = new LoopBackAuth();
  @override
  String getModelPath() {
    return "_";
  }

  @override
  dynamic fromJson(Map<String, Object> item) {
    return UserModel.fromJson(item);
  }

  Future<List<OrderDetail>> getOrderList() async {
    final url = [
      LoopBackConfig.getPath(),
      LoopBackConfig.getApiVersion(),
      'newBonusPointHistory?limit=0&populates=[\"bonusPointItems\",\"agencyId\", \"farmerId\"]'
    ].join('/');
    final result = await this.request(
        method: 'GET',
        url: url,
//        urlParams: {
//          'populates': '[\"bonusPointItems\",\"agencyId\", \"farmerId\"]',
//          'limit': 0
//        },
        isWrapBaseResponse: false);

    var data = result['results']['objects']['rows'];
    if (data != null && data is List) {
      return data.map((item) => OrderDetail.fromJson(item)).toList();
    }
    return [];
  }

  Future<List<ExchangePoint>> getBonusExchangePointList() async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'newFarmerSpinResult?limit=0'].join('/');
    final result = await this.request(
        method: 'GET',
        url: url,
//        urlParams: {
//          'populates': '[\"bonusPointItems\",\"agencyId\", \"farmerId\"]'
//        },
        isWrapBaseResponse: false);

    var data = result['results']['objects']['rows'];
    int count = result['results']['objects']['count'];
    if (data != null && data is List) {
      return data.map((item) {
        ExchangePoint exp = ExchangePoint.fromJson(item);
        exp.count = count;
        return exp;
      }).toList();
    }
    return [];
  }

  Future<List<BannerAds>> getBannerList() async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'notify/getBanner'].join('/');
    final result = await this.request(method: 'GET', url: url);

    if (result != null && result is List) {
      return result.map((item) => BannerAds.fromJson(item)).toList();
    }
    return [];
  }

  Future<List<NotificationData>> getNotificationList() async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'notify/getHistory'].join('/');
    final result = await this.request(method: 'GET', url: url, isWrapBaseResponse: false);

    var data = result['results']['objects']['rows'];
    if (data != null && data is List) {
      return data.map((item) => NotificationData.fromJson(item)).toList();
    }
    return [];
  }

  Future<dynamic> seen(String notiId) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'notify', notiId, 'seen'].join('/');
    final result = await this.request(method: 'GET', url: url);
    return result;
  }

  Future<List<Award>> getAwardList() async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'productCampaign'].join('/');
    final result = await this.request(method: 'GET', url: url, isWrapBaseResponse: false);

    var data = result['results']['objects']['rows'];
    if (data != null && data is List) {
      return data.map((item) => Award.fromJson(item)).toList();
    }
    return [];
  }

  Future<List<LTree>> getTreeList() async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'plant?limit=0'].join('/');
    final result = await this.request(method: 'GET', url: url);
    return (result as List).map((item) => LTree.fromJson(item)).toList();
  }

  Future<List<LHospital>> getHospitalList() async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'hospital?limit=0'].join('/');
    final result = await this.request(method: 'GET', url: url);
    result.sort((a, b) {
      return b.toString().compareTo(a.toString());
    });
    return (result as List).map((item) => LHospital.fromJson(item)).toList();
  }

  Future<List<LDoctor>> getDoctorList() async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'user/doctor?limit=0'].join('/');
    final result = await this.request(method: 'GET', url: url);
    return (result as List).map((item) => LDoctor.fromJson(item)).toList();
  }

  Future<List<LQuestionResponse>> getMyQuestionList(String myId) async {
    final url =
        [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'issue?filter={"owner":"$myId"}&limit=0'].join('/');
    final result = await this.request(method: 'GET', url: url);
    return (result as List).map((item) => LQuestionResponse.fromJson(item)).toList();
  }

  Future<List<LQuestionResponse>> getPopularQuestionList() async {
    final url = [
      LoopBackConfig.getPath(),
      LoopBackConfig.getApiVersion(),
      'issue?order={"updatedAt": -1, "view": -1, "qtyRate": -1, "qtyComment": -1}&limit=0'
    ].join('/');
    final result = await this.request(method: 'GET', url: url);
    return (result as List).map((item) => LQuestionResponse.fromJson(item)).toList();
  }

  Future<LQuestionResponse> sendQuestion(LQuestionRequest request) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'issue'].join('/');
    final result = await this.request(method: 'POST', url: url, postBody: request.toJson());
    return LQuestionResponse.fromJson(result);
  }

  Future<LQuestionResponse> updateQuestion(String id, LQuestionRequest request) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'issue', id].join('/');
    final result = await this.request(method: 'PUT', url: url, postBody: request.toJson());
    return LQuestionResponse.fromJson(result);
  }

  Future deleteQuestion(String id) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'issue', id].join('/');
    await this.request(method: 'DELETE', url: url);
  }

  Future viewQuestion(String id) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'issue', id].join('/');
    dynamic ret = await this.request(method: 'GET', url: url, urlParams: {'cached': 'false'});
    return ret;
  }
}
