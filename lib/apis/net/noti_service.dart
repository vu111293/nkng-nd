import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:loctroi/apis/core/auth_service.dart';
import 'package:loctroi/apis/core/base_service.dart';
import 'package:loctroi/models/response_models.dart';
import 'package:loctroi/models/user.dart';
import 'package:loctroi/utils/cache_utils.dart';

import '../loopback_config.dart';

class NotiService extends BaseLoopBackApi {
  final LoopBackAuth auth;

  NotiService() : auth = new LoopBackAuth();

  @override
  String getModelPath() {
    return "";
  }

  @override
  dynamic fromJson(Map<String, Object> item) {
    return null;
  }

  Future registerFcm(String deviceId, String token) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'newFarmer/updateDeviceToken'].join('/');
    await this.request(method: 'PUT', url: url, postBody: {"deviceId": deviceId, "deviceToken": token});
  }

  Future unregisterFcm() async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'newFarmer/unsubscribe'].join('/');
    await this.request(method: 'GET', url: url);
  }

  Future<List<LNoti>> getNotificationList(int page) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'notification?page=$page&limit=0'].join('/');
    final result = await this.request(method: 'GET', url: url);
    print('getNotificationList ${result.toList().length}');
    return (result as List).map((item) => LNoti.fromJson(item)).toList();
  }

  Future<dynamic> seen(String notiId) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'notify', notiId, 'seen'].join('/');
    final result = await this.request(method: 'GET', url: url);
    return result;
  }
}
