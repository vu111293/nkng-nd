import 'package:loctroi/apis/core/auth_service.dart';
import 'package:loctroi/apis/core/base_service.dart';
import 'package:loctroi/apis/loopback_config.dart';

class IntroTeamService extends BaseLoopBackApi {
  final LoopBackAuth auth;

  IntroTeamService() : auth = new LoopBackAuth();
  @override
  String getModelPath() {
    return "post";
  }

  String getSettingModelPath() {
    return "Intro";
  }

  @override
  String fromJson(Map<String, Object> item) {
    return '';
  }

  Future getIntroTeam() async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), getModelPath(), 'intro'].join('/');
    final result = await this.request(method: 'GET', url: url);
    return result[0]['content'];
  }

  Future getDiagramTeam() async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), getModelPath(), 'diagram'].join('/');
    final result = await this.request(method: 'GET', url: url);
    return result[0]['content'];
  }
}
