// import 'dart:convert';
import 'package:loctroi/apis/core/auth_service.dart';
import 'package:loctroi/apis/core/base_service.dart';
import 'package:loctroi/apis/loopback_config.dart';
import 'package:loctroi/models/response_models.dart';
import 'package:loctroi/models/user.dart';

class CommentService extends BaseLoopBackApi {
  final LoopBackAuth auth;

  CommentService() : auth = new LoopBackAuth();
  @override
  String getModelPath() {
    return "comment";
  }

  @override
  dynamic fromJson(Map<String, Object> item) {
    return UserModel.fromJson(item);
  }

  Future<List<LComment>> getComments(String questionId) async {
    final url = [
      LoopBackConfig.getPath(),
      LoopBackConfig.getApiVersion(),
      'issue',
      questionId,
      'comment?populates=[{"path":"owner", "select": "fullName avatar phone"}]&fields="content picture createdAt updatedAt"&limit=0',
    ].join('/');
    final result = await this.request(method: 'GET', url: url);
    if (result.length == 0) return [];
    return result.map((item) => LComment.fromJson(item)).toList().cast<LComment>();
  }

  Future<dynamic> sendComment(String questionId, String content, String image) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'issue', questionId, 'comment'].join('/');
    final result = await this.request(method: 'POST', url: url, postBody: {'content': content, 'picture': image});
    return result;
  }

  Future editComment(String commentId, String content) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'comment', commentId].join('/');
    final result = await this.request(method: 'PUT', url: url, postBody: {'content': content});
    return result;
  }
}
