import 'dart:convert';

import 'package:loctroi/apis/core/auth_service.dart';
import 'package:loctroi/apis/core/base_service.dart';
import 'package:loctroi/apis/loopback_config.dart';
import 'package:loctroi/models/job_request_model.dart';
import 'package:loctroi/models/jobs_model.dart';
import 'package:loctroi/utils/cache_utils.dart';
import 'package:loctroi/models/user.dart';

class JobsService extends BaseLoopBackApi {
  final LoopBackAuth auth;

  JobsService() : auth = new LoopBackAuth();
  @override
  String getModelPath() {
    return "job";
  }

  String getAssignModelPath() {
    return "assign";
  }

  String getSettingModelPath() {
    return "Account-Settings";
  }

  String getTelesalesModelPath() {
    return "get_list_tele_sale";
  }

  @override
  dynamic fromJson(Map<String, Object> item) {
    return JobsModel.fromJson(item);
  }

  Future getListTelesales(int page, String jobId) async {
    final customFields = '?fields=["\$all"]&limit=10&page=$page';
    final url = [
      LoopBackConfig.getPath(),
      LoopBackConfig.getApiVersion(),
      getModelPath(),
      jobId,
      getTelesalesModelPath() + customFields
    ].join('/');
    final result = await this.request(method: 'GET', url: url, isWrapBaseResponse: false);
    final jsonRows = result["results"]["objects"]["rows"] as List;

    final items = (jsonRows).map((item) {
      return UserModel.fromJson(item);
    }).toList();
    CacheUtils.saveJsonDB('user.dat', json.encode(result));
    return items;
  }

  ///cv mới => unjoinned
  ///cv đã nhận =>joined
  //cv đã từ chối =>
  //cv đang chối
  Future<List<JobsModel>> getListJobs(String jobType, int page) async {
    // final customFields = '?fields=["\$all"]&limit=10&page=$page&filter={"user_id": "${auth.accessToken().user.id}"}';
    // String customFields;
    // if (jobType == "NEW") {
    //   customFields = '?fields=["\$all", {"user": ["\$all"]}]&filter={"status" : true}&limit=10&page=$page';
    // } else {
    //   customFields = '?fields=["\$all", {"user": ["\$all"]}]&limit=10&page=$page';
    // }

    // final url = [
    //   LoopBackConfig.getPath(),
    //   LoopBackConfig.getApiVersion(),
    //   getModelPath(),
    //   jobType,
    //   'get_list_job' + customFields
    // ].join('/');
    // print("getListJobs url ne:" + url.toString());
    // final result = await this.request(method: 'GET', url: url, isWrapBaseResponse: false);
    // print(result);
    // final jsonRows = result["results"]["objects"]["rows"] as List;

    // final items = (jsonRows).map((item) {
    //   return JobsModel.fromJson(item);
    // }).toList();
    // CacheUtils.saveJsonDB('jobs.dat', json.encode(result));
    // return items;
  }

  Future<List<JobsModel>> searchListJobs(String jobType, String searchText, int page) async {
    String customFields;
    String percentiLike = '%';
    if (searchText.length >= 2 && searchText.substring(0, 2).toLowerCase().contains('ab')) {
      percentiLike = '%25';
    }
    if (jobType == "NEW") {
      customFields =
          '?fields=["\$all", {"user": ["\$all"]}]&limit=10&page=$page&filter={"status" : true, "title":{"\$iLike":"$percentiLike$searchText$percentiLike"}}';
    } else {
      customFields =
          '?fields=["\$all", {"user": ["\$all"]}]&limit=10&page=$page&filter={"title":{"\$iLike":"$percentiLike$searchText$percentiLike"}}';
    }

    final url = [
      LoopBackConfig.getPath(),
      LoopBackConfig.getApiVersion(),
      getModelPath(),
      jobType,
      'get_list_job' + customFields
    ].join('/');
    print("searchListJobs url ne:" + url.toString());

    final result = await this.request(method: 'GET', url: url, isWrapBaseResponse: false);
    print("Result: " + result.toString());
    final jsonRows = result["results"]["objects"]["rows"] as List;

    final items = (jsonRows).map((item) {
      return JobsModel.fromJson(item);
    }).toList();
    CacheUtils.saveJsonDB('jobs.dat', json.encode(result));
    return items;
  }

  Future assignJob(Map<String, dynamic> assign) async {
    final url =
        [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), getModelPath(), getAssignModelPath()].join('/');
    // Map<String, dynamic>
    final result = await this.request(method: 'POST', url: url, postBody: assign, isWrapBaseResponse: false);
    print("createJob done" + result["results"]["object"].toString());
    return JobsModel.fromJson(result["results"]["object"]);
  }

  Future createJob(Map<String, dynamic> job) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), getModelPath()].join('/');
    // Map<String, dynamic>
    final result = await this.request(method: 'POST', url: url, postBody: job, isWrapBaseResponse: false);
    print("createJob done" + result["results"]["object"].toString());
    return JobsModel.fromJson(result["results"]["object"]);
  }

  Future rateJob(Map<String, dynamic> job) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'rate'].join('/');
    // Map<String, dynamic>
    final result = await this.request(method: 'POST', url: url, postBody: job, isWrapBaseResponse: false);
    print("rateJob done" + result["code"].toString());
    print("updateStatusJob done $url");

    return true;
  }

  Future updateStatusJob(String id, Map<String, dynamic> body) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), getModelPath(), id].join('/');
    // Map<String, dynamic>
    final result = await this.request(method: 'PUT', url: url, postBody: body, isWrapBaseResponse: false);
    print("updateStatusJob done" + result["results"]["object"].toString());
    return JobsModel.fromJson(result["results"]["object"]);
  }

  ///get telesale'sjob histories (have pagination)
  Future<List<JobsModel>> getCandidateJobHistory(String candidateId, int page) async {
    final customFields =
        '?fields=["\$all", {"call_history": ["\$all", { "\$filter": { "user_id": "$candidateId", "should_call": true}}]}]&limit=10&page=$page';

    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), getModelPath() + customFields].join('/');

    final result = await this.request(method: 'GET', url: url, isWrapBaseResponse: false);
    final jsonRows = result["results"]["objects"]["rows"] as List;

    final items = (jsonRows).map((item) {
      return JobsModel.fromJson(item);
    }).toList();
    // CacheUtils.saveJsonDB('jobs.dat', json.encode(result));
    return items;
  }

  ///return a list pending request for a job (params: job_id, user_id)
  Future<List<JobRequestModel>> getListPendingJobRequested(int page) async {
    // {{host}}/api/{{version}}/job_request?fields=["$all",{"user": ["$all"]}]&filter={"job_id": "e063ce10-c491-11e9-ad8b-919521578e2f", "state": "REJECTED", "is_telesale_request": true}
    final customFields =
        '?fields=["\$all",{"user": ["\$all"]},{"job": ["\$all"]}]&filter={"user_id": "#", "state": "PENDING"}&limit=10&page=$page';
    // final customFields =
    //     '?fields=["\$all",{"user": ["\$all"]}]&limit=10&page=$page&filter={"job_id": "$jobId", "state": "PENDING", "is_telesale_request": $isTelesaleRequest}';
    // final customFields = '?fields=["\$all"]&limit=10&page=$page';
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'job_request' + customFields].join('/');
    final result = await this.request(method: 'GET', url: url, isWrapBaseResponse: false);
    print("result+ " + result.toString());
    final jsonRows = result["results"]["objects"]["rows"] as List;
    final items = (jsonRows).map((item) {
      print("item nhe nha: " + item.toString());
      return JobRequestModel.fromJson(item);
    }).toList();
    print("ahihi items+ " + items.length.toString());
    return items;
  }

  ///return a list pending request for a job (params: job_id, user_id)
  Future<List<JobRequestModel>> searchListPendingJobRequested(int page, searchText) async {
    print("SearchText: " + searchText);
    String percentiLike = '%';
    if (searchText.length >= 2 && searchText.substring(0, 2).toLowerCase().contains('ab')) {
      percentiLike = '%25';
    }
    final customFields =
        '?fields=["\$all",{"user": ["\$all"]},{"job": ["\$all", {"\$filter" : {"title":{"\$iLike":"$percentiLike$searchText$percentiLike"}}}]}]&filter={"user_id": "#", "state": "PENDING"}&limit=10&page=$page';
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'job_request' + customFields].join('/');
    final result = await this.request(method: 'GET', url: url, isWrapBaseResponse: false);
    print("PENDing result: " + result.toString());
    final jsonRows = result["results"]["objects"]["rows"] as List;
    final items = (jsonRows).map((item) {
      return JobRequestModel.fromJson(item);
    }).toList();
    return items;
  }

  ///allow telesale to create join request to a job
  Future addNewJobRequest(String jobId) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'job_request'].join('/');
    // print("<addNewJobRequest url: " + url + ">");
    Map<String, dynamic> postBody = {
      "user_id": "#",
      "job_id": jobId,
      "is_telesale_request": true
    };
    final result = await this.request(method: 'POST', url: url, postBody: postBody, isWrapBaseResponse: false);
    // print(result.toString());
    return result['code'].toString();
  }

  ///allow telesale edit an join request of a job
  Future editJobRequest(String jobRequestId, String state) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), 'job_request', jobRequestId].join('/');
    Map<String, dynamic> postBody = {"state": state};
    print("editJobRequest url: " + url);
    final result = await this.request(method: 'PUT', url: url, postBody: postBody, isWrapBaseResponse: false);
    print("editJobRequest result: " + result.toString());
    return result['code'].toString();
  }
}
