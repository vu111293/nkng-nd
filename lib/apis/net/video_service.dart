import 'package:loctroi/apis/core/auth_service.dart';
import 'package:loctroi/apis/core/base_service.dart';
import 'package:loctroi/apis/loopback_config.dart';
import 'package:loctroi/models/video_model.dart';

class VideoService extends BaseLoopBackApi {
  final LoopBackAuth auth;

  VideoService() : auth = new LoopBackAuth();
  @override
  String getModelPath() {
    return "youtube";
  }

  @override
  dynamic fromJson(Map<String, Object> item) {
    return VideoModel.fromJson(item);
  }

  ///return a list pending request for a job (params: job_id, user_id)
  Future<dynamic> searchListVideo(int page, searchText, token) async {
    print("SearchText: " + searchText);
    // String percentiLike = '%';
    // if (searchText.length >= 2 && searchText.substring(0, 2).toLowerCase().contains('ab')) {
    //   percentiLike = '%25';
    // }
    final customFields = '?limit=$page&search=$searchText&nextPageToken=$token';
    final url =
        [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), getModelPath(), 'videos' + customFields].join('/');
    // print("PENDing result: " + url.toString());
    final result = await this.request(method: 'GET', url: url, isWrapBaseResponse: false);
    // print("searchListVideo result: " + result["data"].toString());
    // final jsonRows = result["data"]["videos"] as List;
    // final items = (jsonRows).map((item) {
    //   return VideoModel.fromJson(item);
    // }).toList();
    return result;
  }
}
