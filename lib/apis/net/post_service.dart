import 'dart:convert';

import 'package:loctroi/apis/core/auth_service.dart';
import 'package:loctroi/apis/core/base_service.dart';
import 'package:loctroi/models/response_models.dart';
import 'package:loctroi/models/user.dart';
import 'package:loctroi/utils/cache_utils.dart';

import '../loopback_config.dart';

class PostService extends BaseLoopBackApi {
  final LoopBackAuth auth;

  PostService() : auth = new LoopBackAuth();
  @override
  String getModelPath() {
    return "";
  }

  @override
  dynamic fromJson(Map<String, Object> item) {
    return LPost.fromJson(item);
  }

  Future<List<LPost>> getAllPost({String name}) async {
    final url = [
      LoopBackConfig.getPath(),
      LoopBackConfig.getApiVersion(),
      'post',
    ].join('/');

    final result = await this.request(method: 'GET', url: url, isWrapBaseResponse: false);
    var data = result['results']['objects']['rows'];
    if (data != null && data is List) {
      return data.map((item) {
        return LPost.fromJson(item);
      }).toList();
    }
    return [];
//    result.sort((a, b) {
//      return a.toString().compareTo(b.toString());
//    });
//    return (result as List).map((item) {
//      return LPost.fromJson(item);
//    }).toList();
  }

  Future<LPost> getPostDetail(String id) async {
    final url = [
      LoopBackConfig.getPath(),
      LoopBackConfig.getApiVersion(),
      'post',
      '$id'
    ].join('/');
    final result = await this.request(method: 'GET', url: url);
    return LPost.fromJson(result);
  }

  Future<List<LPost>> getPostListByName({String name}) async {
    final url = [
      LoopBackConfig.getPath(),
      LoopBackConfig.getApiVersion(),
      'post?filter={\"topicSlugs\": { \"\$in\": [\"$name\"] } }&populates=[{\"path\":\"owner\",\"select\":\"fullName avatar\"}]&limit=0'
    ].join('/');
    final result = await this.request(method: 'GET', url: url);
    result.sort((a, b) {
      return a.toString().compareTo(b.toString());
    });
    return (result as List).map((item) {
      return LPost.fromJson(item);
    }).toList();
  }

//  Future<List<LPost>> getDocumentList() {
//    return getPostListByName('5d8b153ae5de9f190c1f468b');
//  }
//
//  Future<List<LPost>> getCurriculumList() {
//    return getPostListByName('5d970f43f42d210020000d42');
//  }
//
//  Future<List<LPost>> getTechnicalList() {
//    return getPostListByName('5d8dcb773a828e299a8ad65d');
//  }
//
//  Future<List<LPost>> getDiagramList() {
//    return getPostListByName('5dbef36cd5f4530018036dd1');
//  }
//
//  Future<List<LPost>> getExperientList() {
//    return getPostListByName('5dc38fb72b71bc0024b41e96');
//  }
//
//  Future<List<LPost>> getNewsList() {
//    return getPostListByName('5dc6691d21459800182d7fad');
//  }

}
