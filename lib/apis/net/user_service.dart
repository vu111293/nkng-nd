import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:loctroi/apis/core/auth_service.dart';
import 'package:loctroi/apis/core/base_service.dart';
import 'package:loctroi/models/business_models.dart';
import 'package:loctroi/models/user.dart';
import 'package:loctroi/utils/cache_utils.dart';
import 'package:flutter_user_agent/flutter_user_agent.dart';

import '../loopback_config.dart';

class UserService extends BaseLoopBackApi {
  final LoopBackAuth auth;

  UserService() : auth = new LoopBackAuth();
  @override
  String getModelPath() {
    return "newFarmer";
  }

  String getSettingModelPath() {
    return "Account-Settings";
  }

  @override
  dynamic fromJson(Map<String, Object> item) {
    return UserModel.fromJson(item);
  }

  Future<bool> checkPhoneExists(String phone) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), getModelPath(),  'checkExistByPhone'].join('/');
    final result = await this.request(method: 'GET', url: url,
        urlParams: {
          'phone': phone
        });
    if (result['exist'] == null) return false;
    return result['exist'] == 1 ? true : false;
  }

  Future changePassword(String pin) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), getModelPath(),  LoopBackAuth().userId, 'updatePin'].join('/');
    final result = await this.request(method: 'PUT', url: url,
        postBody: {
          'pin': pin
        });
    return Future;
  }

  Future<Farmer> forgotPassword(String token, String pin) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), getModelPath(), 'updatePinByFirebaseToken'].join('/');
    final result = await this.request(method: 'POST', url: url,
    postBody: {
      'token': token,
      'pin': pin
    });
    return Farmer.fromJson(result);
  }

  Future<Farmer> getProfile(String userId) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), getModelPath(), userId].join('/');
    final result = await this.request(method: 'GET', url: url);
    return Farmer.fromJson(result);
  }

  Future<Farmer> loginByPhone(String phone, String pin) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), getModelPath(), 'loginByPhone'].join('/');
    final result = await this.request(
        method: 'POST',
        url: url,
        postBody: {
          'phone': phone,
          'pin': pin
        });
    Farmer user = Farmer.fromJson(result['newFarmer']);
    // cache token
    String token = result['token'];
    LoopBackAuth().accessToken = token;
    LoopBackAuth().userId = user.id;
    print('Access token: $token');
    return user;
  }

  Future<Farmer> login(String fbToken) async {
    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), getModelPath(), 'login'].join('/');
    final result = await this.request(
        method: 'POST',
        url: url,
        postBody: {'token': fbToken});
    Farmer user = Farmer.fromJson(result['user']);

    // cache token
    String token = result['token'];
    LoopBackAuth().accessToken = token;
    LoopBackAuth().userId = user.id;

    print('Access token: $token');
    return user;
  }

  Future<Farmer> update(Farmer user) async {
    Map<String, dynamic> body;
    body = {
//      'fullName': user.fullName ?? '',
//      'address': user.address ?? '',
//      'area': user.area ?? 0,
//      'avatar': user.avatar ?? ''
    };

    final url = [LoopBackConfig.getPath(), LoopBackConfig.getApiVersion(), getModelPath(), user.id].join('/');
    final result = await this.request(method: 'PUT', url: url, postBody: body);
    return Farmer.fromJson(result);
  }

  Future<List<UserModel>> getListUser() async {
    final url = [
      LoopBackConfig.getPath(),
      LoopBackConfig.getApiVersion(),
      getModelPath() + '?fields=["\$all"]',
    ].join('/');

    final result = await this.request(method: 'GET', url: url, isWrapBaseResponse: false);
    final jsonRows = result["results"]["objects"]["rows"] as List;

    final items = (jsonRows).map((item) {
      return UserModel.fromJson(item);
    }).toList();
    CacheUtils.saveJsonDB('user.dat', json.encode(result));
    return items;
  }

  Future<List<UserModel>> getListTelesalesWithEmail(String email) async {
    final url = [
      LoopBackConfig.getPath(),
      LoopBackConfig.getApiVersion(),
      getModelPath() + '?fields=["\$all"]&filter={"account_type": "TELESALE", "email": "$email"}',
    ].join('/');

    final result = await this.request(method: 'GET', url: url, isWrapBaseResponse: false);
    final jsonRows = result["results"]["objects"]["rows"] as List;

    final items = (jsonRows).map((item) {
      return UserModel.fromJson(item);
    }).toList();
    CacheUtils.saveJsonDB('user.dat', json.encode(result));
    return items;
  }
}
