import 'package:get_it/get_it.dart';

import '../app.dart';

class LoopBackConfig {
//  static String _path = 'https://loctroi-dev.mcom.app';
//  static String _version = 'api';
  static String _apiImgur = "https://api.imgur.com/3/image";
  static String _secretImgur = "Client-ID d4de8224fa0042f";


  static String getApiVersion() {
//    return LoopBackConfig._version;
    return GetIt.instance<AppConfig>().versionURL;
  }

  static String getPath() {
//    return LoopBackConfig._path;
    return GetIt.instance<AppConfig>().pathURL;
  }

  static String getApiImgur() {
    return LoopBackConfig._apiImgur;
  }

  static void setApiImgur(String api) {
    LoopBackConfig._apiImgur = api;
  }

  static String getSecretImgur() {
    return LoopBackConfig._secretImgur;
  }

  static void setSecretImgur(String api) {
    LoopBackConfig._secretImgur = api;
  }
}
