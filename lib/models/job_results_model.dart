import 'package:json_annotation/json_annotation.dart';
import 'package:loctroi/models/ScriptItemWidgetModel.dart';

// *** PLEASE RUN COMMAND BELOW FOR REBUILD MODELS ****
// flutter packages pub run build_runner build --delete-conflicting-outputs

//@JsonSerializable(nullable: false)
class JobResultsModel {
  String id;
  String name;
  String phone;
  bool status;
  List<ScriptItemWidgetModel> listScritpts;

  JobResultsModel({this.id, this.name, this.phone, this.status, this.listScritpts});
}
