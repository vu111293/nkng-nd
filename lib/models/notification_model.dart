import 'package:json_annotation/json_annotation.dart';

part 'notification_model.g.dart';

// *** PLEASE RUN COMMAND BELOW FOR REBUILD MODELS ****
// flutter packages pub run build_runner build --delete-conflicting-outputs

@JsonSerializable(nullable: false)
class NotificationModel {
  String id;
  bool status;
  String user_id;
  String title;
  String content;
  Object data;
  String interacting_user_id;
  String interacting_type;
  String interacting_content_id;
  int action;

  DateTime created_at;
  DateTime updated_at;

  NotificationModel({
    this.id,
    this.status,
    this.user_id,
    this.title,
    this.content,
    this.data,
    this.interacting_user_id,
    this.interacting_type,
    this.interacting_content_id,
    this.action,
    this.created_at,
    this.updated_at,
  });

  factory NotificationModel.fromJson(Map<String, dynamic> json) {
    final notification = _$NotificationModelFromJson(json);
    return notification;
  }
  Map<String, dynamic> toJson() => _$NotificationModelToJson(this);

  // @override
  // String toString() {
  //   return "NotificationModel $id ,$status ,$user_id ,$content ,$data ,$interacting_user_id ,$interacting_type ,$interacting_content_id ,$action ,$created_at ,$updated_at";
  // }
}

enum NotifyType { POST, ISSUE, NEWS, UNDEFINE }

class NotifyAction {
  final NotifyType type;
  final String id;

  NotifyAction({this.type, this.id});
}
