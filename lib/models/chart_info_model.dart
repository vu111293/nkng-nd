import 'package:json_annotation/json_annotation.dart';

part 'chart_info_model.g.dart';

// *** PLEASE RUN COMMAND BELOW FOR REBUILD MODELS ****
// flutter packages pub run build_runner build --delete-conflicting-outputs

@JsonSerializable(nullable: true)
class ChartInfoModel {
  final int index;
  final String day;
  final int job_of_user_number;
  final int money_of_user_number;
  final int money_of_user;
  final int call_history_of_user_number;

  ChartInfoModel(
      {this.index,
      this.day,
      this.job_of_user_number,
      this.money_of_user_number,
      this.money_of_user,
      this.call_history_of_user_number});

  ChartInfoModel copyWith(
      {int index,
      String day,
      int job_of_user_number,
      int money_of_user_number,
      int money_of_user,
      int call_history_of_user_number}) {
    return ChartInfoModel(
        index: index ?? this.index,
        day: day ?? this.day,
        job_of_user_number: job_of_user_number ?? this.job_of_user_number,
        money_of_user: money_of_user ?? this.money_of_user,
        money_of_user_number: money_of_user_number ?? this.money_of_user_number,
        call_history_of_user_number: call_history_of_user_number ?? this.call_history_of_user_number);
  }

  factory ChartInfoModel.fromJson(Map<String, dynamic> json) {
    final history = _$ChartInfoModelFromJson(json);
    return history;
  }
  Map<String, dynamic> toJson() => _$ChartInfoModelToJson(this);

  @override
  String toString() {
    return " $day ";
  }
}
