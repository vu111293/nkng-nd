import 'package:json_annotation/json_annotation.dart';
import 'package:loctroi/models/client_model.dart';
import 'package:loctroi/models/user.dart';
part 'jobs_model.g.dart';

// *** PLEASE RUN COMMAND BELOW FOR REBUILD MODELS ****
// flutter packages pub run build_runner build --delete-conflicting-outputs

@JsonSerializable(nullable: false)
class JobsModel {
  String id;
  String state;
  bool status;
  String price;
  String user_id;
  String title;
  String requirement;
  @JsonKey(nullable: true)
  List<ClientModel> list_clients;
  int worker_amount;
  int total_call_amount;
  int done_call_amount;
  String instruction;
  @JsonKey(nullable: true)
  String interacting_type;
  @JsonKey(nullable: true)
  String interacting_content_id;
  String action;
  @JsonKey(nullable: true)
  UserModel user;
  String created_at_unix_timestamp;
  String updated_at_unix_timestamp;

  JobsModel(
      {this.id,
      this.state,
      this.status,
      this.price,
      this.user_id,
      this.title,
      this.requirement,
      this.list_clients,
      this.worker_amount,
      this.total_call_amount,
      this.done_call_amount,
      this.instruction,
      this.interacting_type,
      this.interacting_content_id,
      this.action,
      this.created_at_unix_timestamp,
      this.updated_at_unix_timestamp,
      this.user});

  factory JobsModel.fromJson(Map<String, dynamic> json) {
    final jobs = _$JobsModelFromJson(json);
    return jobs;
  }
  Map<String, dynamic> toJson() => _$JobsModelToJson(this);

  @override
  String toString() {
    return "JobsModel $id ----";
  }
  // @override
  // String toString() {
  //   return "JobsModel $id ,$status ,$user_id ,$requirement ,$list_clients ,$instruction ,$interacting_type ,$interacting_content_id ,$action ,$created_at_unix_timestamp ,$updated_at_unix_timestamp";
  // }
}
