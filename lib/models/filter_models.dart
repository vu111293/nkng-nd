class ClientQuestionFilter {
  final String keyword;
  final bool isMyQuestion;
  final bool noneAnswer;

  ClientQuestionFilter({this.keyword, this.isMyQuestion, this.noneAnswer});
}

class DoctorQuestionFilter {
  final String hospitalId;
  final String doctorId;
  final String plantId;
  final int sort; // 0: newest, 1: oldest, 2: popular
  final String keyword;
  final bool myQuestion;
  final bool noneAnswer;

  DoctorQuestionFilter(
      {this.keyword, this.hospitalId, this.doctorId, this.plantId, this.sort, this.myQuestion, this.noneAnswer});

  DoctorQuestionFilter copyWith(
      {String hospitalId,
      String doctorId,
      String plantId,
      int sort,
      String keyword,
      bool myQuestion,
      bool noneAnswer}) {
    return DoctorQuestionFilter(
        hospitalId: hospitalId ?? this.hospitalId,
        doctorId: doctorId ?? this.doctorId,
        plantId: plantId ?? this.plantId,
        sort: sort ?? this.sort,
        keyword: keyword ?? this.keyword,
        myQuestion: myQuestion ?? this.myQuestion,
        noneAnswer: noneAnswer ?? this.noneAnswer);
  }
}
