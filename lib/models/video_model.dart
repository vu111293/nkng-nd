import 'package:json_annotation/json_annotation.dart';
part 'video_model.g.dart';

@JsonSerializable(nullable: false)
class VideoModel {
  String title;
  String description;
  String videoId;
  String thumb;
  String published;

  VideoModel({this.title, this.description, this.videoId, this.thumb, this.published});

  factory VideoModel.fromJson(Map<String, dynamic> json) {
    final jobs = _$VideoModelFromJson(json);
    return jobs;
  }
  Map<String, dynamic> toJson() => _$VideoModelToJson(this);
  @override
  String toString() {
    return 'VideoModel{title: $title, description: $description, videoId: $videoId, thumb: $thumb, published: $published}';
  }
}
