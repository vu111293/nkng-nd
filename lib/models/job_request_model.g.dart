// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'job_request_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JobRequestModel _$JobRequestModelFromJson(Map<String, dynamic> json) {
  return JobRequestModel(
    id: json['id'] as String,
    user_id: json['user_id'] as String,
    job_id: json['job_id'] as String,
    state: json['state'] as String,
    is_telesale_request: json['is_telesale_request'] as bool,
    created_at_unix_timestamp: json['created_at_unix_timestamp'] as String,
    updated_at_unix_timestamp: json['updated_at_unix_timestamp'] as String,
    status: json['status'] as bool,
    created_at: json['created_at'] as String,
    updated_at: json['updated_at'] as String,
    job: JobsModel.fromJson(json['job'] as Map<String, dynamic>),
    user: UserModel.fromJson(json['user'] as Map<String, dynamic>),
  )..deleted_at = json['deleted_at'] as String;
}

Map<String, dynamic> _$JobRequestModelToJson(JobRequestModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'user_id': instance.user_id,
      'job_id': instance.job_id,
      'state': instance.state,
      'is_telesale_request': instance.is_telesale_request,
      'created_at_unix_timestamp': instance.created_at_unix_timestamp,
      'updated_at_unix_timestamp': instance.updated_at_unix_timestamp,
      'status': instance.status,
      'created_at': instance.created_at,
      'updated_at': instance.updated_at,
      'deleted_at': instance.deleted_at,
      'user': instance.user,
      'job': instance.job,
    };
