// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_bank_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccountBankModel _$AccountBankModelFromJson(Map<String, dynamic> json) {
  return AccountBankModel(
    json['id'] as String,
    json['bankname'] as String,
    json['account_name'] as String,
    json['account_number'] as String,
    json['branch'] as String,
    json['city'] as String,
  );
}

Map<String, dynamic> _$AccountBankModelToJson(AccountBankModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'bankname': instance.bankname,
      'account_name': instance.account_name,
      'account_number': instance.account_number,
      'branch': instance.branch,
      'city': instance.city,
    };
