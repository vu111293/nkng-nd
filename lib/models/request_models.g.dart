// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'request_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LQuestionRequest _$LQuestionRequestFromJson(Map<String, dynamic> json) {
  return LQuestionRequest(
    title: json['title'] as String,
    description: json['description'] as String,
    plant: json['plant'] as String,
    images: json['images'] as List,
    hospital: json['hospital'] as String,
    doctor: json['doctor'] as String,
  );
}

Map<String, dynamic> _$LQuestionRequestToJson(LQuestionRequest instance) =>
    <String, dynamic>{
      'title': instance.title,
      'description': instance.description,
      'plant': instance.plant,
      'hospital': instance.hospital,
      'doctor': instance.doctor,
      'images': instance.images,
    };
