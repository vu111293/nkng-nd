// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cash_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CashModel _$CashModelFromJson(Map<String, dynamic> json) {
  return CashModel(
    id: json['id'] as String,
    user_id: json['user_id'] as String,
    history: json['history'] as String,
    money: json['money'] as String,
    remaining_money: json['remaining_money'] as String,
    type: json['type'] as String,
    payer_id: json['payer_id'] as String,
    employee_id: json['employee_id'] as String,
    payment_type: json['payment_type'] as String,
    state: json['state'] as String,
    status: json['status'] as bool,
    created_at_unix_timestamp: json['created_at_unix_timestamp'] as String,
    updated_at_unix_timestamp: json['updated_at_unix_timestamp'] as String,
    create_at: json['create_at'] as String,
    deleted_at: json['deleted_at'] as String,
  );
}

Map<String, dynamic> _$CashModelToJson(CashModel instance) => <String, dynamic>{
      'id': instance.id,
      'user_id': instance.user_id,
      'history': instance.history,
      'money': instance.money,
      'remaining_money': instance.remaining_money,
      'type': instance.type,
      'payer_id': instance.payer_id,
      'employee_id': instance.employee_id,
      'payment_type': instance.payment_type,
      'state': instance.state,
      'status': instance.status,
      'created_at_unix_timestamp': instance.created_at_unix_timestamp,
      'updated_at_unix_timestamp': instance.updated_at_unix_timestamp,
      'create_at': instance.create_at,
      'deleted_at': instance.deleted_at,
    };
