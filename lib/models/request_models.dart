import 'package:json_annotation/json_annotation.dart';
part 'request_models.g.dart';

@JsonSerializable(nullable: false)
class LQuestionRequest {
  final String title;
  final String description;
  final String plant;
  @JsonKey(nullable: true)
  final String hospital;
  @JsonKey(nullable: true)
  final String doctor;
  final List images;

  LQuestionRequest({this.title, this.description, this.plant, this.images, this.hospital, this.doctor});

  factory LQuestionRequest.fromJson(Map<String, dynamic> json) {
    final user = _$LQuestionRequestFromJson(json);
    return user;
  }
  Map<String, dynamic> toJson() => _$LQuestionRequestToJson(this);
}
