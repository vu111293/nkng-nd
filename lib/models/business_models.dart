//"phone": [
//"0965432140"
//],
//"point": 0,
//"exchangePoint": 0,
//"status": "active",
//"_id": "5e61d18dcb5f1629c299966d",
//"fullname": "Tư MCOM",
//"idcard": "123456789",
//"idcardAddress": "HCM",
//"address": "42 Hoa Huệ",
//"code": "123456",
//"cardNumber": "456789",
//"idcardDate": "10/10/1980",
//"pin": "sha1$4d6f5296$1$076b5dd7a392c365752dca7718b1aa0d4a9c1cd6",
//"createdAt": "2020-03-06T04:29:01.449Z",
//"updatedAt": "2020-03-06T07:41:26.830Z",

import 'package:json_annotation/json_annotation.dart';
part 'business_models.g.dart';

@JsonSerializable()
class Farmer {
  @JsonKey(name: '_id')
  final String id;
  final List<String> phone;
  final int point;
  final int pointAfter;
  final int exchangePoint;
  final String status;
  final String fullname;
  final String idcard;
  @JsonKey(name: 'idcardAddress')
  final String cardAddress;
  final String address;
  final String code;
  final String cardNumber;
  final String idcardDate;
  final String pin;
  final int unreadCount;
  final String createdAt;
  final String updatedAt;

  Farmer(
      {this.id,
      this.phone,
      this.point,
      this.pointAfter,
      this.exchangePoint,
      this.status,
      this.fullname,
      this.idcard,
      this.cardAddress,
      this.address,
      this.code,
      this.cardNumber,
      this.idcardDate,
      this.pin,
      this.unreadCount,
      this.createdAt,
      this.updatedAt});

  factory Farmer.fromJson(Map<String, dynamic> json) => _$FarmerFromJson(json);
  Map<String, dynamic> toJson() => _$FarmerToJson(this);

  bool get isClientFilledInfo =>
      fullname?.isNotEmpty == true && address?.isNotEmpty == true && phone?.isNotEmpty == true;

  Farmer copyWith(
      {String id,
      List<String> phone,
      int point,
      int pointAfter,
      int exchangePoint,
      String status,
      String fullname,
      String idcard,
      String cardAddress,
      String address,
      String code,
      String cardNumber,
      String idcardDate,
      String pin,
      int unreadCount,
      String createdAt,
      String updatedAt}) {
    return Farmer(
        id: id ?? this.id,
        phone: phone ?? this.phone,
        point: point ?? this.point,
        pointAfter: pointAfter ?? this.pointAfter,
        exchangePoint: exchangePoint ?? this.exchangePoint,
        status: status ?? this.status,
        fullname: fullname ?? this.fullname,
        idcard: idcard ?? this.idcard,
        cardAddress: cardAddress ?? this.cardAddress,
        address: address ?? this.address,
        code: code ?? this.code,
        cardNumber: cardNumber ?? this.cardNumber,
        idcardDate: idcardDate ?? this.idcardDate,
        pin: pin ?? this.pin,
        unreadCount: unreadCount ?? this.unreadCount,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt);
  }
}

//{
//"type": "success",
//"totalAmount": 51,
//"totalItem": 5,
//"bonusPointItems": [
//"5e732fa966e1627324d36491",
//"5e732fa966e1627324d36492",
//"5e732fa966e1627324d36493",
//"5e732fa966e1627324d36494",
//"5e732fa966e1627324d36495"
//],
//"_id": "5e732fa966e1627324d36490",
//"billNumber": 1388,
//"totalPoint": 8031100,
//"farmerId": {
//"phone": [
//"0109123009"
//],
//"point": 60940600,
//"exchangePoint": 17595580,
//"status": "active",
//"_id": "5e538c641fa329602d43ba7f",
//"code": "FC00008999",
//"idcard": "250250010",
//"cardNumber": "",
//"fullname": "Nông dân Test 10",
//"idcardAddress": "Trà Vinh",
//"idcardDate": "1/1/2019",
//"address": "135 Trần Hưng Đạo",
//"createdAt": "2020-02-24T08:42:12.159Z",
//"updatedAt": "2020-03-19T08:39:05.428Z",
//"__v": 0
//},
//"agencyId": {
//"point": 0,
//"pointSpin": 0,
//"pointEmptyBox": 0,
//"revenue": 0,
//"_id": "5e5780b5c934af025f044ce4",
//"code": "J2170",
//"phone": "0987448948",
//"name": "Trần Nhựt Thẩm",
//"level": "L1",
//"address": "",
//"createdAt": "2020-02-27T08:41:25.955Z",
//"updatedAt": "2020-02-27T08:41:25.955Z",
//"__v": 0
//},
//"farmerCode": "FC00008999",
//"agencyCode": "J2170",
//"createdAt": "2020-03-19T08:39:05.430Z",
//"updatedAt": "2020-03-19T08:39:05.430Z",
//"__v": 0
//},

@JsonSerializable()
class Agency {
  @JsonKey(name: '_id')
  final String id;
  final int point;
  final int pointSpin;
  final int pointEmptyBox;
  final int revenue;
  final String code;
  final String phone;
  final String name;
  final String level;
  final String address;
  final String createdAt;
  final String updatedAt;

  Agency(
      {this.id,
      this.point,
      this.pointSpin,
      this.pointEmptyBox,
      this.revenue,
      this.code,
      this.phone,
      this.name,
      this.level,
      this.address,
      this.createdAt,
      this.updatedAt});

  factory Agency.fromJson(Map<String, dynamic> json) => _$AgencyFromJson(json);
  Map<String, dynamic> toJson() => _$AgencyToJson(this);
}

@JsonSerializable()
class OrderDetail {
  @JsonKey(name: '_id')
  final String id;
  final String type;
  final int totalAmount;
  final int totalItem;
  @JsonKey(toJson: _productsToJson)
  final List<Product> bonusPointItems;
  final int billNumber;
  final int totalPoint;
  @JsonKey(toJson: _farmerToJson, nullable: true)
  final Farmer farmerId;
  @JsonKey(toJson: _agencyToJson, nullable: true)
  final Agency agencyId;
  final String farmerCode;
  final String agencyCode;
  final String createdAt;
  final String updatedAt;
  final String cancelDate;

  OrderDetail(
      {this.id,
      this.type,
      this.totalAmount,
      this.totalItem,
      this.bonusPointItems,
      this.billNumber,
      this.totalPoint,
      this.farmerId,
      this.agencyId,
      this.farmerCode,
      this.agencyCode,
      this.createdAt,
      this.updatedAt,
      this.cancelDate});

  factory OrderDetail.fromJson(Map<String, dynamic> json) => _$OrderDetailFromJson(json);
  Map<String, dynamic> toJson() => _$OrderDetailToJson(this);
}

Map<String, dynamic> _farmerToJson(Farmer item) => item?.toJson();
Map<String, dynamic> _agencyToJson(Agency item) => item?.toJson();
List<Map<String, dynamic>> _productsToJson(List<Product> items) => items?.map((item) => item?.toJson())?.toList();

//"_id": "5e2efdcf5df7c439ee35de16",
//"code": "LUAST25",
//"name": "Lúa ST25",
//"bonusPointHistory": "5e2efdcf5df7c439ee35de15",
//"product": "5e291b0f74fef10f024989e5",
//"amount": 1,
//"point": 1000,
//"createdAt": "2020-01-27T15:12:16.074Z",
//"updatedAt": "2020-01-27T15:12:16.074Z",
//"__v": 0
@JsonSerializable()
class Product {
  @JsonKey(name: '_id')
  final String id;
  final String code;
  final String name;
  final String bonusPointHistory;
  final String product;
  final int amount;
  final int point;
  final String createdAt;
  final String updatedAt;

  Product(
      {this.id,
      this.code,
      this.name,
      this.bonusPointHistory,
      this.product,
      this.amount,
      this.point,
      this.createdAt,
      this.updatedAt});

  int get totalPoint => amount * point;

  factory Product.fromJson(Map<String, dynamic> json) => _$ProductFromJson(json);
  Map<String, dynamic> toJson() => _$ProductToJson(this);
}

//"publicFor": [
//"ND",
//"DL_L1",
//"DL_L2"
//],
//"isBanner": true,
//"countSend": 3,
//"_id": "5e6b5125e85ef7030d0130d3",
//"message": "test2",
//"picture": "https://i.imgur.com/sCX6sjT.png",
//"validFrom": "2020-03-12T17:00:00.000Z",
//"validTo": "2020-04-20T17:00:00.000Z",
@JsonSerializable()
class BannerAds {
  @JsonKey(name: '_id')
  final String id;
  final List<String> publicFor;
  final bool isBanner;
  final int countSend;
  final String title;
  final String message;
  final String picture;
  final String validFrom;
  final String validTo;
  final String createdAt;
  final String updatedAt;

  BannerAds(
      {this.id,
      this.publicFor,
      this.isBanner,
      this.countSend,
      this.title,
      this.message,
      this.picture,
      this.validFrom,
      this.validTo,
      this.createdAt,
      this.updatedAt});

  factory BannerAds.fromJson(Map<String, dynamic> json) => _$BannerAdsFromJson(json);
  Map<String, dynamic> toJson() => _$BannerAdsToJson(this);
}

//"farmerId": "5e61d18dcb5f1629c299966d",
//"notifyId": "5e6efd3dd5f8b4097a47512e",
//"message": "Ngày mới vui vẻ!",
//"picture": "https://i.imgur.com/D9mUNjJ.jpg",
@JsonSerializable()
class NotificationData {
  @JsonKey(name: '_id')
  final String id;
  final String farmerId;
  final String notifyId;
  final String title;
  final bool seen;
  final String message;
  final String picture;
  final String createdAt;
  final String updatedAt;

  NotificationData({this.id, this.farmerId, this.notifyId, this.title, this.seen, this.message, this.picture, this.createdAt, this.updatedAt});

  factory NotificationData.fromJson(Map<String, dynamic> json) => _$NotificationDataFromJson(json);
  Map<String, dynamic> toJson() => _$NotificationDataToJson(this);
}

//"productCodes": [],
//"limitDate": 5,
//"limitAmount": 5,
//"gifts": [
//{
//"name": "Xe Honda Wave",
//"code": "GIAI1",
//"image": "https://i.imgur.com/nHMQfuW.png",
//"amount": "1",
//"used": 1,
//"type": "GIFT_SPECIAL"
//},
//{
//"name": "0.5 Chỉ vàng",
//"code": "GIAI2",
//"image": "https://i.imgur.com/WPrqEbW.jpg",
//"amount": "46",
//"used": 46,
//"type": "GIFT"
//},
//{
//"name": "Thẻ cào 10.000đ",
//"code": "GIAI3",
//"image": "https://i.imgur.com/SJiQqFP.png",
//"amount": "319",
//"used": 319,
//"type": "GIFT"
//},
//{
//"name": "Chúc bạn may mắn lần sau",
//"code": "NONE",
//"image": "https://i.imgur.com/mncv1Yi.png",
//"amount": "504",
//"used": 562,
//"type": "NONE"
//}
//],
//"spinPoint": 100,
//"_id": "5e684f0b34b3294e04135aaa",
//"code": "Livestream-11-03",
//"title": "Livestream 11-03",
//"type": "OFFLINE",
//"createdAt": "2020-03-11T02:38:03.862Z",
//"updatedAt": "2020-03-25T09:44:46.246Z",
//"__v": 0,
//"backgroundImage": "https://i.imgur.com/0w7wICk.jpg",
//"bannerImage": "https://i.imgur.com/F8Qp6xA.png",
//"btnTitle": "Bắt đầu quay",
//"buttonColor": "#158b39",
//"endDate": "2020-03-29T17:00:00.000Z",
//"footerImage": "",
//"pinImage": "https://i.imgur.com/sSTgcSR.png",
//"startDate": "2020-03-11T02:38:19.964Z",
//"wheelImage": "https://i.imgur.com/WXe3xjI.png",
//"status": "done",
//"taskId": "5e7b280c10de8255464af42c"

//"name": "Xe Honda Wave",
//"code": "GIAI1",
//"image": "https://i.imgur.com/nHMQfuW.png",
//"amount": "1",
//"used": 1,
//"type": "GIFT_SPECIAL"

@JsonSerializable()
class Gift
{
  final String name;
  final String code;
  final String image;
  final dynamic amount;
  final int used;
  final String type;

  Gift({this.name, this.code, this.image, this.amount, this.used, this.type});

  factory Gift.fromJson(Map<String, dynamic> json) => _$GiftFromJson(json);
  Map<String, dynamic> toJson() => _$GiftToJson(this);
}

//"spinPoint": 100,
//"_id": "5e684f0b34b3294e04135aaa",
//"code": "Livestream-11-03",
//"title": "Livestream 11-03",
//"type": "OFFLINE",
//"createdAt": "2020-03-11T02:38:03.862Z",
//"updatedAt": "2020-03-25T09:44:46.246Z",
//"__v": 0,
//"backgroundImage": "https://i.imgur.com/0w7wICk.jpg",
//"bannerImage": "https://i.imgur.com/F8Qp6xA.png",
//"btnTitle": "Bắt đầu quay",
//"buttonColor": "#158b39",
//"endDate": "2020-03-29T17:00:00.000Z",
//"footerImage": "",
//"pinImage": "https://i.imgur.com/sSTgcSR.png",
//"startDate": "2020-03-11T02:38:19.964Z",
//"wheelImage": "https://i.imgur.com/WXe3xjI.png",
//"status": "done",
//"taskId": "5e7b280c10de8255464af42c"
@JsonSerializable()
class Award {
  @JsonKey(name: '_id')
  final String id;
  final List<dynamic> productCodes;
  final int limitDate;
  final int limitAmount;
  final int spinPoint;
  final String code;
  final String title;
  final String type;
  final String backgroundImage;
  final String bannerImage;
  final String btnTitle;
  final String buttonColor;
  final String startDate;
  final String endDate;
  final String footerImage;
  final String pinImage;
  final String wheelImage;
  final String status;
  final String taskId;
  @JsonKey(toJson: _giftsToJson)
  final List<Gift> gifts;

  Award(
      {this.id,
      this.productCodes,
      this.limitDate,
      this.limitAmount,
      this.spinPoint,
      this.code,
      this.title,
      this.type,
      this.backgroundImage,
      this.bannerImage,
      this.btnTitle,
      this.buttonColor,
      this.startDate,
      this.endDate,
      this.footerImage,
      this.pinImage,
      this.wheelImage,
      this.status,
      this.taskId,
      this.gifts});

  String get giftListInText {
    return gifts.map((item) => '● ${item.name}').toList().join('\n');
  }

  factory Award.fromJson(Map<String, dynamic> json) => _$AwardFromJson(json);
  Map<String, dynamic> toJson() => _$AwardToJson(this);
}

List<Map<String, dynamic>> _giftsToJson(List<Gift> items) => items?.map((item) => item?.toJson())?.toList();


//"isUpdated": false,
//"farmerPhone": [
//"0965432140"
//],
//"isSendTopup": false,
//"_id": "5e89b64e4adde279fa27424d",
//"giftCode": "GIAI3",
//"giftName": "Thẻ cào 10.000đ",
//"spinPoint": 100,
//"farmer": "5e61d18dcb5f1629c299966d",
//"farmerCode": "FC0987123456",
//"productCampaignCode": "THETICHDIEM",
//"productCampaignName": "Vòng quay đổi quà",
//"farmerIdcard": "0987123456",
//"farmerPoint": 279200,
//"farmerFullname": "Minh Tâm",
//"agencyName": "",
//"agencyLevel": "",
//"agencyCode": "",
//"createdAt": "2020-04-05T10:43:26.359Z",
//"updatedAt": "2020-04-05T10:43:26.359Z",
//"__v": 0

@JsonSerializable()
class ExchangePoint {

  @JsonKey(name: '_id')
  final String id;
  final String createdAt;
  final String updatedAt;
  final bool isUpdated;
  final List<String> farmerPhone;
  final bool isSendTopup;
  final String giftCode;
  final String giftName;
  final int spinPoint;
  final String farmer;
  final String farmerCode;
  final String productCampaignCode;
  final String productCampaignName;
  final String farmerIdcard;
  final int farmerPoint;
  final String farmerFullname;
  final String agencyName;
  final String agencyLevel;
  final String agencyCode;

  @JsonKey(ignore: true)
  int count;


  ExchangePoint({this.id, this.isUpdated, this.farmerPhone, this.isSendTopup, this.giftCode, this.giftName, this.spinPoint,
  this.farmer, this.farmerCode, this.productCampaignCode, this.productCampaignName, this.farmerIdcard, this.farmerPoint,
  this.farmerFullname, this.agencyName, this.agencyLevel, this.agencyCode, this.createdAt, this.updatedAt});

  factory ExchangePoint.fromJson(Map<String, dynamic> json) => _$ExchangePointFromJson(json);
  Map<String, dynamic> toJson() => _$ExchangePointToJson(this);
}