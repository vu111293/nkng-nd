// To parse this JSON data, do
//
//     final ratingModel = ratingModelFromJson(jsonString);

import 'dart:convert';

import 'package:loctroi/models/user.dart';

List<RatingModel> ratingModelFromJson(String str) =>
    List<RatingModel>.from(json.decode(str).map((x) => RatingModel.fromJson(x)));

String ratingModelToJson(List<RatingModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class RatingModel {
  String id;
  String raterUserId;
  String rateeUserId;
  double rating;
  dynamic memo;
  String createdAtUnixTimestamp;
  String updatedAtUnixTimestamp;
  bool status;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic deletedAt;
  UserModel raterUser;

  RatingModel({
    this.id,
    this.raterUserId,
    this.rateeUserId,
    this.rating,
    this.memo,
    this.createdAtUnixTimestamp,
    this.updatedAtUnixTimestamp,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.raterUser,
  });

  factory RatingModel.fromJson(Map<String, dynamic> json) => RatingModel(
        id: json["id"],
        raterUserId: json["rater_user_id"],
        rateeUserId: json["ratee_user_id"],
        rating: double.parse(json["rating"].toString()),
        memo: json["memo"],
        createdAtUnixTimestamp: json["created_at_unix_timestamp"],
        updatedAtUnixTimestamp: json["updated_at_unix_timestamp"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
        raterUser: UserModel.fromJson(json["rater_user"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "rater_user_id": raterUserId,
        "ratee_user_id": rateeUserId,
        "rating": rating,
        "memo": memo,
        "created_at_unix_timestamp": createdAtUnixTimestamp,
        "updated_at_unix_timestamp": updatedAtUnixTimestamp,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "deleted_at": deletedAt,
        "rater_user": raterUser.toJson(),
      };
}
