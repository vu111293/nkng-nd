import 'package:json_annotation/json_annotation.dart';
part 'client_model.g.dart';

@JsonSerializable(nullable: false)
class ClientModel {
  String name;
  String phone;

  ClientModel({this.name, this.phone});

  factory ClientModel.fromJson(Map<String, dynamic> json) {
    final jobs = _$ClientModelFromJson(json);
    return jobs;
  }
  Map<String, dynamic> toJson() => _$ClientModelToJson(this);

  @override
  String toString() {
    return 'ClientModel{name: $name, phone: $phone}';
  }
}
