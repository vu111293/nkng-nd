import 'package:json_annotation/json_annotation.dart';
part 'q_and_a_model.g.dart';

@JsonSerializable(nullable: false)
class QAndAModel {
  String id;
  String job_id;
  String question;
  String answer;
  String created_at_unix_timestamp;
  String updated_at_unix_timestamp;
  bool status;
  String created_at;
  String updated_at;
  String deleted_at;

  bool isExpanded = false;

  QAndAModel(
      {this.id,
      this.job_id,
      this.question,
      this.answer,
      this.created_at_unix_timestamp,
      this.updated_at_unix_timestamp,
      this.status,
      this.created_at,
      this.updated_at,
      this.deleted_at,
      this.isExpanded = false});

  factory QAndAModel.fromJson(Map<String, dynamic> json) {
    final notification = _$QAndAModelFromJson(json);
    return notification;
  }
  Map<String, dynamic> toJson() => _$QAndAModelToJson(this);

  @override
  String toString() {
    return 'QAndAModel{question: $question, answer: $answer, isExpanded: $isExpanded}';
  }
}
