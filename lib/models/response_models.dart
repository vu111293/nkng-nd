import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
part 'response_models.g.dart';

@JsonSerializable(nullable: false)
class LDoctor {
  @JsonKey(name: '_id')
  final String id;
  final String avatar;
  final String uid;
  final String fullName;
  final String phone;
  final String email;
  final String address;
  final String type;
  final String status;
  final String position; // chức vụ
  final String hospital; // đơn vị
  final String plant;
  @JsonKey(nullable: true)
  final double area;
  final String otp;
  final String intro; // mô tả
  @JsonKey(nullable: true)
  final double locationLat;
  @JsonKey(nullable: true)
  final double locationLng;
  final String sessionID;
  final String degree; // học vị
  final String subject; // bộ môn
  final String specialized; // chuyên ngành
  final String updatedAt;
  final String createdAt;

  LDoctor(
      {this.id,
      this.avatar,
      this.uid,
      this.fullName,
      this.email,
      this.address,
      this.phone,
      this.type,
      this.status,
      this.position,
      this.hospital,
      this.plant,
      this.area,
      this.otp,
      this.intro,
      this.locationLat,
      this.locationLng,
      this.sessionID,
      this.degree,
      this.subject,
      this.specialized,
      this.updatedAt,
      this.createdAt});

  factory LDoctor.fromJson(Map<String, dynamic> json) {
    final user = _$LDoctorFromJson(json);
    return user;
  }
  Map<String, dynamic> toJson() => _$LDoctorToJson(this);

  @override
  String toString() {
    return fullName;
  }
}

//"status": "active",
//"_id": "5d88a0481177f00620a5d5df",
//"type": "localHospital",
//"long": 50,
//"lat": 30,
//"name": "Hospital B",
//"introduction": "dfagagfsdgs",
//"createdAt": "2019-09-23T10:36:56.684Z",
//"updatedAt": "2019-10-08T03:09:55.351Z",
@JsonSerializable(nullable: false)
class LHospital {
  @JsonKey(name: '_id')
  final String id;
  final String avatar;
  final String type;
  final String address;
  final double long;
  final double lat;
  final String name;
  final String intro;
  final String status;
  final String phone;
  final String updatedAt;
  final String createdAt;
  @JsonKey(ignore: true)
  final List<LDoctor> doctors;

  LHospital(
      {this.id,
      this.avatar,
      this.type,
      this.address,
      this.long,
      this.lat,
      this.name,
      this.intro,
      this.status,
      this.phone,
      this.updatedAt,
      this.createdAt,
      this.doctors});

  factory LHospital.fromJson(Map<String, dynamic> json) {
    final user = _$LHospitalFromJson(json);
    return user;
  }
  Map<String, dynamic> toJson() => _$LHospitalToJson(this);

  LHospital copyWith(
      {String id,
      String type,
      int long,
      int lat,
      String name,
      String intro,
      String address,
      String status,
      String phone,
      String updatedAt,
      String createdAt,
      List<LDoctor> doctors}) {
    return LHospital(
        id: id ?? this.id,
        type: type ?? this.type,
        long: long ?? this.long,
        lat: lat ?? this.lat,
        name: name ?? this.name,
        intro: intro ?? this.intro,
        address: address ?? this.address,
        status: status ?? this.status,
        phone: phone ?? this.phone,
        updatedAt: updatedAt ?? this.updatedAt,
        createdAt: createdAt ?? this.createdAt,
        doctors: doctors ?? this.doctors);
  }

  @override
  String toString() {
    return this.name;
  }

  bool get isPhoneAvailable => phone?.isNotEmpty == true;
  bool get isLocationAvailable => lat != null && long != null;

}

//"status": "active",
//"_id": "5d9ab4f3bc09a8001f7242a2",
//"image": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Pinus_latteri_Aungban.jpg/280px-Pinus_latteri_Aungban.jpg",
//"name": "Cây thông",
//"createdAt": "2019-10-07T03:45:55.615Z",
//"updatedAt": "2019-10-07T03:45:55.615Z",
@JsonSerializable(nullable: false)
class LTree {
  @JsonKey(name: '_id')
  final String id;
  final String name;
  final String image;
  final String status;
  final String updatedAt;
  final String createdAt;

  LTree({this.id, this.name, this.image, this.status, this.updatedAt, this.createdAt});

  factory LTree.fromJson(Map<String, dynamic> json) {
    final user = _$LTreeFromJson(json);
    return user;
  }
  Map<String, dynamic> toJson() => _$LTreeToJson(this);

  @override
  String toString() {
    return name;
  }
}

@JsonSerializable(nullable: false)
class LQuestionResponse {
  @JsonKey(name: '_id')
  final String id;
  final bool doctorCommented;
  final int qtyRate;
  final int qtyComment;
  final int view;
  final dynamic images;
  final dynamic tags;
  final String status;
  final String title;
  final String description;
  final String plant;
  final String doctor;
  final String hospital;
  final String fullName;
  final String address;
  final String phone;
  final String owner;
  final String createdAt;
  final String updatedAt;

  LQuestionResponse(
      {this.id,
      this.doctorCommented,
      this.qtyRate,
      this.qtyComment,
      this.view,
      this.images,
      this.tags,
      this.status,
      this.title,
      this.description,
      this.plant,
      this.doctor,
      this.hospital,
      this.fullName,
      this.address,
      this.phone,
      this.owner,
      this.createdAt,
      this.updatedAt});

  factory LQuestionResponse.fromJson(Map<String, dynamic> json) {
    final user = _$LQuestionResponseFromJson(json);
    return user;
  }
  Map<String, dynamic> toJson() => _$LQuestionResponseToJson(this);

  bool get hasImages => images is List && (images as List).length > 0;
}

@JsonSerializable(nullable: false)
class LPost {
  @JsonKey(name: '_id')
  final String id;
  final int qtyRate;
  final int qtyComment;
  final int view;
  final dynamic images;
  final String title;
  final String description;
  final String content;
  final String type;
  final String thumbnail;
  final String owner;
  final String createdAt;
  final String updatedAt;
  final bool rated;

  LPost(
      {this.id,
      this.thumbnail,
      this.qtyRate,
      this.qtyComment,
      this.view,
      this.images,
      this.title,
      this.description,
      this.owner,
      this.content,
      this.type,
      this.createdAt,
      this.updatedAt,
      this.rated});

  factory LPost.fromJson(Map<String, dynamic> json) {
    final item = _$LPostFromJson(json);
    return item;
  }
  Map<String, dynamic> toJson() => _$LPostToJson(this);
}

//"status": "active",
//"_id": "5d8dc28ab66f1e2625204d62",
//"comment": "5d8d9b810486c217dd4056e7",
//"type": "reply",
//"content": "Trả lời comment thứ nhất trong post",
//"owner": "5d8d8980c419aa11000a7518",
//"createdAt": "2019-09-27T08:04:26.446Z",
//"updatedAt": "2019-09-27T08:04:26.446Z",
@JsonSerializable(nullable: true)
class LComment {
  @JsonKey(name: '_id')
  final String id;
  final String content;
  final String picture;

  @JsonKey(toJson: _ownerToJson, nullable: true)
  final CommentOwner owner;
  final String createdAt;
  final String updatedAt;

  @JsonKey(ignore: true)
  bool editing;

  LComment({this.id, this.content, this.picture, this.owner, this.createdAt, this.updatedAt, this.editing = false});

  factory LComment.fromJson(Map<String, dynamic> json) {
    final item = _$LCommentFromJson(json);
    return item;
  }
  Map<String, dynamic> toJson() => _$LCommentToJson(this);
}

Map<String, dynamic> _ownerToJson(CommentOwner owner) => owner.toJson();

@JsonSerializable(nullable: false)
class CommentOwner {
  @JsonKey(name: '_id')
  final String id;
  @JsonKey(nullable: true)
  final String fullName;
  final String phone;
  @JsonKey(nullable: true)
  final String avatar;

  CommentOwner({this.id, this.fullName, this.phone, this.avatar});

  factory CommentOwner.fromJson(Map<String, dynamic> json) {
    final item = _$CommentOwnerFromJson(json);
    return item;
  }
  Map<String, dynamic> toJson() => _$CommentOwnerToJson(this);
}

//user: string | UserDocument;
//title: string;
//body: string;
//clickAction: string;
//data: any;
//seen: boolean;
//seenAt: Date;
//hash: String;
@JsonSerializable(nullable: false)
class LNoti {
  @JsonKey(name: '_id')
  final String id;
  final String user;
  final String title;
  final String body;
  @JsonKey(nullable: true)
  final dynamic data;
  final String seenAt;
  bool seen;
  final String hash;
  final String createdAt;

  LNoti({this.id, this.user, this.title, this.body, this.data, this.seen, this.seenAt, this.hash, this.createdAt});

  factory LNoti.fromJson(Map<String, dynamic> json) {
    final item = _$LNotiFromJson(json);
    return item;
  }
  Map<String, dynamic> toJson() => _$LNotiToJson(this);
}
