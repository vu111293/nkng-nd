class BaseObject {
  String id;
  bool status;
  String created_at_unix_timestamp;
  String updated_at_unix_timestamp;
  DateTime created_at;
  DateTime updated_at;
  DateTime deleted_at;

  BaseObject.customConstructor(
      id, status, created_at_unix_timestamp, updated_at_unix_timestamp, created_at, updated_at, deleted_at) {
    this.id = id;
    this.status = status;

    this.created_at = created_at;
    this.created_at_unix_timestamp = created_at_unix_timestamp;
    this.updated_at = updated_at;
    this.updated_at_unix_timestamp = updated_at_unix_timestamp;
    this.deleted_at = deleted_at;
  }

  BaseObject({
    this.id,
    this.status,
    this.created_at,
    this.created_at_unix_timestamp,
    this.updated_at,
    this.updated_at_unix_timestamp,
    this.deleted_at,
  });

  BaseObject copyWith({
    String id,
    DateTime status,
    String created_at,
    String created_at_unix_timestamp,
    bool updated_at,
    DateTime updated_at_unix_timestamp,
    DateTime deleted_at,
  }) {
    return BaseObject(
      id: id ?? this.id,
      status: status ?? this.status,
      created_at: created_at ?? this.created_at,
      created_at_unix_timestamp: created_at_unix_timestamp ?? this.created_at_unix_timestamp,
      updated_at: updated_at ?? this.updated_at,
      updated_at_unix_timestamp: updated_at_unix_timestamp ?? this.updated_at_unix_timestamp,
      deleted_at: deleted_at ?? this.deleted_at,
    );
  }

  @override
  String toString() {
    return ",$id ,$status ,$created_at ,$created_at_unix_timestamp ,$updated_at ,$updated_at_unix_timestamp ,$deleted_at";
  }
}
