// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'candidates_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CandidatesModel _$CandidatesModelFromJson(Map<String, dynamic> json) {
  return CandidatesModel(
    call_history: (json['call_history'] as List)
        ?.map((e) => e == null
            ? null
            : CallHistoryModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    jobs: (json['jobs'] as List)
        ?.map((e) =>
            e == null ? null : JobsModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    userModel: json['userModel'] == null
        ? null
        : UserModel.fromJson(json['userModel'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CandidatesModelToJson(CandidatesModel instance) =>
    <String, dynamic>{
      'call_history': instance.call_history,
      'jobs': instance.jobs,
      'userModel': instance.userModel,
    };
