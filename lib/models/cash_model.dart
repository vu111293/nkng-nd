import 'package:json_annotation/json_annotation.dart';

part 'cash_model.g.dart';

// *** PLEASE RUN COMMAND BELOW FOR REBUILD MODELS ****
// flutter packages pub run build_runner build --delete-conflicting-outputs

//                 "id": "35c165f0-965e-11e9-9036-ef8f95a14b59",
//                 "user_id": "0d669670-965e-11e9-9036-ef8f95a14b59",
//                 "history": "test 1",
//                 "money": "10",
//                 "remaining_money": "10",
//                 "type": "INCREASE",
//                 "payer_id": "0d669670-965e-11e9-9036-ef8f95a14b59",
//                 "employee_id": null,
//                 "payment_type": "ONLINE",
//                 "created_at_unix_timestamp": "0",
//                 "updated_at_unix_timestamp": "0",
//                 "status": true,
//                 "state": SUCCESS/FAILED/PENDING,
//                 "created_at": "2019-06-24T15:58:18.459Z",
//                 "updated_at": "2019-06-24T15:58:18.459Z",
//                 "deleted_at": null
@JsonSerializable(nullable: false)
class CashModel {
  String id;
  String user_id;
  String history;
  String money;
  String remaining_money;
  String type;
  String payer_id;
  String employee_id;
  String payment_type;
  String state;
  bool status;
  String created_at_unix_timestamp;
  String updated_at_unix_timestamp;
  String create_at;
  String deleted_at;

  CashModel({
    this.id,
    this.user_id,
    this.history,
    this.money,
    this.remaining_money,
    this.type,
    this.payer_id,
    this.employee_id,
    this.payment_type,
    this.state,
    this.status,
    this.created_at_unix_timestamp,
    this.updated_at_unix_timestamp,
    this.create_at,
    this.deleted_at,
  });

  factory CashModel.fromJson(Map<String, dynamic> json) {
    final cash = _$CashModelFromJson(json);
    return cash;
  }
  Map<String, dynamic> toJson() => _$CashModelToJson(this);

  @override
  String toString() {
    return "CashModel $id ,$user_id";
  }
}
