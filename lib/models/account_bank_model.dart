import 'dart:io';

import 'package:json_annotation/json_annotation.dart';

part 'account_bank_model.g.dart';

// *** PLEASE RUN COMMAND BELOW FOR REBUILD MODELS ****
// flutter packages pub run build_runner build --delete-conflicting-outputs

// this is model for only my account
@JsonSerializable(nullable: false)
class AccountBankModel {
  String id;
  String bankname;
  String account_name;
  String account_number;
  String branch;
  String city;
  AccountBankModel(
    this.id,
    this.bankname,
    this.account_name,
    this.account_number,
    this.branch,
    this.city,
  );

  factory AccountBankModel.fromJson(Map<String, dynamic> json) {
    final accountBank = _$AccountBankModelFromJson(json);
    return accountBank;
  }
  Map<String, dynamic> toJson() => _$AccountBankModelToJson(this);
  @override
  String toString() {
    return "AccountBankModel $id ";
  }
}
