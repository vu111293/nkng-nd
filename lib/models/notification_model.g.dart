// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationModel _$NotificationModelFromJson(Map<String, dynamic> json) {
  return NotificationModel(
    id: json['id'] as String,
    status: json['status'] as bool,
    user_id: json['user_id'] as String,
    title: json['title'] as String,
    content: json['content'] as String,
    data: json['data'],
    interacting_user_id: json['interacting_user_id'] as String,
    interacting_type: json['interacting_type'] as String,
    interacting_content_id: json['interacting_content_id'] as String,
    action: json['action'] as int,
    created_at: DateTime.parse(json['created_at'] as String),
    updated_at: DateTime.parse(json['updated_at'] as String),
  );
}

Map<String, dynamic> _$NotificationModelToJson(NotificationModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'status': instance.status,
      'user_id': instance.user_id,
      'title': instance.title,
      'content': instance.content,
      'data': instance.data,
      'interacting_user_id': instance.interacting_user_id,
      'interacting_type': instance.interacting_type,
      'interacting_content_id': instance.interacting_content_id,
      'action': instance.action,
      'created_at': instance.created_at.toIso8601String(),
      'updated_at': instance.updated_at.toIso8601String(),
    };
