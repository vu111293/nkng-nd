// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'employee_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EmployeeModel _$EmployeeModelFromJson(Map<String, dynamic> json) {
  return EmployeeModel(
    id: json['id'] as String,
    fullname: json['fullname'] as String,
    avatar: json['avatar'] as String,
    phone: json['phone'] as String,
    email: json['email'] as String,
    username: json['username'] as String,
    type: json['type'] as String,
    status: json['status'] as bool,
  );
}

Map<String, dynamic> _$EmployeeModelToJson(EmployeeModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'fullname': instance.fullname,
      'avatar': instance.avatar,
      'phone': instance.phone,
      'email': instance.email,
      'username': instance.username,
      'type': instance.type,
      'status': instance.status,
    };
