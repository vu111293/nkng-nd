// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chart_info_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChartInfoModel _$ChartInfoModelFromJson(Map<String, dynamic> json) {
  return ChartInfoModel(
    index: json['index'] as int,
    day: json['day'] as String,
    job_of_user_number: json['job_of_user_number'] as int,
    money_of_user_number: json['money_of_user_number'] as int,
    money_of_user: json['money_of_user'] as int,
    call_history_of_user_number: json['call_history_of_user_number'] as int,
  );
}

Map<String, dynamic> _$ChartInfoModelToJson(ChartInfoModel instance) =>
    <String, dynamic>{
      'index': instance.index,
      'day': instance.day,
      'job_of_user_number': instance.job_of_user_number,
      'money_of_user_number': instance.money_of_user_number,
      'money_of_user': instance.money_of_user,
      'call_history_of_user_number': instance.call_history_of_user_number,
    };
