// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'slider_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SliderModel _$SliderModelFromJson(Map<String, dynamic> json) {
  return SliderModel(
    id: json['id'] as String,
    title: json['title'] as String,
    image: json['image'] as String,
    description_1: json['description_1'] as String,
    description_2: json['description_2'] as String,
    color: json['color'] as String,
    position: json['position'] as String,
    state: json['state'] as bool,
    created_at_unix_timestamp: json['created_at_unix_timestamp'] as String,
    updated_at_unix_timestamp: json['updated_at_unix_timestamp'] as String,
    status: json['status'] as bool,
    created_at: json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String),
    updated_at: json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String),
    deleted_at: json['deleted_at'] as String,
  );
}

Map<String, dynamic> _$SliderModelToJson(SliderModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'image': instance.image,
      'description_1': instance.description_1,
      'description_2': instance.description_2,
      'color': instance.color,
      'position': instance.position,
      'state': instance.state,
      'created_at_unix_timestamp': instance.created_at_unix_timestamp,
      'updated_at_unix_timestamp': instance.updated_at_unix_timestamp,
      'status': instance.status,
      'created_at': instance.created_at?.toIso8601String(),
      'updated_at': instance.updated_at?.toIso8601String(),
      'deleted_at': instance.deleted_at,
    };
