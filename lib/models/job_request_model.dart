import 'package:json_annotation/json_annotation.dart';
import 'package:loctroi/models/jobs_model.dart';
import 'package:loctroi/models/user.dart';
part "job_request_model.g.dart";

@JsonSerializable(nullable: false)
class JobRequestModel {
  String id;
  String user_id;
  String job_id;
  String state;
  bool is_telesale_request;
  String created_at_unix_timestamp;
  String updated_at_unix_timestamp;
  bool status;
  String created_at;
  String updated_at;
  String deleted_at;
  UserModel user;
  JobsModel job;
  @JsonKey(ignore: true)
  bool isSelected;

  JobRequestModel(
      {String id,
      String user_id,
      String job_id,
      String state,
      bool is_telesale_request,
      String created_at_unix_timestamp,
      String updated_at_unix_timestamp,
      bool status,
      String created_at,
      String updated_at,
      String deletedAt,
      bool isSelected,
      JobsModel job,
      UserModel user}) {
    this.id = id;
    this.user_id = user_id;
    this.job_id = job_id;
    this.state = state;
    this.is_telesale_request = is_telesale_request;
    this.created_at_unix_timestamp = created_at_unix_timestamp;
    this.updated_at_unix_timestamp = updated_at_unix_timestamp;
    this.status = status;
    this.created_at = created_at;
    this.updated_at = updated_at;
    this.deleted_at = deleted_at;
    this.user = user;
    this.isSelected = isSelected;
    this.job = job;
  }

  factory JobRequestModel.fromJson(Map<String, dynamic> json) {
    final jobRequest = _$JobRequestModelFromJson(json);
    return jobRequest;
  }
  Map<String, dynamic> toJson() => _$JobRequestModelToJson(this);

  @override
  String toString() {
    return "<JobRequestModel: $id>";
  }
}
