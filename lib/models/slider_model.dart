import 'package:json_annotation/json_annotation.dart';

part 'slider_model.g.dart';

// *** PLEASE RUN COMMAND BELOW FOR REBUILD MODELS ****
// flutter packages pub run build_runner build --delete-conflicting-outputs

@JsonSerializable()
class SliderModel {
  String id;
  String title;
  String image;
  String description_1;
  String description_2;
  String color;
  String position;

  bool state;
  String created_at_unix_timestamp;
  String updated_at_unix_timestamp;
  bool status;
  DateTime created_at;
  DateTime updated_at;
  String deleted_at;

  SliderModel({
    this.id,
    this.title,
    this.image,
    this.description_1,
    this.description_2,
    this.color,
    this.position,
    this.state,
    this.created_at_unix_timestamp,
    this.updated_at_unix_timestamp,
    this.status,
    this.created_at,
    this.updated_at,
    this.deleted_at,
  });

  factory SliderModel.fromJson(Map<String, dynamic> json) {
    final Slider = _$SliderModelFromJson(json);
    return Slider;
  }
  Map<String, dynamic> toJson() => _$SliderModelToJson(this);

  @override
  String toString() {
    return "SliderModel $id ,$title ,$image ,$description_1 ----";
  }
  // @override
  // String toString() {
  //   return "SliderModel $id ,$status ,$user_id ,$requirement ,$list_clients ,$instruction ,$interacting_type ,$interacting_content_id ,$action ,$created_at_unix_timestamp ,$updated_at_unix_timestamp";
  // }
}
