import 'package:json_annotation/json_annotation.dart';
part 'employee_model.g.dart';

@JsonSerializable(nullable: false)
class EmployeeModel {
  final String id;
  final String fullname;
  final String avatar;
  final String phone;
  final String email;
  final String username;
  final String type;
  final bool status;
  // final DateTime created_at;
  // final DateTime updated_at;
  // final DateTime deleted_at;

  EmployeeModel({
    this.id,
    this.fullname,
    this.avatar,
    this.phone,
    this.email,
    this.username,
    this.type,
    this.status,
    // this.created_at,
    // this.updated_at,
    // this.deleted_at,
  });

  EmployeeModel copyWith({String id, String title, String url}) {
    return EmployeeModel(
      id: id ?? this.id,
      fullname: fullname ?? this.fullname,
      avatar: avatar ?? this.avatar,
      phone: phone ?? this.phone,
      email: email ?? this.email,
      username: username ?? this.username,
      type: type ?? this.type,
      status: status ?? this.status,
      // created_at: created_at ?? this.created_at,
      // updated_at: updated_at ?? this.updated_at,
      // deleted_at: deleted_at ?? this.deleted_at,
    );
  }

  @override
  String toString() {
    return "$username";
  }
}
// "id": "bc353c60-8e89-11e9-b2d3-c7f16ca8a89a",
// "fullname": "Hoang Phuc",
// "avatar": "https://server.hitek.com.vn:9001/api/v1/image/get/resized-image-1560505780188.png",
// "phone": "0987654321",
// "email": "hoangphuc@gmail.com",
// "username": "hoangphuc",
// "type": "SUPERADMIN",
// "status": true,
// "created_at": "2019-06-14T16:49:43.085Z",
// "updated_at": "2019-06-14T17:16:40.900Z",
// "deleted_at": null
// id,
// fullname,
// avatar,
// phone,
// email,
// username,
// type,
// status,
// created_at,
// updated_at,
// deleted_at,
