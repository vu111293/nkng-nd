// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'video_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VideoModel _$VideoModelFromJson(Map<String, dynamic> json) {
  return VideoModel(
    title: json['title'] as String,
    description: json['description'] as String,
    videoId: json['videoId'] as String,
    thumb: json['thumb'] as String,
    published: json['published'] as String,
  );
}

Map<String, dynamic> _$VideoModelToJson(VideoModel instance) =>
    <String, dynamic>{
      'title': instance.title,
      'description': instance.description,
      'videoId': instance.videoId,
      'thumb': instance.thumb,
      'published': instance.published,
    };
