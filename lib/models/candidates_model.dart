import 'package:json_annotation/json_annotation.dart';
import 'package:loctroi/models/call_history_model.dart';
import 'package:loctroi/models/jobs_model.dart';
import 'package:loctroi/models/user.dart';

part 'candidates_model.g.dart';

// *** PLEASE RUN COMMAND BELOW FOR REBUILD MODELS ****
// flutter packages pub run build_runner build --delete-conflicting-outputs

@JsonSerializable(nullable: true)
class CandidatesModel {
  List<CallHistoryModel> call_history;
  List<JobsModel> jobs;
  UserModel userModel;
  CandidatesModel({this.call_history, this.jobs, this.userModel});

  factory CandidatesModel.fromJson(Map<String, dynamic> json) {
    final jobs = _$CandidatesModelFromJson(json);
    return jobs;
  }
  Map<String, dynamic> toJson() => _$CandidatesModelToJson(this);

  @override
  String toString() {
    return "CandidatesModel $call_history $jobs $userModel";
  }
}
