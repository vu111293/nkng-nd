// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'call_history_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CallHistoryModel _$CallHistoryModelFromJson(Map<String, dynamic> json) {
  return CallHistoryModel(
    id: json['id'] as String,
    user_id: json['user_id'] as String,
    job_id: json['job_id'] as String,
    note: json['note'] as String,
    should_call: json['should_call'] as bool,
    duration: json['duration'] as int,
    phone: json['phone'] as String,
    answer: json['answer'] as String,
    client_phone: json['client_phone'] as String,
    client_name: json['client_name'] as String,
    enterprise_user_id: json['enterprise_user_id'] as String,
    created_at_unix_timestamp: json['created_at_unix_timestamp'] as String,
    updated_at_unix_timestamp: json['updated_at_unix_timestamp'] as String,
    cancel_reason: json['cancel_reason'] as String,
    status: json['status'] as bool,
    created_at: json['created_at'] as String,
    updated_at: json['updated_at'] as String,
    deleted_at: json['deleted_at'] as String,
    job: json['job'] == null
        ? null
        : JobsModel.fromJson(json['job'] as Map<String, dynamic>),
    user: json['user'] == null
        ? null
        : UserModel.fromJson(json['user'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CallHistoryModelToJson(CallHistoryModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'user_id': instance.user_id,
      'job_id': instance.job_id,
      'note': instance.note,
      'duration': instance.duration,
      'should_call': instance.should_call,
      'phone': instance.phone,
      'answer': instance.answer,
      'client_phone': instance.client_phone,
      'client_name': instance.client_name,
      'enterprise_user_id': instance.enterprise_user_id,
      'created_at_unix_timestamp': instance.created_at_unix_timestamp,
      'updated_at_unix_timestamp': instance.updated_at_unix_timestamp,
      'cancel_reason': instance.cancel_reason,
      'status': instance.status,
      'created_at': instance.created_at,
      'updated_at': instance.updated_at,
      'deleted_at': instance.deleted_at,
      'job': instance.job,
      'user': instance.user,
    };
