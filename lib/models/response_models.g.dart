// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LDoctor _$LDoctorFromJson(Map<String, dynamic> json) {
  return LDoctor(
    id: json['_id'] as String,
    avatar: json['avatar'] as String,
    uid: json['uid'] as String,
    fullName: json['fullName'] as String,
    email: json['email'] as String,
    address: json['address'] as String,
    phone: json['phone'] as String,
    type: json['type'] as String,
    status: json['status'] as String,
    position: json['position'] as String,
    hospital: json['hospital'] as String,
    plant: json['plant'] as String,
    area: (json['area'] as num)?.toDouble(),
    otp: json['otp'] as String,
    intro: json['intro'] as String,
    locationLat: (json['locationLat'] as num)?.toDouble(),
    locationLng: (json['locationLng'] as num)?.toDouble(),
    sessionID: json['sessionID'] as String,
    degree: json['degree'] as String,
    subject: json['subject'] as String,
    specialized: json['specialized'] as String,
    updatedAt: json['updatedAt'] as String,
    createdAt: json['createdAt'] as String,
  );
}

Map<String, dynamic> _$LDoctorToJson(LDoctor instance) => <String, dynamic>{
      '_id': instance.id,
      'avatar': instance.avatar,
      'uid': instance.uid,
      'fullName': instance.fullName,
      'phone': instance.phone,
      'email': instance.email,
      'address': instance.address,
      'type': instance.type,
      'status': instance.status,
      'position': instance.position,
      'hospital': instance.hospital,
      'plant': instance.plant,
      'area': instance.area,
      'otp': instance.otp,
      'intro': instance.intro,
      'locationLat': instance.locationLat,
      'locationLng': instance.locationLng,
      'sessionID': instance.sessionID,
      'degree': instance.degree,
      'subject': instance.subject,
      'specialized': instance.specialized,
      'updatedAt': instance.updatedAt,
      'createdAt': instance.createdAt,
    };

LHospital _$LHospitalFromJson(Map<String, dynamic> json) {
  return LHospital(
    id: json['_id'] as String,
    avatar: json['avatar'] as String,
    type: json['type'] as String,
    address: json['address'] as String,
    long: (json['long'] as num).toDouble(),
    lat: (json['lat'] as num).toDouble(),
    name: json['name'] as String,
    intro: json['intro'] as String,
    status: json['status'] as String,
    phone: json['phone'] as String,
    updatedAt: json['updatedAt'] as String,
    createdAt: json['createdAt'] as String,
  );
}

Map<String, dynamic> _$LHospitalToJson(LHospital instance) => <String, dynamic>{
      '_id': instance.id,
      'avatar': instance.avatar,
      'type': instance.type,
      'address': instance.address,
      'long': instance.long,
      'lat': instance.lat,
      'name': instance.name,
      'intro': instance.intro,
      'status': instance.status,
      'phone': instance.phone,
      'updatedAt': instance.updatedAt,
      'createdAt': instance.createdAt,
    };

LTree _$LTreeFromJson(Map<String, dynamic> json) {
  return LTree(
    id: json['_id'] as String,
    name: json['name'] as String,
    image: json['image'] as String,
    status: json['status'] as String,
    updatedAt: json['updatedAt'] as String,
    createdAt: json['createdAt'] as String,
  );
}

Map<String, dynamic> _$LTreeToJson(LTree instance) => <String, dynamic>{
      '_id': instance.id,
      'name': instance.name,
      'image': instance.image,
      'status': instance.status,
      'updatedAt': instance.updatedAt,
      'createdAt': instance.createdAt,
    };

LQuestionResponse _$LQuestionResponseFromJson(Map<String, dynamic> json) {
  return LQuestionResponse(
    id: json['_id'] as String,
    doctorCommented: json['doctorCommented'] as bool,
    qtyRate: json['qtyRate'] as int,
    qtyComment: json['qtyComment'] as int,
    view: json['view'] as int,
    images: json['images'],
    tags: json['tags'],
    status: json['status'] as String,
    title: json['title'] as String,
    description: json['description'] as String,
    plant: json['plant'] as String,
    doctor: json['doctor'] as String,
    hospital: json['hospital'] as String,
    fullName: json['fullName'] as String,
    address: json['address'] as String,
    phone: json['phone'] as String,
    owner: json['owner'] as String,
    createdAt: json['createdAt'] as String,
    updatedAt: json['updatedAt'] as String,
  );
}

Map<String, dynamic> _$LQuestionResponseToJson(LQuestionResponse instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'doctorCommented': instance.doctorCommented,
      'qtyRate': instance.qtyRate,
      'qtyComment': instance.qtyComment,
      'view': instance.view,
      'images': instance.images,
      'tags': instance.tags,
      'status': instance.status,
      'title': instance.title,
      'description': instance.description,
      'plant': instance.plant,
      'doctor': instance.doctor,
      'hospital': instance.hospital,
      'fullName': instance.fullName,
      'address': instance.address,
      'phone': instance.phone,
      'owner': instance.owner,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
    };

LPost _$LPostFromJson(Map<String, dynamic> json) {
  return LPost(
    id: json['_id'] as String,
    thumbnail: json['thumbnail'] as String,
    qtyRate: json['qtyRate'] as int,
    qtyComment: json['qtyComment'] as int,
    view: json['view'] as int,
    images: json['images'],
    title: json['title'] as String,
    description: json['description'] as String,
    owner: json['owner'] as String,
    content: json['content'] as String,
    type: json['type'] as String,
    createdAt: json['createdAt'] as String,
    updatedAt: json['updatedAt'] as String,
    rated: json['rated'] as bool,
  );
}

Map<String, dynamic> _$LPostToJson(LPost instance) => <String, dynamic>{
      '_id': instance.id,
      'qtyRate': instance.qtyRate,
      'qtyComment': instance.qtyComment,
      'view': instance.view,
      'images': instance.images,
      'title': instance.title,
      'description': instance.description,
      'content': instance.content,
      'type': instance.type,
      'thumbnail': instance.thumbnail,
      'owner': instance.owner,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'rated': instance.rated,
    };

LComment _$LCommentFromJson(Map<String, dynamic> json) {
  return LComment(
    id: json['_id'] as String,
    content: json['content'] as String,
    picture: json['picture'] as String,
    owner: json['owner'] == null
        ? null
        : CommentOwner.fromJson(json['owner'] as Map<String, dynamic>),
    createdAt: json['createdAt'] as String,
    updatedAt: json['updatedAt'] as String,
  );
}

Map<String, dynamic> _$LCommentToJson(LComment instance) => <String, dynamic>{
      '_id': instance.id,
      'content': instance.content,
      'picture': instance.picture,
      'owner': _ownerToJson(instance.owner),
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
    };

CommentOwner _$CommentOwnerFromJson(Map<String, dynamic> json) {
  return CommentOwner(
    id: json['_id'] as String,
    fullName: json['fullName'] as String,
    phone: json['phone'] as String,
    avatar: json['avatar'] as String,
  );
}

Map<String, dynamic> _$CommentOwnerToJson(CommentOwner instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'fullName': instance.fullName,
      'phone': instance.phone,
      'avatar': instance.avatar,
    };

LNoti _$LNotiFromJson(Map<String, dynamic> json) {
  return LNoti(
    id: json['_id'] as String,
    user: json['user'] as String,
    title: json['title'] as String,
    body: json['body'] as String,
    data: json['data'],
    seen: json['seen'] as bool,
    seenAt: json['seenAt'] as String,
    hash: json['hash'] as String,
    createdAt: json['createdAt'] as String,
  );
}

Map<String, dynamic> _$LNotiToJson(LNoti instance) => <String, dynamic>{
      '_id': instance.id,
      'user': instance.user,
      'title': instance.title,
      'body': instance.body,
      'data': instance.data,
      'seenAt': instance.seenAt,
      'seen': instance.seen,
      'hash': instance.hash,
      'createdAt': instance.createdAt,
    };
