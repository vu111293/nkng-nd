import 'package:json_annotation/json_annotation.dart';
import 'package:loctroi/models/jobs_model.dart';
import 'package:loctroi/models/user.dart';

part 'call_history_model.g.dart';

// *** PLEASE RUN COMMAND BELOW FOR REBUILD MODELS ****
// flutter packages pub run build_runner build --delete-conflicting-outputs

@JsonSerializable(nullable: true)
class CallHistoryModel {
  String id;
  String user_id;
  String job_id;
  String note;
  int duration;
  bool should_call;
  String phone;
  String answer;
  String client_phone;
  String client_name;
  String enterprise_user_id;
  String created_at_unix_timestamp;
  String updated_at_unix_timestamp;
  String cancel_reason;
  bool status;
  String created_at;
  String updated_at;
  String deleted_at;
  JobsModel job;
  UserModel user;

  CallHistoryModel({
    this.id,
    this.user_id,
    this.job_id,
    this.note,
    this.should_call,
    this.duration,
    this.phone,
    this.answer,
    this.client_phone,
    this.client_name,
    this.enterprise_user_id,
    this.created_at_unix_timestamp,
    this.updated_at_unix_timestamp,
    this.cancel_reason,
    this.status,
    this.created_at,
    this.updated_at,
    this.deleted_at,
    this.job,
    this.user,
  });

  factory CallHistoryModel.fromJson(Map<String, dynamic> json) {
    final history = _$CallHistoryModelFromJson(json);
    return history;
  }
  Map<String, dynamic> toJson() => _$CallHistoryModelToJson(this);

  @override
  String toString() {
    return "CallHistoryModel $id ,$status ,$user_id, $job";
  }
}
// this.id
// ,this.user_id
// ,this.job_id
// ,this.note
// ,this.should_call
// ,this.phone
// ,this.answer
// ,this.client_phone
// ,this.client_name
// ,this.enterprise_user_id
// ,this.created_at_unix_timestamp
// ,this.updated_at_unix_timestamp
// ,this.status
// ,this.created_at
// ,this.updated_at
// ,this.deleted_at,
