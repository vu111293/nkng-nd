// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'q_and_a_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

QAndAModel _$QAndAModelFromJson(Map<String, dynamic> json) {
  return QAndAModel(
    id: json['id'] as String,
    job_id: json['job_id'] as String,
    question: json['question'] as String,
    answer: json['answer'] as String,
    created_at_unix_timestamp: json['created_at_unix_timestamp'] as String,
    updated_at_unix_timestamp: json['updated_at_unix_timestamp'] as String,
    status: json['status'] as bool,
    created_at: json['created_at'] as String,
    updated_at: json['updated_at'] as String,
    deleted_at: json['deleted_at'] as String,
    isExpanded: json['isExpanded'] as bool,
  );
}

Map<String, dynamic> _$QAndAModelToJson(QAndAModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'job_id': instance.job_id,
      'question': instance.question,
      'answer': instance.answer,
      'created_at_unix_timestamp': instance.created_at_unix_timestamp,
      'updated_at_unix_timestamp': instance.updated_at_unix_timestamp,
      'status': instance.status,
      'created_at': instance.created_at,
      'updated_at': instance.updated_at,
      'deleted_at': instance.deleted_at,
      'isExpanded': instance.isExpanded,
    };
