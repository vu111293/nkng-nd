// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'business_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Farmer _$FarmerFromJson(Map<String, dynamic> json) {
  return Farmer(
    id: json['_id'] as String,
    phone: (json['phone'] as List)?.map((e) => e as String)?.toList(),
    point: json['point'] as int,
    pointAfter: json['pointAfter'] as int,
    exchangePoint: json['exchangePoint'] as int,
    status: json['status'] as String,
    fullname: json['fullname'] as String,
    idcard: json['idcard'] as String,
    cardAddress: json['idcardAddress'] as String,
    address: json['address'] as String,
    code: json['code'] as String,
    cardNumber: json['cardNumber'] as String,
    idcardDate: json['idcardDate'] as String,
    pin: json['pin'] as String,
    unreadCount: json['unreadCount'] as int,
    createdAt: json['createdAt'] as String,
    updatedAt: json['updatedAt'] as String,
  );
}

Map<String, dynamic> _$FarmerToJson(Farmer instance) => <String, dynamic>{
      '_id': instance.id,
      'phone': instance.phone,
      'point': instance.point,
      'pointAfter': instance.pointAfter,
      'exchangePoint': instance.exchangePoint,
      'status': instance.status,
      'fullname': instance.fullname,
      'idcard': instance.idcard,
      'idcardAddress': instance.cardAddress,
      'address': instance.address,
      'code': instance.code,
      'cardNumber': instance.cardNumber,
      'idcardDate': instance.idcardDate,
      'pin': instance.pin,
      'unreadCount': instance.unreadCount,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
    };

Agency _$AgencyFromJson(Map<String, dynamic> json) {
  return Agency(
    id: json['_id'] as String,
    point: json['point'] as int,
    pointSpin: json['pointSpin'] as int,
    pointEmptyBox: json['pointEmptyBox'] as int,
    revenue: json['revenue'] as int,
    code: json['code'] as String,
    phone: json['phone'] as String,
    name: json['name'] as String,
    level: json['level'] as String,
    address: json['address'] as String,
    createdAt: json['createdAt'] as String,
    updatedAt: json['updatedAt'] as String,
  );
}

Map<String, dynamic> _$AgencyToJson(Agency instance) => <String, dynamic>{
      '_id': instance.id,
      'point': instance.point,
      'pointSpin': instance.pointSpin,
      'pointEmptyBox': instance.pointEmptyBox,
      'revenue': instance.revenue,
      'code': instance.code,
      'phone': instance.phone,
      'name': instance.name,
      'level': instance.level,
      'address': instance.address,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
    };

OrderDetail _$OrderDetailFromJson(Map<String, dynamic> json) {
  return OrderDetail(
    id: json['_id'] as String,
    type: json['type'] as String,
    totalAmount: json['totalAmount'] as int,
    totalItem: json['totalItem'] as int,
    bonusPointItems: (json['bonusPointItems'] as List)
        ?.map((e) =>
            e == null ? null : Product.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    billNumber: json['billNumber'] as int,
    totalPoint: json['totalPoint'] as int,
    farmerId: json['farmerId'] == null
        ? null
        : Farmer.fromJson(json['farmerId'] as Map<String, dynamic>),
    agencyId: json['agencyId'] == null
        ? null
        : Agency.fromJson(json['agencyId'] as Map<String, dynamic>),
    farmerCode: json['farmerCode'] as String,
    agencyCode: json['agencyCode'] as String,
    createdAt: json['createdAt'] as String,
    updatedAt: json['updatedAt'] as String,
    cancelDate: json['cancelDate'] as String,
  );
}

Map<String, dynamic> _$OrderDetailToJson(OrderDetail instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'type': instance.type,
      'totalAmount': instance.totalAmount,
      'totalItem': instance.totalItem,
      'bonusPointItems': _productsToJson(instance.bonusPointItems),
      'billNumber': instance.billNumber,
      'totalPoint': instance.totalPoint,
      'farmerId': _farmerToJson(instance.farmerId),
      'agencyId': _agencyToJson(instance.agencyId),
      'farmerCode': instance.farmerCode,
      'agencyCode': instance.agencyCode,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'cancelDate': instance.cancelDate,
    };

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product(
    id: json['_id'] as String,
    code: json['code'] as String,
    name: json['name'] as String,
    bonusPointHistory: json['bonusPointHistory'] as String,
    product: json['product'] as String,
    amount: json['amount'] as int,
    point: json['point'] as int,
    createdAt: json['createdAt'] as String,
    updatedAt: json['updatedAt'] as String,
  );
}

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      '_id': instance.id,
      'code': instance.code,
      'name': instance.name,
      'bonusPointHistory': instance.bonusPointHistory,
      'product': instance.product,
      'amount': instance.amount,
      'point': instance.point,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
    };

BannerAds _$BannerAdsFromJson(Map<String, dynamic> json) {
  return BannerAds(
    id: json['_id'] as String,
    publicFor: (json['publicFor'] as List)?.map((e) => e as String)?.toList(),
    isBanner: json['isBanner'] as bool,
    countSend: json['countSend'] as int,
    title: json['title'] as String,
    message: json['message'] as String,
    picture: json['picture'] as String,
    validFrom: json['validFrom'] as String,
    validTo: json['validTo'] as String,
    createdAt: json['createdAt'] as String,
    updatedAt: json['updatedAt'] as String,
  );
}

Map<String, dynamic> _$BannerAdsToJson(BannerAds instance) => <String, dynamic>{
      '_id': instance.id,
      'publicFor': instance.publicFor,
      'isBanner': instance.isBanner,
      'countSend': instance.countSend,
      'title': instance.title,
      'message': instance.message,
      'picture': instance.picture,
      'validFrom': instance.validFrom,
      'validTo': instance.validTo,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
    };

NotificationData _$NotificationDataFromJson(Map<String, dynamic> json) {
  return NotificationData(
    id: json['_id'] as String,
    farmerId: json['farmerId'] as String,
    notifyId: json['notifyId'] as String,
    title: json['title'] as String,
    seen: json['seen'] as bool,
    message: json['message'] as String,
    picture: json['picture'] as String,
    createdAt: json['createdAt'] as String,
    updatedAt: json['updatedAt'] as String,
  );
}

Map<String, dynamic> _$NotificationDataToJson(NotificationData instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'farmerId': instance.farmerId,
      'notifyId': instance.notifyId,
      'title': instance.title,
      'seen': instance.seen,
      'message': instance.message,
      'picture': instance.picture,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
    };

Gift _$GiftFromJson(Map<String, dynamic> json) {
  return Gift(
    name: json['name'] as String,
    code: json['code'] as String,
    image: json['image'] as String,
    amount: json['amount'],
    used: json['used'] as int,
    type: json['type'] as String,
  );
}

Map<String, dynamic> _$GiftToJson(Gift instance) => <String, dynamic>{
      'name': instance.name,
      'code': instance.code,
      'image': instance.image,
      'amount': instance.amount,
      'used': instance.used,
      'type': instance.type,
    };

Award _$AwardFromJson(Map<String, dynamic> json) {
  return Award(
    id: json['_id'] as String,
    productCodes: json['productCodes'] as List,
    limitDate: json['limitDate'] as int,
    limitAmount: json['limitAmount'] as int,
    spinPoint: json['spinPoint'] as int,
    code: json['code'] as String,
    title: json['title'] as String,
    type: json['type'] as String,
    backgroundImage: json['backgroundImage'] as String,
    bannerImage: json['bannerImage'] as String,
    btnTitle: json['btnTitle'] as String,
    buttonColor: json['buttonColor'] as String,
    startDate: json['startDate'] as String,
    endDate: json['endDate'] as String,
    footerImage: json['footerImage'] as String,
    pinImage: json['pinImage'] as String,
    wheelImage: json['wheelImage'] as String,
    status: json['status'] as String,
    taskId: json['taskId'] as String,
    gifts: (json['gifts'] as List)
        ?.map(
            (e) => e == null ? null : Gift.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$AwardToJson(Award instance) => <String, dynamic>{
      '_id': instance.id,
      'productCodes': instance.productCodes,
      'limitDate': instance.limitDate,
      'limitAmount': instance.limitAmount,
      'spinPoint': instance.spinPoint,
      'code': instance.code,
      'title': instance.title,
      'type': instance.type,
      'backgroundImage': instance.backgroundImage,
      'bannerImage': instance.bannerImage,
      'btnTitle': instance.btnTitle,
      'buttonColor': instance.buttonColor,
      'startDate': instance.startDate,
      'endDate': instance.endDate,
      'footerImage': instance.footerImage,
      'pinImage': instance.pinImage,
      'wheelImage': instance.wheelImage,
      'status': instance.status,
      'taskId': instance.taskId,
      'gifts': _giftsToJson(instance.gifts),
    };

ExchangePoint _$ExchangePointFromJson(Map<String, dynamic> json) {
  return ExchangePoint(
    id: json['_id'] as String,
    isUpdated: json['isUpdated'] as bool,
    farmerPhone:
        (json['farmerPhone'] as List)?.map((e) => e as String)?.toList(),
    isSendTopup: json['isSendTopup'] as bool,
    giftCode: json['giftCode'] as String,
    giftName: json['giftName'] as String,
    spinPoint: json['spinPoint'] as int,
    farmer: json['farmer'] as String,
    farmerCode: json['farmerCode'] as String,
    productCampaignCode: json['productCampaignCode'] as String,
    productCampaignName: json['productCampaignName'] as String,
    farmerIdcard: json['farmerIdcard'] as String,
    farmerPoint: json['farmerPoint'] as int,
    farmerFullname: json['farmerFullname'] as String,
    agencyName: json['agencyName'] as String,
    agencyLevel: json['agencyLevel'] as String,
    agencyCode: json['agencyCode'] as String,
    createdAt: json['createdAt'] as String,
    updatedAt: json['updatedAt'] as String,
  );
}

Map<String, dynamic> _$ExchangePointToJson(ExchangePoint instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'isUpdated': instance.isUpdated,
      'farmerPhone': instance.farmerPhone,
      'isSendTopup': instance.isSendTopup,
      'giftCode': instance.giftCode,
      'giftName': instance.giftName,
      'spinPoint': instance.spinPoint,
      'farmer': instance.farmer,
      'farmerCode': instance.farmerCode,
      'productCampaignCode': instance.productCampaignCode,
      'productCampaignName': instance.productCampaignName,
      'farmerIdcard': instance.farmerIdcard,
      'farmerPoint': instance.farmerPoint,
      'farmerFullname': instance.farmerFullname,
      'agencyName': instance.agencyName,
      'agencyLevel': instance.agencyLevel,
      'agencyCode': instance.agencyCode,
    };
