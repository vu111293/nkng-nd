import 'dart:math';

import 'package:intl/intl.dart';

class DataTestModel {
  ///tạo props:
  final String avatar;
  final String title;
  final String detail;
  final String notification;
  final String date;
  final int phonecall;
  final int amount;
  final bool check;

  ///construstor:
  DataTestModel({
    this.avatar,
    this.title,
    this.detail,
    this.notification,
    this.date,
    this.phonecall,
    this.amount,
    this.check,
  });

  ///copyWith:
  DataTestModel copyWith({
    String avatar,
    String title,
    String detail,
    String notification,
    String date,
    int phonecall,
    int amount,
    bool check,
  }) {
    return DataTestModel(
      title: title ?? this.title,
      avatar: avatar ?? this.avatar,
      detail: detail ?? this.detail,
      notification: notification ?? this.notification,
      date: date ?? this.date,
      phonecall: phonecall ?? this.phonecall,
      amount: amount ?? this.amount,
      check: check ?? this.check,
    );
  }

  //tosting
  // @override
  // String toString() {
  //   return "{ avatar: $avatar, title: $title ,detail: $detail}";
  // }

  List<DataTestModel> testData() {
    return [
      DataTestModel(
          title: "Nguyễn Thị Hồng Hạnh",
          check: true,
          avatar: "https://notintop100.com/wp-content/uploads/2018/03/girl-xinh-4k.jpg",
          detail: "Gọi ứng viên INACTIVE, Đăng ký mới...",
          notification: "Thuan Binh đã đăng ký công việc của bạn.",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Đạt Đăng",
          check: false,
          avatar:
              "https://kenh14cdn.com/2018/5/18/290882137325318102755197476772419990978560n-15266545132221570661072.jpg",
          detail: "Đăng ký mới từ Landing Page AllAO,...",
          notification: "Công việc \"gọi hỏi phụ huynh\" của bạn đã được hệ thống xét duyệt thành công",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Quang Mai Duy",
          check: false,
          avatar:
              "http://images6.fanpop.com/image/photos/40900000/thiet-bi-xu-ly-nuoc-thai-westerntechvn-40941432-600-785.jpg",
          detail: "Gọi ứng viên Paolo, Gọi chăm sóc...",
          notification: "Trần Chúi Nhũi đã hủy công việc \"gọi ứng tuyển\" của bạn",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Ngọc Hải",
          check: true,
          avatar:
              "https://kenh14cdn.com/2018/5/18/2187975116277389405904394300345471227920384n-1526654871547348867622.jpg",
          detail: "Gọi Hướng dẫn ứng viên, Đăng ký ...",
          notification: "Công việc \"làm pet\" của bạn đã bị từ chối xét duyệt",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Trần Thị Thu",
          check: false,
          avatar: "https://anh.24h.com.vn/upload/2-2015/images/2015-05-28/1432809954-nmnbngan_clnd.jpg",
          detail: "Gọi ứng viên INACTIVE, Đăng ký mới...",
          notification: "Thuan Binh đã đăng ký công việc của bạn.",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Nguyễn Thị Hồng Hạnh",
          check: true,
          avatar: "https://notintop100.com/wp-content/uploads/2018/03/girl-xinh-4k.jpg",
          detail: "Gọi ứng viên INACTIVE, Đăng ký mới...",
          notification: "Thuan Binh đã đăng ký công việc của bạn.",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Đạt Đăng",
          check: false,
          avatar:
              "https://kenh14cdn.com/2018/5/18/290882137325318102755197476772419990978560n-15266545132221570661072.jpg",
          detail: "Đăng ký mới từ Landing Page AllAO,...",
          notification: "Công việc \"gọi hỏi phụ huynh\" của bạn đã được hệ thống xét duyệt thành công",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Quang Mai Duy",
          check: false,
          avatar:
              "http://images6.fanpop.com/image/photos/40900000/thiet-bi-xu-ly-nuoc-thai-westerntechvn-40941432-600-785.jpg",
          detail: "Gọi ứng viên Paolo, Gọi chăm sóc...",
          notification: "Trần Chúi Nhũi đã hủy công việc \"gọi ứng tuyển\" của bạn",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Ngọc Hải",
          check: true,
          avatar:
              "https://kenh14cdn.com/2018/5/18/2187975116277389405904394300345471227920384n-1526654871547348867622.jpg",
          detail: "Gọi Hướng dẫn ứng viên, Đăng ký ...",
          notification: "Công việc \"làm pet\" của bạn đã bị từ chối xét duyệt",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Trần Thị Thu",
          check: false,
          avatar: "https://anh.24h.com.vn/upload/2-2015/images/2015-05-28/1432809954-nmnbngan_clnd.jpg",
          detail: "Gọi ứng viên INACTIVE, Đăng ký mới...",
          notification: "Thuan Binh đã đăng ký công việc của bạn.",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Nguyễn Thị Hồng Hạnh",
          check: true,
          avatar: "https://notintop100.com/wp-content/uploads/2018/03/girl-xinh-4k.jpg",
          detail: "Gọi ứng viên INACTIVE, Đăng ký mới...",
          notification: "Thuan Binh đã đăng ký công việc của bạn.",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Đạt Đăng",
          check: false,
          avatar:
              "https://kenh14cdn.com/2018/5/18/290882137325318102755197476772419990978560n-15266545132221570661072.jpg",
          detail: "Đăng ký mới từ Landing Page AllAO,...",
          notification: "Công việc \"g��i hỏi phụ huynh\" của bạn đã được hệ thống xét duyệt thành công",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Quang Mai Duy",
          check: false,
          avatar:
              "http://images6.fanpop.com/image/photos/40900000/thiet-bi-xu-ly-nuoc-thai-westerntechvn-40941432-600-785.jpg",
          detail: "Gọi ứng viên Paolo, Gọi chăm sóc...",
          notification: "Trần Chúi Nhũi đã hủy công việc \"gọi ứng tuyển\" của bạn",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Ngọc Hải",
          check: true,
          avatar:
              "https://kenh14cdn.com/2018/5/18/2187975116277389405904394300345471227920384n-1526654871547348867622.jpg",
          detail: "Gọi Hướng dẫn ứng viên, Đăng ký ...",
          notification: "Công việc \"làm pet\" của bạn đã bị từ chối xét duyệt",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Trần Thị Thu",
          check: false,
          avatar: "https://anh.24h.com.vn/upload/2-2015/images/2015-05-28/1432809954-nmnbngan_clnd.jpg",
          detail: "Gọi ứng viên INACTIVE, Đăng ký mới...",
          notification: "Thuan Binh đã đăng ký công việc của bạn.",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Nguyễn Thị Hồng Hạnh",
          check: true,
          avatar: "https://notintop100.com/wp-content/uploads/2018/03/girl-xinh-4k.jpg",
          detail: "Gọi ứng viên INACTIVE, Đăng ký mới...",
          notification: "Thuan Binh đã đăng ký công việc của bạn.",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Đạt Đăng",
          check: false,
          avatar:
              "https://kenh14cdn.com/2018/5/18/290882137325318102755197476772419990978560n-15266545132221570661072.jpg",
          detail: "Đăng ký mới từ Landing Page AllAO,...",
          notification: "Công việc \"gọi hỏi phụ huynh\" của bạn đã được hệ thống xét duyệt thành công",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Quang Mai Duy",
          check: false,
          avatar:
              "http://images6.fanpop.com/image/photos/40900000/thiet-bi-xu-ly-nuoc-thai-westerntechvn-40941432-600-785.jpg",
          detail: "Gọi ứng viên Paolo, Gọi chăm sóc...",
          notification: "Trần Chúi Nhũi đã hủy công việc \"gọi ứng tuyển\" của bạn",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Ngọc Hải",
          check: true,
          avatar:
              "https://kenh14cdn.com/2018/5/18/2187975116277389405904394300345471227920384n-1526654871547348867622.jpg",
          detail: "Gọi Hướng dẫn ứng viên, Đăng ký ...",
          notification: "Công việc \"làm pet\" của bạn đã bị từ chối xét duyệt",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Trần Thị Thu",
          check: false,
          avatar: "https://anh.24h.com.vn/upload/2-2015/images/2015-05-28/1432809954-nmnbngan_clnd.jpg",
          detail: "Gọi ứng viên INACTIVE, Đăng ký mới...",
          notification: "Thuan Binh đã đăng ký công việc của bạn.",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
      DataTestModel(
          title: "Nguyễn Thị Hồng Hạnh",
          check: true,
          avatar: "https://notintop100.com/wp-content/uploads/2018/03/girl-xinh-4k.jpg",
          detail: "Gọi ứng viên INACTIVE, Đăng ký mới...",
          notification: "Thuan Binh đã đăng ký công việc của bạn.",
          amount: Random().nextInt(100),
          phonecall: Random().nextInt(999),
          date: DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.now().add(Duration(days: Random().nextInt(100))))),
    ];
  }
}
// https://anh.24h.com.vn/upload/2-2015/images/2015-05-28/1432809954-nmnbngan_clnd.jpg
// https://kenh14cdn.com/2018/5/18/2187975116277389405904394300345471227920384n-1526654871547348867622.jpg
// http://images6.fanpop.com/image/photos/40900000/thiet-bi-xu-ly-nuoc-thai-westerntechvn-40941432-600-785.jpg
