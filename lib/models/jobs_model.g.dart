// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'jobs_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JobsModel _$JobsModelFromJson(Map<String, dynamic> json) {
  return JobsModel(
    id: json['id'] as String,
    state: json['state'] as String,
    status: json['status'] as bool,
    price: json['price'] as String,
    user_id: json['user_id'] as String,
    title: json['title'] as String,
    requirement: json['requirement'] as String,
    list_clients: (json['list_clients'] as List)
        ?.map((e) =>
            e == null ? null : ClientModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    worker_amount: json['worker_amount'] as int,
    total_call_amount: json['total_call_amount'] as int,
    done_call_amount: json['done_call_amount'] as int,
    instruction: json['instruction'] as String,
    interacting_type: json['interacting_type'] as String,
    interacting_content_id: json['interacting_content_id'] as String,
    action: json['action'] as String,
    created_at_unix_timestamp: json['created_at_unix_timestamp'] as String,
    updated_at_unix_timestamp: json['updated_at_unix_timestamp'] as String,
    user: json['user'] == null
        ? null
        : UserModel.fromJson(json['user'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$JobsModelToJson(JobsModel instance) => <String, dynamic>{
      'id': instance.id,
      'state': instance.state,
      'status': instance.status,
      'price': instance.price,
      'user_id': instance.user_id,
      'title': instance.title,
      'requirement': instance.requirement,
      'list_clients': instance.list_clients,
      'worker_amount': instance.worker_amount,
      'total_call_amount': instance.total_call_amount,
      'done_call_amount': instance.done_call_amount,
      'instruction': instance.instruction,
      'interacting_type': instance.interacting_type,
      'interacting_content_id': instance.interacting_content_id,
      'action': instance.action,
      'user': instance.user,
      'created_at_unix_timestamp': instance.created_at_unix_timestamp,
      'updated_at_unix_timestamp': instance.updated_at_unix_timestamp,
    };
