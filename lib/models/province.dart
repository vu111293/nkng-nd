import 'dart:convert';

Map<String, Province> provinceFromJson(String str) =>
    Map.from(json.decode(str)).map((k, v) => MapEntry<String, Province>(k, Province.fromJson(v)));

String provinceToJson(Map<String, Province> data) =>
    json.encode(Map.from(data).map((k, v) => MapEntry<String, dynamic>(k, v.toJson())));

class Province {
  String name;
  String slug;
  String type;
  String nameWithType;
  String code;

  Province({
    this.name,
    this.slug,
    this.type,
    this.nameWithType,
    this.code,
  });

  factory Province.fromJson(Map<String, dynamic> json) => Province(
        name: json["name"],
        slug: json["slug"],
        type: json["type"],
        nameWithType: json["name_with_type"],
        code: json["code"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "slug": slug,
        "type": type,
        "name_with_type": nameWithType,
        "code": code,
      };
}
