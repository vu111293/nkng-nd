// To parse this JSON data, do
//
//     final banner = bannerFromJson(jsonString);

import 'dart:convert';

List<Settings> settingsFromJson(String str) => List<Settings>.from(json.decode(str).map((x) => Settings.fromJson(x)));

String settingsToJson(List<Settings> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Settings {
  String id;
  String key;
  String value;
  String createdAtUnixTimestamp;
  String updatedAtUnixTimestamp;
  bool status;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic deletedAt;

  Settings({
    this.id,
    this.key,
    this.value,
    this.createdAtUnixTimestamp,
    this.updatedAtUnixTimestamp,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
  });

  factory Settings.fromJson(Map<String, dynamic> json) => Settings(
        id: json["id"],
        key: json["key"],
        value: json["value"],
        createdAtUnixTimestamp: json["created_at_unix_timestamp"],
        updatedAtUnixTimestamp: json["updated_at_unix_timestamp"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "key": key,
        "value": value,
        "created_at_unix_timestamp": createdAtUnixTimestamp,
        "updated_at_unix_timestamp": updatedAtUnixTimestamp,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "deleted_at": deletedAt,
      };
}
