import 'package:loctroi/utils/app_store.dart';

///HeaderStyle
TextStyle ptHeadStyleText(BuildContext context) => Theme.of(context).textTheme.title.copyWith(fontSize: 20);

///TextTheme
TextStyle ptDisplay4(BuildContext context) => Theme.of(context).textTheme.display4;
TextStyle ptDisplay3(BuildContext context) => Theme.of(context).textTheme.display3;
TextStyle ptDisplay2(BuildContext context) => Theme.of(context).textTheme.display2;
TextStyle ptDisplay1(BuildContext context) => Theme.of(context).textTheme.display1;
TextStyle ptHeadline(BuildContext context) => Theme.of(context).textTheme.headline;
TextStyle ptTitle(BuildContext context) => Theme.of(context).textTheme.title;
TextStyle ptSubhead(BuildContext context) => Theme.of(context).textTheme.subhead;
TextStyle ptBody2(BuildContext context) => Theme.of(context).textTheme.body2;
TextStyle ptBody1(BuildContext context) => Theme.of(context).textTheme.body1;
TextStyle ptCaption(BuildContext context) => Theme.of(context).textTheme.caption;
TextStyle ptButton(BuildContext context) => Theme.of(context).textTheme.button;
TextStyle ptSubtitle(BuildContext context) => Theme.of(context).textTheme.subtitle;
TextStyle ptOverline(BuildContext context) => Theme.of(context).textTheme.overline;
