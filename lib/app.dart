import 'package:loctroi/utils/app_store.dart';

import 'models/response_models.dart';

class MyApp extends StatefulWidget {
  final AppConfig appConf;

  MyApp({Key key, this.appConf}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();

  void setTheme(BuildContext context, Color newColor) {
    _MyAppState state = context.ancestorStateOfType(TypeMatcher<_MyAppState>());
    state.setState(() {
      state._primaryColor = newColor;
    });
  }
}

class _MyAppState extends State<MyApp> {
  ApplicationBloc _appBloc;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  var _primaryColor = HexColor(appColor); // This will hold the value of the app main color
  double fontSize = 13;

  @override
  void initState() {
    getValuesSF();
    _appBloc = ApplicationBloc();
    PaintingBinding.instance.imageCache.maximumSizeBytes = 10485760 * 20; // 2
    super.initState();
    initFCM();
  }

  @override
  void dispose() {
    _appBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ApplicationBloc>(
      bloc: _appBloc,
      child: BotToastInit(
        child: LayoutBuilder(
          builder: (context, constraints) {
            return OrientationBuilder(
              builder: (context, orientation) {
                SizeConfig().init(constraints, orientation);
                return MaterialApp(
                  debugShowCheckedModeBanner: false,
                  navigatorKey: navKey,
                  navigatorObservers: [BotToastNavigatorObserver()],
                  home: SplashScreen(),
                  localizationsDelegates: [
                    GlobalMaterialLocalizations.delegate,
                    GlobalWidgetsLocalizations.delegate,
                    GlobalCupertinoLocalizations.delegate
                  ],
                  supportedLocales: [
                    const Locale('en'), // English
                    const Locale('vi'), // VietNam
                  ],
                  title: widget.appConf.appName,
                  theme: _theme(),
                  builder: (BuildContext context, Widget child) {
                    return MediaQuery(
                      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                      child: child,
                    );
                  },
                );
              },
            );
          },
        ),
      ),
    );
  }

  ThemeData _theme() {
    return Theme.of(context).copyWith(
      primaryIconTheme: IconThemeData(color: HexColor(appText)),
      iconTheme: IconThemeData(color: HexColor(appText)),
      // Define the default Brightness and Colors
      brightness: Brightness.light,
      primaryColor: _primaryColor,
      accentColor: HexColor(appWhite),
      inputDecorationTheme: InputDecorationTheme(
        labelStyle: Theme.of(context).textTheme.subtitle,
        fillColor: _primaryColor,
        focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: _primaryColor)),
      ), // Define the default buttonTheme. Use this to specify the default
      buttonTheme: ButtonThemeData(
        buttonColor: _primaryColor,
        disabledColor: Colors.black26,
        textTheme: ButtonTextTheme.primary,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
        padding: EdgeInsets.all(5),
      ),
      textTheme: TextTheme(
        headline: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: fontSize * 1.6,
          color: HexColor(appText),
        ),
        subhead: TextStyle(
          fontSize: fontSize * 1.4,
          color: HexColor(appText),
        ),
        title: TextStyle(
          fontSize: fontSize * 1.3,
          color: HexColor(appColor),
        ),
        subtitle: TextStyle(
          fontSize: fontSize * 1.1,
          color: HexColor(appText60),
        ),
        body1: TextStyle(
          fontSize: fontSize,
          color: HexColor(appText),
        ),
        body2: TextStyle(
          fontSize: fontSize,
          color: HexColor(appText60),
        ),
        caption: TextStyle(
          fontSize: fontSize * 0.9,
          color: HexColor(appText60),
        ),
        button: TextStyle(
          fontSize: fontSize,
          color: HexColor(appText),
        ),
        overline: TextStyle(
          fontSize: fontSize * 0.8,
          color: HexColor(appText),
        ),
        display4: TextStyle(
          fontSize: fontSize * 1.6,
          color: HexColor(appText),
        ),
        display3: TextStyle(
          fontSize: fontSize * 1.6,
          color: HexColor(appText),
        ),
        display2: TextStyle(
          fontSize: fontSize * 1.6,
          color: HexColor(appText),
        ),
        display1: TextStyle(
          fontSize: fontSize * 1.6,
          color: HexColor(appText),
        ),
      ),
    );
  }

  // Logic methods

  void initFCM() async {
    await _firebaseMessaging.subscribeToTopic("news");

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("bambi FCM onMessage: $message");
        _handleMessageOnLaunch(message, AppStartMode.LIVE);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("phat onLaunch: $message");
        _handleMessageOnLaunch(message, AppStartMode.LAUNCH);
      },
      onResume: (Map<String, dynamic> message) async {
        print("bambi FCM onResume: $message");
        _handleMessageOnLaunch(message, AppStartMode.RESUME);
      },
    );

    _firebaseMessaging
        .requestNotificationPermissions(const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings settings) {
      print("bambi FCM Settings registered: $settings");
    });
  }

  _handleMessageOnLaunch(Map<String, dynamic> message, AppStartMode startMode) {
    String msg = getMessageFromNotify(message);
    if (msg != null) {
      var cancelToastFuc;
      cancelToastFuc = BotToast.showSimpleNotification(
          title: 'Thông báo',
          subTitle: msg,
          duration: Duration(seconds: 10),
          onTap: () async {
            Future.delayed(Duration(milliseconds: 500), () {
              Navigator.popUntil(Routing().latestContext, ModalRoute.withName('/homepage'));
              Routing().navigate2(Routing().latestContext, NotificationScreen());
            });
            cancelToastFuc();
          });
    }

//    String type = Platform.isAndroid ? message['data']['type'] : message['type'];
//    print('type $type ');
//    String dataId = Platform.isAndroid ? message['data']['id'] : message['id'];
//    String id = dataId != null && dataId != '' ? dataId : null;
//    print('dataId $id ');
//
//    if (type != null) {
//      switch (type.toLowerCase()) {
//        case 'post':
//          if (startMode == AppStartMode.LAUNCH) {
//            _appBloc.addNotifyActionWhenOpen(NotifyAction(id: id, type: NotifyType.POST));
//          } else if (startMode == AppStartMode.RESUME) {
//            if (id != null) {
//              Future.delayed(Duration(milliseconds: 500), () {
//                Routing().navigate2(Routing().latestContext, DocumentDetailScreen(postId: id));
//              });
//            }
//          } else if (startMode == AppStartMode.LIVE) {
//            String msg = getMessageFromNotify(message);
//            if (msg != null) {
//              var cancelToastFuc;
//              cancelToastFuc = BotToast.showSimpleNotification(
//                  title: 'Thông báo',
//                  subTitle: msg,
//                  duration: Duration(seconds: 5),
//                  onTap: () async {
//                    Future.delayed(Duration(milliseconds: 500), () {
//                      Routing().navigate2(Routing().latestContext, DocumentDetailScreen(postId: id));
//                    });
//                    cancelToastFuc();
//                  });
//            }
//          }
//          break;
//        case 'issue':
//          if (startMode == AppStartMode.LAUNCH) {
//            _appBloc.addNotifyActionWhenOpen(NotifyAction(id: id, type: NotifyType.ISSUE));
//          } else if (startMode == AppStartMode.RESUME) {
//            _openQuestion(id);
//
//            // Refresh question data
//            _appBloc.loadUserData();
//            _appBloc.pushNotiUpdateQuestionAction("");
//          } else if (startMode == AppStartMode.LIVE) {
//            String msg = getMessageFromNotify(message);
//            if (msg != null) {
//              var cancelToastFuc;
//              cancelToastFuc = BotToast.showSimpleNotification(
//                  title: 'Thông báo',
//                  subTitle: msg,
//                  duration: Duration(seconds: 5),
//                  onTap: () async {
//                    _openQuestion(id);
//                    cancelToastFuc();
//                  });
//            }
//
//            // Refresh question data
//            _appBloc.loadUserData();
//            _appBloc.pushNotiUpdateQuestionAction(""); // Todo please insert noti question here
//          }
//          break;
//        case 'news':
//          if (startMode == AppStartMode.LAUNCH) {
//            _appBloc.addNotifyActionWhenOpen(NotifyAction(type: NotifyType.NEWS));
//          } else if (startMode == AppStartMode.RESUME) {
//            Routing().navigate2(Routing().latestContext, NotificationScreen());
//          } else if (startMode == AppStartMode.LIVE) {
//            String msg = getMessageFromNotify(message);
//            if (msg != null) {
//              var cancelToastFuc;
//              cancelToastFuc = BotToast.showSimpleNotification(
//                  title: 'Thông báo',
//                  subTitle: msg,
//                  duration: Duration(seconds: 5),
//                  onTap: () async {
//                    Routing().navigate2(Routing().latestContext, NotificationScreen());
//                    cancelToastFuc();
//                  });
//            }
//          }
//          break;
//        default:
//          break;
//      }
//    }
  }

  String getMessageFromNotify(Map<String, dynamic> message) {
    String title = Platform.isAndroid ? message['notification']['title'] : message['aps']['alert']['title'];
    String content = Platform.isAndroid ? message['notification']['body'] : message['aps']['alert']['body'];
    if (title != null && title.isNotEmpty && content != null && content.isNotEmpty) {
      return '$title: $content';
    }
    if (title != null && title.isNotEmpty) {
      return title;
    }
    if (content != null && content.isNotEmpty) {
      return content;
    }
    return null;
  }

  String getQuestionIdFromMessage(Map<String, dynamic> message) {
    return Platform.isAndroid ? message['data']['id'] : message['id'];
  }

  Future _openQuestion(String id) async {
    LQuestionResponse q = _appBloc.allQuestionList.firstWhere((item) => item.id == id, orElse: () => null);
    if (q != null) {
//      await Future.delayed(Duration(milliseconds: 500), () {
//        Routing().navigate2(Routing().latestContext, QuestionDetailScreen(question: q));
//      });
    } else {
      if (mounted) {
        Toast.show('Câu hỏi đã bị thay đổi hoặc không tồn tại', Routing().latestContext);
      }
    }
    return Future;
  }

  getValuesSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return String
    if (prefs.containsKey('primariColor')) {
      String primariColor = prefs.getString('primariColor');
      String valueString = primariColor.split('(0x')[1].split(')')[0];
      int value = int.parse(valueString, radix: 16);
      Color otherColor = new Color(value);
      setState(() {
        _primaryColor = otherColor;
      });
    }
  }
}

enum AppStartMode { RESUME, LAUNCH, LIVE }

enum AppType { DOCTOR, FARMER }

class AppConfig {
  final String appName;
  final AppType appType;
  String pathURL;
  String versionURL;
  String webviewURL;
  String browserURL;
  String spinToken;
  String spinProductCampaignId;

  AppConfig(
      {this.appName,
      this.appType,
      this.pathURL,
      this.versionURL,
      this.webviewURL,
      this.browserURL,
      this.spinToken,
      this.spinProductCampaignId});
}
