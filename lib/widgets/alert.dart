import 'package:loctroi/utils/app_store.dart';

class MyAlert extends StatelessWidget {
  final String title;
  final String content;
  final Function onAgree;
  final String nameButton;
  const MyAlert({Key key, this.title, this.content, this.onAgree, this.nameButton}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        title,
        style: Theme.of(context).textTheme.title,
      ),
      content: Text(
        content,
        style: Theme.of(context).textTheme.subtitle,
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.pop(context);
            onAgree != null ? onAgree() : () {};
          },
          child: Text(nameButton != null ? nameButton.isNotEmpty ? nameButton : Strings.agree : Strings.agree),
        )
      ],
    );
  }
}
