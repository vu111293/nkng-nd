import 'package:loctroi/utils/app_store.dart';

class ZoomImageAsset extends StatelessWidget {
  const ZoomImageAsset({
    this.imageProvider,
    this.loadingChild,
    this.backgroundDecoration,
    this.minScale,
    this.maxScale,
  });

  final ImageProvider imageProvider;
  final Widget loadingChild;
  final Decoration backgroundDecoration;
  final dynamic minScale;
  final dynamic maxScale;

  @override
  Widget build(BuildContext context) {
    return Stack(fit: StackFit.expand, children: <Widget>[
      Container(
        constraints: BoxConstraints.expand(
          height: MediaQuery.of(context).size.height,
        ),
//        child: PhotoView(
//          imageProvider: imageProvider,
//          loadingChild: loadingChild,
//          backgroundDecoration: backgroundDecoration,
//          minScale: minScale,
//          maxScale: maxScale,
//        ),
      ),
      Positioned(
        top: 30,
        left: 15,
        child: Container(
          decoration:
              BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(32.0)), color: ptPrimaryColor(context)),
          child: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.close,
              color: Colors.white,
              size: 32,
            ),
          ),
        ),
      )
    ]);
  }
}
