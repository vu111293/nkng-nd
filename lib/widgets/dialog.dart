import 'package:loctroi/utils/app_store.dart';

class CustomDialog extends StatelessWidget {
  final Widget image;
  final Text title;
  final Text description;
  final bool onlyOkButton;
  final Text buttonOkText;
  final Text buttonCancelText;
  final Color buttonOkColor;
  final Color buttonCancelColor;
  final double buttonRadius;
  final double cornerRadius;
  final bool hideButton;
  final VoidCallback onOkButtonPressed;

  CustomDialog({
    Key key,
    this.image,
    @required this.title,
    @required this.onOkButtonPressed,
    this.description,
    this.onlyOkButton = false,
    this.buttonOkText,
    this.buttonCancelText,
    this.buttonOkColor,
    this.buttonCancelColor,
    this.hideButton = true,
    this.cornerRadius = 8.0,
    this.buttonRadius = 8.0,
  })  : assert(title != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SimpleDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(cornerRadius)),
          contentPadding: EdgeInsets.all(8),
          titlePadding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
          title: Container(
            // height: 30,
            child: Stack(
              alignment: AlignmentDirectional.topCenter,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      child: Image.asset(
                        'assets/images/logo_do_me.png',
                        fit: BoxFit.cover,
                        height: 30,
                      ),
                      margin: EdgeInsets.only(top: 10),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: title != null
                          ? Center(
                              child: title,
                            )
                          : null,
                    ),
                  ],
                ),
                Positioned(
                    right: 5,
                    child: ClipOval(
                      child: Material(
                        color: Colors.white70, // button color
                        child: InkWell(
                          child: SizedBox(width: 30, height: 30, child: Icon(Icons.close)),
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    )),
              ],
            ),
          ),
          children: <Widget>[
            Stack(
              children: <Widget>[
                !hideButton
                    ? Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Row(
                          mainAxisAlignment: !onlyOkButton ? MainAxisAlignment.spaceEvenly : MainAxisAlignment.center,
                          children: <Widget>[
                            RaisedButton(
                              color: buttonOkColor ?? Colors.green,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(buttonRadius)),
                              onPressed: onOkButtonPressed ?? () {},
                              child: buttonOkText ??
                                  Text(
                                    'OK',
                                    style: ptButton(context),
                                  ),
                            ),
                            !onlyOkButton
                                ? RaisedButton(
                                    color: buttonCancelColor ?? Colors.grey,
                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(buttonRadius)),
                                    onPressed: () => Navigator.of(context).pop(),
                                    child: buttonCancelText ??
                                        Text(
                                          'Cancel',
                                          style: ptButton(context),
                                        ),
                                  )
                                : Container(),
                          ],
                        ),
                      )
                    : Container(),
                Column(
                  children: <Widget>[
                    // image != null
                    //     ? Container(
                    //         // width: MediaQuery.of(context).size.width * 0.8,
                    //         // height: hideButton
                    //         //     ? (MediaQuery.of(context).size.height / 2) * 0.6
                    //         //     : (MediaQuery.of(context).size.height / 2) * 0.45,
                    //         child: Card(
                    //           elevation: 0.0,
                    //           margin: EdgeInsets.all(0.0),
                    //           shape: RoundedRectangleBorder(
                    //               borderRadius: BorderRadius.only(
                    //                   topRight: Radius.circular(cornerRadius), topLeft: Radius.circular(cornerRadius))),
                    //           clipBehavior: Clip.antiAlias,
                    //           child: image,
                    //         ),
                    //       )
                    //     : Container(
                    //         height: 20,
                    //       ),
                    SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: description,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            )
          ],
        ),
      ],
    );
  }
}
