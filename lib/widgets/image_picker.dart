import 'package:loctroi/utils/app_store.dart';

class ImagePickerWidget extends StatefulWidget {
  final double size;
  final int quality;
  final String resourceUrl;
  final String positionUser;
  final BuildContext context;
  final bool circle;
  final bool isEdit;
  final bool isRemove;
  final bool avatar;
  final Function onClick;
  final List listImage;

  ImagePickerWidget({
    Key key,
    this.context,
    this.size = 50,
    this.quality = 50,
    this.resourceUrl,
    this.positionUser,
    this.circle = false,
    this.isEdit = false,
    this.isRemove = false,
    this.avatar = false,
    this.onFileChanged,
    this.listImage,
    this.onClick,
  }) : super(
          key: key,
        );
  final Function(String, String) onFileChanged;

  @override
  _ImagePickerWidgetState createState() => _ImagePickerWidgetState();
}

class _ImagePickerWidgetState extends State<ImagePickerWidget> {
  File image;

  popupAlert() {
    FocusScope.of(context).requestFocus(FocusNode());
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.photo_camera),
                title: Text(
                  'Máy ảnh',
                  style: ptTitle(context),
                ),
                onTap: imageSelectorCamera,
              ),
              ListTile(
                leading: Icon(Icons.photo_library),
                title: Text(
                  'Thư viện ảnh',
                  style: ptTitle(context),
                ),
                onTap: imageSelectorGallery,
              )
            ],
          );
        });
  }

  openZoomListImage(List url) {
    int indexImage =
        widget.listImage != null && widget.listImage.length > 1 ? widget.listImage.indexOf(url[0]) : 0; // 1
    Routing().navigate2(context, PicSwiper(indexImage, widget.listImage ?? url));
  }

  Widget uploadExtraInfo() {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(widget.circle ? widget.size / 2 : 5.0)),
      child: GestureDetector(
        onTap: () {
          widget.resourceUrl != null && widget.resourceUrl != '' && widget.resourceUrl.contains("http")
              ? openZoomListImage([widget.resourceUrl])
              : {};
        },
        child: widget.resourceUrl != null && widget.resourceUrl != '' && widget.resourceUrl.contains("http")
            ? CachedNetworkImage(
                fit: BoxFit.cover,
                width: widget.size,
                // height: widget.size,
                imageUrl:
                    "https://aelaaawjoo.cloudimg.io/crop/${widget.quality}x${widget.quality}/x/${widget.resourceUrl}",
                placeholder: (context, url) => Center(
                  child: Container(
                    width: widget.size / 2,
                    height: widget.size / 2,
                    child: CircularProgressIndicator(
                        backgroundColor: ptPrimaryColor(context).withOpacity(0.1),
                        valueColor: AlwaysStoppedAnimation<Color>(ptPrimaryColor(context).withOpacity(0.3))),
                  ),
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              )
            : image == null
                ? Image.asset(
                    widget.positionUser == 'doctor'
                        ? "assets/images/avatar_doctor.jpg"
                        : widget.positionUser == 'client'
                            ? "assets/images/avatar_client.jpg"
                            : "assets/images/default_avatar.png",
                    fit: BoxFit.cover,
                    width: widget.size,
                    height: widget.size,
                  )
                // Center(
                //     child: Container(
                //       width: widget.size / 2,
                //       height: widget.size / 2,
                //       child: CircularProgressIndicator(
                //           backgroundColor: ptPrimaryColor(context).withOpacity(0.1),
                //           valueColor: AlwaysStoppedAnimation<Color>(ptPrimaryColor(context).withOpacity(0.3))),
                //     ),
                //   )
                : Image.file(
                    image,
                    fit: BoxFit.cover,
                    width: widget.size,
                    height: widget.size,
                  ),
      ),
    );
  }

  _handleUploadImage(localImage) async {
    var image0 = image;
    showWaitingDialog(context, message: "Đang tải ảnh lên...");
    String link = await ImageService().uploadImageToImgur(localImage).then((onValue) {
      if (widget.onFileChanged != null) {
        widget.onFileChanged(onValue, 'image');
      }
      if (localImage != null) {
        print("You selected gallery image : " + localImage.path);
        setState(() {
          image = localImage;
        });
      }
      Navigator.pop(context);
    }).catchError((onError) {
      Navigator.pop(context);
      setState(() {
        image = image0;
      });
      showAlertDialog(context, "Cập nhật ảnh thất bại");
    });
    // print('phat update link ảnh nè: $link');
  }

  //display image selected from camera
  Future imageSelectorCamera() async {
    Navigator.pop(context);
    var cameraFile = await ImagePicker.pickImage(source: ImageSource.camera, maxWidth: 500.0, maxHeight: 500.0);
    _handleUploadImage(cameraFile);
  }

  //display image selected from gallery
  Future imageSelectorGallery() async {
    Navigator.pop(context);
    var galleryFile =
        await ImagePicker.pickImage(source: ImageSource.gallery, maxWidth: 800, maxHeight: 1000, imageQuality: 90);
    _handleUploadImage(galleryFile);
  }

  @override
  Widget build(BuildContext context) {
    if (widget.avatar) {
      return ClipOval(
        child: GestureDetector(
          onTap: () {
            widget.resourceUrl != null && widget.resourceUrl != '' && widget.resourceUrl.contains("http")
                ? openZoomListImage([widget.resourceUrl])
                : {};
          },
          child: widget.resourceUrl != null && widget.resourceUrl.contains("http")
              ? CachedNetworkImage(
                  fit: BoxFit.cover,
                  width: widget.size,
                  height: widget.size,
                  imageUrl:
                      "https://aelaaawjoo.cloudimg.io/crop/${widget.quality}x${widget.quality}/x/${widget.resourceUrl}",
                  placeholder: (context, url) => Center(
                    child: Container(
                      width: widget.size / 2,
                      height: widget.size / 2,
                      child: CircularProgressIndicator(
                          backgroundColor: ptPrimaryColor(context).withOpacity(0.1),
                          valueColor: AlwaysStoppedAnimation<Color>(ptPrimaryColor(context).withOpacity(0.3))),
                    ),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                )
              : image == null
                  ? Image.asset(
                      widget.positionUser == 'doctor'
                          ? "assets/images/avatar_doctor.jpg"
                          : widget.positionUser == 'client'
                              ? "assets/images/avatar_client.jpg"
                              : "assets/images/default_avatar.png",
                      fit: BoxFit.cover,
                      width: widget.size,
                      height: widget.size,
                    )
                  : Image.file(
                      image,
                      fit: BoxFit.cover,
                      width: widget.size,
                      height: widget.size,
                    ),
        ),
      );
    } else {
      return Container(
        width: widget.size,
        height: widget.size,
        decoration: BoxDecoration(
          color: ptPrimaryColor(context).withOpacity(0.5),
          borderRadius: BorderRadius.all(
            Radius.circular(widget.circle ? widget.size : 5.0),
          ),
          border: Border.all(color: HexColor('#fafafa')),
        ),
        child: Stack(
          alignment: Alignment.bottomCenter,
          overflow: Overflow.clip,
          fit: StackFit.expand,
          children: <Widget>[
            uploadExtraInfo(),
            widget.isEdit
                ? GestureDetector(
                    onTap: popupAlert,
                    child: Container(
                      width: widget.size,
                      height: widget.size,
                      decoration: BoxDecoration(
                        color: Colors.black26,
                        borderRadius: BorderRadius.all(
                          Radius.circular(widget.circle ? widget.size : 5.0),
                        ),
                        border: Border.all(color: HexColor('#fafafa')),
                      ),
                      child: Center(
                        child: Icon(
                          Icons.camera_alt,
                          size: widget.size * 0.3,
                          color: Color(0xFFE5E5E5),
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
            widget.isRemove
                ? Container(
                    width: widget.size,
                    height: widget.size,
                    decoration: BoxDecoration(
                      color: Colors.white30,
                      borderRadius: BorderRadius.all(
                        Radius.circular(widget.circle ? widget.size : 5.0),
                      ),
                      border: Border.all(color: HexColor('#fafafa')),
                    ),
                    child: Center(
                      child: Icon(
                        Icons.remove_circle,
                        size: widget.size * 0.3,
                        color: Colors.redAccent,
                      ),
                    ),
                  )
                : SizedBox()
          ],
        ),
      );
    }
  }
}
