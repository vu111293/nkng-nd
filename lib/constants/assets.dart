const rootPath = 'assets/';
const pngFileType = '.png';
const jpgFileType = '.jpg';

class Assets {
  static const bg = '${rootPath}bg$jpgFileType';
  static const bgNew = '${rootPath}bgNew$pngFileType';
  static const background = '${rootPath}background$pngFileType';
  static const gift = '${rootPath}gift$pngFileType';
  static const letter = '${rootPath}letter$pngFileType';
  static const icon = '${rootPath}icon$pngFileType';
  static const contract = '${rootPath}contract$pngFileType';
  static const logo = '${rootPath}logo$pngFileType';
  static const megaphone = '${rootPath}megaphone$pngFileType';
  static const qrCode = '${rootPath}qrcode$pngFileType';
  static const qrCodeBlack = '${rootPath}qrcode_black$pngFileType';
  static const shoppingBag = '${rootPath}shopping-bag$pngFileType';
  static const dataEmpty = '${rootPath}data_empty$pngFileType';
  static const qrIcon = '${rootPath}qrcode$pngFileType';
}