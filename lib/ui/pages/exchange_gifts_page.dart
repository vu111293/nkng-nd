import 'package:flutter_native_web/flutter_native_web.dart';
import 'package:loctroi/utils/app_store.dart';

import '../../app.dart';

class ExchangeGiftsPage extends StatefulWidget {
  ExchangeGiftsPage({Key key}) : super(key: key);

  @override
  ExchangeGiftsState createState() => ExchangeGiftsState();
}

class ExchangeGiftsState extends State<ExchangeGiftsPage> with WidgetsBindingObserver {
  ApplicationBloc _appBloc;

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Future<Null> didChangeAppLifecycleState(AppLifecycleState state) async {
    if (state == AppLifecycleState.resumed) {
      _reloadAppData();
    }
  }

  @override
  Widget build(BuildContext context) {
    return BaseHeader(
      title: 'Đổi quà',
      body: StreamBuilder<List<Award>>(
        stream: _appBloc.awardListStream,
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Container(alignment: Alignment.center, child: Text('Đang tải'));
          if (snapshot.data == null || snapshot.data.isEmpty) {
            return Container(alignment: Alignment.center, child: Text('Chưa có chương trình đổi quà'));
          }
          return ItemExchangeGifts(snapshot.data, () {
            _reloadAppData();
          });
        },
      ),
    );
  }

  _reloadAppData() async {
    Farmer user = await UserService()?.getProfile(LoopBackAuth()?.userId);
    _appBloc.getAuthBloc().updateUserAction(user);
    await _appBloc.loadUserData();
  }
}

class ItemExchangeGifts extends StatefulWidget {
  final dateTimeFormat = DateFormat('dd-MM-yyyy');
  final List<Award> items;
  final Function onReload;

  ItemExchangeGifts(this.items, this.onReload);

  @override
  ItemExchangeGiftsState createState() => ItemExchangeGiftsState();
}

class ItemExchangeGiftsState extends State<ItemExchangeGifts> {
  final dateTimeFormat = DateFormat('dd-MM-yyyy');

  ApplicationBloc _appBloc;
//  WebController webController;

//  void onWebCreated(webController) {
//    this.webController = webController;
//    this.webController.loadUrl("https://flutter.io/");
//    this.webController.onPageStarted.listen((url) => print("Loading $url"));
//    this.webController.onPageFinished.listen((url) => print("Finished loading $url"));
//  }

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
//    final titles = [
//      '#1 - Cùng nhau hái lộc - Giải KK',
//      '#2 - thẻ tích điểm - Giải ba',
//      '#3 - thẻ tích điểm - Giải KK',
//      '#4 - Cùng nhau hái lộc - Giải nhất',
//      '#5 - thẻ tích điểm - Giải kk',
//      '#6 - thẻ tích điểm - Giải kk',
//    ];
    if (widget.items.isEmpty) {
      return buildListTile(context);
    }
    return ListView.builder(
      shrinkWrap: true,
      itemCount: widget.items.length,
      itemBuilder: (context, index) {
        Award award = widget.items[index];
        return Column(
          children: <Widget>[
            index == 0 ? buildListTile(context) : Container(),
            Container(
                decoration:
                    BoxDecoration(color: ptPlaceholder(context), borderRadius: BorderRadius.all(Radius.circular(5.0))),
                margin: EdgeInsets.only(top: 16),
                padding: EdgeInsets.symmetric(vertical: 9, horizontal: 16),
                child: Row(
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Image.network(
                          award.backgroundImage,
                          width: 120.0,
                          height: 180,
                          fit: BoxFit.cover,
                        ),
                        Positioned(
                          top: 40,
                          bottom: 40,
                          left: 0,
                          right: 0,
                          child: Image.network(
                            award.wheelImage,
                            fit: BoxFit.cover,
                          ),
                        )
                      ],
                    ),
//                    ClipRRect(
//                      borderRadius: BorderRadius.circular(8.0),
//                      child: Image.network(
//                        award.wheelImage,
//                        width: 73,
//                        height: 135,
//                        fit: BoxFit.cover,
//                      ),
//                    ),
                    Expanded(
                      child: Container(
                        child: ListTile(
                          title: Text(
                            award.title ?? '',
                            style: ptTitle(context)
                                .copyWith(fontWeight: FontWeight.bold, fontSize: 16, color: ptPrimaryColor(context)),
                          ),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              GestureDetector(
                                onTap: () {},
                                child: Container(
                                    margin: EdgeInsets.only(top: 16),
                                    constraints: BoxConstraints(maxWidth: 250, maxHeight: 40),
                                    padding: EdgeInsets.symmetric(vertical: 6, horizontal: 6),
                                    decoration: BoxDecoration(
                                        color: HexColor(appColor3),
                                        borderRadius: BorderRadius.all(Radius.circular(5.0))),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Icon(
                                          Icons.autorenew,
                                          size: 18,
                                          color: Colors.white,
                                        ),
                                        Text(
                                          ' ${award?.spinPoint != null && award.spinPoint > 0 ? '-${formatPrice(award.spinPoint)}' : 'Miễn phí'}',
                                          style: ptButton(context).copyWith(color: Colors.white),
                                        ),
                                      ],
                                    )),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(vertical: 8),
                                child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                  SizedBox(height: 5),
                                  RichText(
                                      maxLines: 3,
                                      overflow: TextOverflow.ellipsis,
                                      text: TextSpan(children: [
                                        TextSpan(
                                          text: 'Bắt đầu: ',
                                          style: ptBody1(context)
                                              .copyWith(color: HexColor(appColor), fontWeight: FontWeight.w600),
                                        ),
                                        TextSpan(
                                            text: dateTimeFormat.format(DateTime.parse(award.startDate)),
                                            style: ptBody1(context).copyWith(color: ptPrimaryColor(context)))
                                      ])),
                                  RichText(
                                      maxLines: 3,
                                      overflow: TextOverflow.ellipsis,
                                      text: TextSpan(children: [
                                        TextSpan(
                                          text: 'Kết thúc: ',
                                          style: ptBody1(context)
                                              .copyWith(color: HexColor(appColor), fontWeight: FontWeight.w600),
                                        ),
                                        TextSpan(
                                            text: dateTimeFormat.format(DateTime.parse(award.endDate)),
                                            style: ptBody1(context).copyWith(color: ptPrimaryColor(context)))
                                      ])),
                                ]),
                              ),
                              Text(
                                'Giải thưởng:',
                                style: ptTitle(context).copyWith(
                                    fontWeight: FontWeight.bold, fontSize: 16, color: ptPrimaryColor(context)),
                              ),
                              Text(
                                award.giftListInText,
                                style: ptSubtitle(context),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                )),
            Container(
              color: ptPrimaryColor(context),
              width: deviceWidth(context),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    elevation: 0,
                    onPressed: () {
                      if (Platform.isIOS) {
                        openWebBrowserhURL(_buildSpinURL(award.id));
                      } else {
                        Routing().navigate2(context, SpinWheelPage(_buildSpinURL(award.id))).then((e) async {
                          widget.onReload();
                        });
                      }
                    },
                    color: ptPrimaryColor(context),
                    child: Text('ĐỔI ĐIỂM'),
                  ),
//                  RaisedButton(
//                    elevation: 0,
//                    onPressed: () => openWebBrowserhURL(_buildSpinURL(award.id)),
//                    color: ptPrimaryColor(context),
//                    child: Text('Vòng quay Browser'),
//                  )
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  String _buildSpinURL(String id) {
    String url = GetIt.instance<AppConfig>().webviewURL;
    return '$url?productCampaignId=$id&token=${LoopBackAuth().accessToken}';
  }

  Widget buildListTile(BuildContext context) {
    return StreamBuilder<Farmer>(
      stream: _appBloc.getAuthBloc().userInfoEvent,
      builder: (context, snapshot) {
        if (!snapshot.hasData || snapshot.data == null) return Container();
        Farmer user = snapshot.data;
        return ListTile(
          contentPadding: EdgeInsets.only(top: 16, left: 12, right: 12),
          title: Text(
            user.fullname,
            style: ptTitle(context).copyWith(fontSize: 16, fontWeight: FontWeight.w600, color: ptPrimaryColor(context)),
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(vertical: 4),
                child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                  Text(
                    'Tổng doanh số:  ${formatPrice(user.point)}',
                    style: TextStyle(fontSize: 13),
                  ),
                  Text(
                    'Tổng doanh số đổi quà còn lại:  ${formatPrice(user.exchangePoint)}',
                    style: TextStyle(fontSize: 13),
                  ),
                  
                ]),
              ),
            ],
          ),
          // trailing: Container(
          //   width: 100,
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.end,
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     children: <Widget>[
          //       IconButton(
          //         onPressed: () {},
          //         color: ptPrimaryColor(context),
          //         icon: Icon(Icons.view_list),
          //       ),
          //       IconButton(
          //         onPressed: () {},
          //         color: ptPrimaryColor(context),
          //         icon: Icon(Icons.border_all),
          //       ),
          //     ],
          //   ),
          // ),
        );
      },
    );
  }
}
