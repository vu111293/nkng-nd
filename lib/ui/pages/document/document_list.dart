import 'package:loctroi/utils/app_store.dart';

enum DocumentType { GIONG, KTCANHTAC, BAOVETHUCVAT, KTXULIHOA, DINHDUONG, THUHOACHBAOQUAN, PHACDODIEUTRI, TINTUC }

class DocumentList extends StatefulWidget {
  final String titleHeader;
  final DocumentType type;

  DocumentList({Key key, this.titleHeader = '', this.type}) : super(key: key);
  DocumentListState createState() => DocumentListState();
}

class DocumentListState extends State<DocumentList> with AutomaticKeepAliveClientMixin {
  ApplicationBloc _appBloc;

  bool refreshing = false;
  bool isLoading = false;

  ScrollController _scrollController;
  TextEditingController _searchController = new TextEditingController();
  String searchText;
  List<LPost> _postList;

  final _searchChangeBehavior = BehaviorSubject<String>.seeded('');
  Stream<String> get _searchChangedEvent => _searchChangeBehavior.stream.debounceTime(Duration(milliseconds: 200));
  StreamSubscription _searchStreamSub;
  final _videoBehavior = BehaviorSubject<List<LPost>>.seeded(null);

  @override
  void dispose() {
    _searchChangeBehavior.close();
    _videoBehavior.close();
    _scrollController.dispose();
    _searchStreamSub.cancel();
    _searchController.dispose();
    super.dispose();
  }

  _onSearchChanged() {
    print("_onSearchChanged");
  }

  @override
  void initState() {
    _scrollController = ScrollController();

    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    _searchController.addListener(_onSearchChanged);
    searchText = "";
    _onRefresh();
    super.initState();

    // Listen search changed
    _searchStreamSub = _searchChangedEvent.listen((kw) {
      _filterDocuments(kw);
    });
    _refreshDocument();
  }

  _filterDocuments(String kw) {
    if (_postList != null) {
      if (kw.isEmpty) {
        _videoBehavior.sink.add(_postList);
      } else {
        _videoBehavior.sink.add(_postList.where((item) {
          return compareContainsNoneAccent(item.title, kw);
        }).toList());
      }
    }
  }

  Future _refreshDocument() async {
    List<LPost> posts = [];
    switch (widget.type) {
      case DocumentType.GIONG:
        posts = await PostService().getPostListByName(name: 'giong');
        break;

      case DocumentType.KTCANHTAC:
        posts = await PostService().getPostListByName(name: 'ky-thuat-canh-tac');
        break;

      case DocumentType.BAOVETHUCVAT:
        posts = await PostService().getPostListByName(name: 'bao-ve-thuc-vat');
        break;

      case DocumentType.KTXULIHOA:
        posts = await PostService().getPostListByName(name: 'ky-thuat-xu-ly-ra-hoa');
        break;

      case DocumentType.DINHDUONG:
        posts = await PostService().getPostListByName(name: 'dinh-duong');
        break;

      case DocumentType.THUHOACHBAOQUAN:
        posts = await PostService().getPostListByName(name: 'thu-hoach-bao-quan');
        break;

      case DocumentType.PHACDODIEUTRI:
        posts = await PostService().getPostListByName(name: 'phac-do-dieu-tri');
        break;

      case DocumentType.TINTUC:
//        posts = await PostService().getPostListByName(name: 'tin-tuc');
        posts = await PostService().getAllPost();
        break;

      default:
        break;
    }

    posts.sort((a, b) {
      return b.createdAt?.compareTo(a.createdAt);
    });
    _postList = posts;
    _filterDocuments(_searchChangeBehavior.stream.value);
    return Future;
  }

  Future<void> _onRefresh() async {
    setState(() {
      refreshing = true;
    });
    await _refreshDocument();
    setState(() {
      refreshing = false;
    });
    return Future;
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BaseHeader(
      title: 'Tin tức',
      body: Stack(
        children: <Widget>[
          Container(
            width: deviceWidth(context),
            margin: EdgeInsets.all(10),
            decoration:
                BoxDecoration(color: ptPlaceholder(context), borderRadius: BorderRadius.all(Radius.circular(10))),
            child: TextField(
              controller: _searchController,
              onChanged: _searchChangeBehavior.sink.add,
              decoration: InputDecoration(
                hintText: "Tìm kiếm...",
                suffixIcon: StreamBuilder<String>(
                  stream: _searchChangeBehavior.stream.debounceTime(Duration(milliseconds: 100)),
                  builder: (context, snapshot) {
                    if (snapshot.hasData == false || snapshot.data.isEmpty) return SizedBox();
                    return GestureDetector(
                      onTap: () {
                        _searchChangeBehavior.sink.add('');
                        _searchController.clear();
                      },
                      child: Icon(Icons.cancel),
                    );
                  },
                ),
                contentPadding: EdgeInsets.only(
                    left: ScaleUtil.getInstance().setHeight(20),
                    right: ScaleUtil.getInstance().setHeight(20),
                    top: ptBody1(context).fontSize * 1.1),
                border: InputBorder.none,
                enabledBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: ScaleUtil.getInstance().setHeight(60)),
            child: StreamBuilder<List<LPost>>(
                stream: _videoBehavior.stream,
                builder: (context, snapshot) {
                  if (snapshot.hasData == false)
                    Container(
                      alignment: Alignment.center,
                      child: Text('Đang tải ...'),
                    );

                  return RefreshIndicator(
                    backgroundColor: ptPrimaryColor(context),
                    onRefresh: _onRefresh,
                    child: snapshot.data == null || snapshot.data.length == 0
                        ? Container(
                            alignment: Alignment.center,
                            child: snapshot.data == null
                                ? LinearProgressIndicator(
                                    backgroundColor: ptPrimaryColor(context),
                                  )
                                : Text('Không tìm thấy kết quả', style: ptBody1(context)),
                          )
                        : ListView.builder(
                            padding: EdgeInsets.only(bottom: 150),
                            itemCount: snapshot.data.length,
                            itemBuilder: (BuildContext context, int index) {
                              LPost listDocument = snapshot.data.elementAt(index);
                              return InkWell(
                                onTap: () {
                                  Routing().navigate2(context, DocumentDetailScreen(postId: listDocument.id));
                                },
                                child: ListTile(
                                  leading: ImagePickerWidget(
                                    context: context,
                                    size: 80,
                                    resourceUrl: listDocument.thumbnail,
                                    quality: 100,
                                  ),
                                  //  Image.network(listDocument.thumbnail),
                                  title: Text(
                                    listDocument.title ?? '',
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: ptSubtitle(context),
                                  ),
                                  subtitle: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.symmetric(vertical: 5.0),
                                        child: Text(
                                          DateFormat('dd/MM/yyyy ')
                                                  .format(DateTime.parse(listDocument.createdAt).toLocal()) ??
                                              '',
                                          // style: ptCaption(context).copyWith(color: HexColor(appColor)),
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
//                            Padding(
//                              padding: EdgeInsets.only(right: 5.0),
//                              child: Icon(
//                                Icons.remove_red_eye,
//                                color: HexColor(appText60),
//                                size: ptCaption(context).fontSize * 1.2,
//                              ),
//                            ),
                                    ],
                                  ),
                                  trailing: Icon(Icons.chevron_right),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                  // isThreeLine: true,
                                ),
                              );
                            },
                          ),
                  );
                }),
          ),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
