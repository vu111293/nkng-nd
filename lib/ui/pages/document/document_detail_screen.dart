import 'package:loctroi/utils/app_store.dart';

class DocumentDetailScreen extends StatefulWidget {
  final String postId;

  DocumentDetailScreen({this.postId});

  @override
  State<StatefulWidget> createState() => DocumentDetailScreenState();
}

class DocumentDetailScreenState extends State<DocumentDetailScreen> {
  static final double MIN_FONT = 18.0;
  static final double MAX_FONT = 40.0;

  final _htmlFontBehavior = BehaviorSubject<double>.seeded(25);

  ScrollController scrollController;
  LPost itemPost;
  double fontSlider = 25;

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    scrollController = ScrollController();
    scrollController.addListener(() => setState(() {}));
    _loadPostDetail();
  }

  _loadPostDetail() async {
    LPost post = await PostService().getPostDetail(widget.postId);
    setState(() {
      itemPost = post;
    });
  }

  @override
  Widget build(BuildContext context) {
//    Matrix4 matrix = Matrix4.identity();
//    Matrix4 zerada = Matrix4.identity();
    // String contentBase64 = base64Encode(const Utf8Encoder().convert(itemPost.content));
    // controller.loadUrl('data:text/html;base64,$contentBase64');
    // print('${itemPost?.content}');
    return Scaffold(
      backgroundColor: Colors.white,
      body: NestedScrollView(
        controller: scrollController,
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            itemPost != null
                ? SliverPersistentHeader(
                    delegate: MySliverAppBar(expandedHeight: 200, itemPost: itemPost),
                    pinned: true,
                  )
                : SliverAppBar(
                    leading: IconBack(isBackground: true),
                    backgroundColor: Colors.transparent,
                    pinned: true,
                    elevation: 0.0,
                  ),
          ];
        },
        body: Padding(
          padding: EdgeInsets.only(top: 80.0),
          child: ZoomWebView(html: itemPost?.content),
        ),
      ),
    );
  }
}

class MySliverAppBar extends SliverPersistentHeaderDelegate {
  final double expandedHeight;
  LPost itemPost;
  MySliverAppBar({@required this.expandedHeight, this.itemPost});

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Stack(
      fit: StackFit.passthrough,
      overflow: Overflow.visible,
      children: [
        itemPost != null && itemPost?.thumbnail != null && itemPost?.thumbnail != '' && itemPost.thumbnail.isNotEmpty
            ? ImagePickerWidget(
                context: context,
                size: expandedHeight - shrinkOffset,
                resourceUrl: itemPost.thumbnail,
                quality: deviceWidth(context).toInt(),
              )
            : Container(
                child: Image.asset(
                  "assets/images/bg-document.jpg",
                  fit: BoxFit.cover,
                ),
              ),
        Positioned(
          top: expandedHeight - shrinkOffset - 60,
          // left: MediaQuery.of(context).size.width / 4,
          child: Opacity(
              opacity: 1 - shrinkOffset / expandedHeight > 0.3 ? 1 - shrinkOffset / expandedHeight : 0,
              child: Container(
                width: deviceWidth(context),
//                height: 130,
                padding: EdgeInsets.symmetric(horizontal: 0),
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Material(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                    animationDuration: Duration(milliseconds: 500),
                    elevation: 2.0,
                    color: ptPrimaryColor(context),
                    child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            itemPost?.title?.isNotEmpty == true ? itemPost.title : "",
                            textAlign: TextAlign.left,
                            style: ptTitle(context).copyWith(color: Colors.white),
//                                      maxLines: 2,
//                                      overflow: TextOverflow.ellipsis,
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            padding: EdgeInsets.symmetric(vertical: 5.0),
                            child: Text(
                              itemPost?.createdAt?.isNotEmpty == true
                                  ? DateFormat('dd/MM/yyyy ').format(DateTime.parse(itemPost?.createdAt).toLocal())
                                  : '',
                              textAlign: TextAlign.right,
                              style: ptSubtitle(context).copyWith(color: Colors.white),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )),
        ),
        Opacity(
            opacity: shrinkOffset / expandedHeight,
            child: Container(
              padding: EdgeInsets.only(top: 20),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                  bottom: BorderSide(
                    color: ptPrimaryColor(context),
                    width: 8,
                  ),
                ),
              ),
              height: expandedHeight + 8,
              child: Row(
                children: <Widget>[
                  IconBack(isBackground: true),
                  // Container(
                  //   height: 40,
                  //   width: 40,
                  //   decoration: BoxDecoration(
                  //     borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  //     // shape: BoxShape.circle,
                  //     color: Colors.white,
                  //     image: DecorationImage(
                  //       image: ExactAssetImage(
                  //         "assets/images/logo.png",
                  //       ),
                  //       fit: BoxFit.cover,
                  //     ),
                  //     border: Border.all(color: Colors.white, width: 1.0),
                  //   ),
                  // ),
                  Container(
                    constraints: BoxConstraints(maxWidth: deviceWidth(context) - (100)),
                    padding: EdgeInsets.only(left: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          itemPost?.title ?? "",
                          style: ptBody1(context).copyWith(),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Text(
                          itemPost?.createdAt?.isNotEmpty == true
                              ? DateFormat('dd/MM/yyyy ').format(DateTime.parse(itemPost.createdAt).toLocal())
                              : '',
                          style: ptCaption(context).copyWith(),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )),
        Positioned(
          top: kToolbarHeight - 20,
          left: 0,
          child: Opacity(
            opacity: 1 - shrinkOffset / expandedHeight > 0.2 ? 1 - shrinkOffset / expandedHeight : 0,
            child: IconBack(isBackground: true),
          ),
        ),
      ],
    );
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => kToolbarHeight + 35;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}
