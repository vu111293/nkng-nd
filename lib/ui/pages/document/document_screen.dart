import 'package:loctroi/utils/app_store.dart';

class DocumentScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => DocumentScreenState();
}

class DocumentScreenState extends State<DocumentScreen> {
  ApplicationBloc _appBloc;

  bool refreshing = false;

  bool isLoading = false;

  ScrollController _scrollController;

  TextEditingController _searchController = new TextEditingController();

  String searchText;

  @override
  void dispose() {
    _scrollController.dispose();
    // _searchController.removeListener(_onSearchChanged);
    _searchController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _scrollController = ScrollController();

    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    _searchController.addListener(_onSearchChanged);
    searchText = "";

    // TODO Phat please help me integrate model in page
    print(_appBloc.getDocumentList);
    print(_appBloc.getCurriculumList);
    print(_appBloc.getTechnicalList);
  }

  _onSearchChanged() {
    print("ok");
    // if (_debounce?.isActive ?? false) _debounce.cancel();
    // _debounce = Timer(const Duration(seconds: 1), () {
    //   if (_searchController.text != searchText) {
    //     _appBloc.getLoadingBloc().onLoadingSearch(true);
    //     setState(() {
    //       searchText = _searchController.text;
    //     });
    //     if (_searchController.text.isNotEmpty) {
    //       _appBloc
    //           .getJobsBloc()
    //           .searchFirstLoad(currentJobType[_tabController.index], _searchController.text)
    //           .then((_) {
    //         _appBloc.getLoadingBloc().onLoadingSearch(false);
    //       }).catchError((err) {
    //         print(err);
    //         _appBloc.getLoadingBloc().onLoadingSearch(false);
    //       });
    //     }
    //     _appBloc.getLoadingBloc().onLoadingSearch(false);
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Padding(
                padding: EdgeInsets.all(14.0),
                child: Image.asset(
                  "assets/images/ic_back.png",
                  fit: BoxFit.cover,
                  width: 24,
                  height: 24,
                )),
          ),
          actions: <Widget>[
            GestureDetector(
              onTap: () {
//                Routing().navigate2(context, CreatQuestionScreen());
              },
              child: Padding(
                padding: EdgeInsets.all(3),
                child: ExtendedImage.asset(
                  "assets/images/question.png",
                ),
              ),
            ),
          ],
          brightness: Brightness.light,
          title: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Tài liệu kỹ thuật',
                  style: ptHeadline(context).copyWith(color: Colors.red, fontWeight: FontWeight.bold)),
              Text('www.benhviencayanqua.vn',
                  style: ptSubhead(context).copyWith(color: ptPrimaryColor(context), fontWeight: FontWeight.bold)),
            ],
          ),
          elevation: 2,
          backgroundColor: Colors.white,
          bottom: PreferredSize(
              child: Container(
                color: ptPrimaryColor(context),
                height: 8,
              ),
              preferredSize: Size.fromHeight(8)),
        ),
        body: Stack(
          children: <Widget>[
            // Container(
            //   margin: EdgeInsets.only(top: ScaleUtil.getInstance().setHeight(10)),
            //   decoration: BoxDecoration(
            //     color: Colors.white,
            //     border: Border(
            //       top: BorderSide(
            //         color: ptPrimaryColor(context),
            //         width: ScaleUtil.getInstance().setHeight(8),
            //       ),
            //       bottom: BorderSide(
            //         color: ptPrimaryColor(context),
            //         width: ScaleUtil.getInstance().setHeight(8),
            //       ),
            //     ),
            //   ),
            //   child: Container(
            //     height: ScaleUtil.getInstance().setHeight(50),
            //     width: deviceWidth(context),
            //     decoration: BoxDecoration(
            //         gradient: LinearGradient(
            //             begin: Alignment.topRight,
            //             end: Alignment.bottomLeft,
            //             colors: [ptPrimaryColor(context), HexColor('#3FB424')])),
            //     child: TextField(
            //       controller: _searchController,
            //       style: ptTitle(context).copyWith(color: Colors.white),
            //       cursorColor: Colors.white,
            //       // onChanged: _onSearchChanged,
            //       decoration: InputDecoration(
            //         hintText: "Tìm kiếm...",
            //         contentPadding: EdgeInsets.only(
            //           left: ScaleUtil.getInstance().setHeight(20),
            //           right: _searchController.text.isNotEmpty ? 0 : ScaleUtil.getInstance().setHeight(20),
            //           top: ScaleUtil.getInstance().setHeight(16),
            //           bottom: ScaleUtil.getInstance().setHeight(16),
            //         ),
            //         hintStyle: ptTitle(context).copyWith(color: Colors.white70),
            //         border: InputBorder.none,
            //         enabledBorder: InputBorder.none,
            //         disabledBorder: InputBorder.none,
            //         focusedBorder: InputBorder.none,
            //       ),
            //     ),
            //   ),
            // ),
            Container(
              // margin: EdgeInsets.only(top: ScaleUtil.getInstance().setHeight(80)),
              child: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(bottom: 150),
                  child: Column(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          Routing().navigate2(
                              context,
                              DocumentList(
                                titleHeader: "Giống",
                                type: DocumentType.GIONG,
                                // listDocument: _appBloc.getTechnicalList,
                              ));
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(5.0)),
                              border: Border.all(color: HexColor('#00000010'))),
                          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                flex: 3,
                                child: Image.asset(
                                  "assets/images/giong-icon.png",
                                  width: 80,
                                  height: 80,
                                ),
                              ),
                              Expanded(
                                flex: 6,
                                child: Text(
                                  "Giống",
                                  style: ptTitle(context),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Routing().navigate2(
                              context,
                              DocumentList(
                                titleHeader: "Kỹ thuật canh tác",
                                type: DocumentType.KTCANHTAC,
                                // listDocument: _appBloc.getTechnicalList,
                              ));
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(5.0)),
                              border: Border.all(color: HexColor('#00000010'))),
                          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                flex: 3,
                                child: Image.asset(
                                  "assets/images/kythuatcanhtac-icon.png",
                                  width: 80,
                                  height: 80,
                                ),
                              ),
                              Expanded(
                                flex: 6,
                                child: Text(
                                  "Kỹ thuật canh tác",
                                  style: ptTitle(context),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Routing().navigate2(
                              context,
                              DocumentList(
                                titleHeader: "Bảo vệ thực vật",
                                type: DocumentType.BAOVETHUCVAT,
                                // listDocument: _appBloc.getTechnicalList,
                              ));
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(5.0)),
                              border: Border.all(color: HexColor('#00000010'))),
                          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                flex: 3,
                                child: Image.asset(
                                  "assets/images/baovethucvat-icon.png",
                                  width: 80,
                                  height: 80,
                                ),
                              ),
                              Expanded(
                                flex: 6,
                                child: Text(
                                  "Bảo vệ thực vật",
                                  style: ptTitle(context),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Routing().navigate2(
                              context,
                              DocumentList(
                                titleHeader: "Kỹ thuật xử lí ra hoa",
                                type: DocumentType.KTXULIHOA,
                              ));
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(5.0)),
                              border: Border.all(color: HexColor('#00000010'))),
                          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                flex: 3,
                                child: Image.asset(
                                  "assets/images/kythuatxulyhoa-icon.png",
                                  width: 80,
                                  height: 80,
//                                color: ptPrimaryColor(context),
                                ),
                              ),
                              Expanded(
                                flex: 6,
                                child: Text(
                                  "Kỹ thuật xử lí ra hoa",
                                  style: ptTitle(context),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Routing().navigate2(
                              context,
                              DocumentList(
                                titleHeader: "Dinh dưỡng",
                                type: DocumentType.DINHDUONG,
                                // listDocument: _appBloc.getTechnicalList,
                              ));
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(5.0)),
                              border: Border.all(color: HexColor('#00000010'))),
                          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                flex: 3,
                                child: Image.asset(
                                  "assets/images/dinhduong-icon.png",
                                  width: 80,
                                  height: 80,
                                ),
                              ),
                              Expanded(
                                flex: 6,
                                child: Text(
                                  "Dinh dưỡng",
                                  style: ptTitle(context),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Routing().navigate2(
                              context,
                              DocumentList(
                                titleHeader: "Thu hoạch và bảo quản",
                                type: DocumentType.THUHOACHBAOQUAN,
                                // listDocument: _appBloc.getTechnicalList,
                              ));
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(5.0)),
                              border: Border.all(color: HexColor('#00000010'))),
                          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                          alignment: Alignment.center,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                flex: 3,
                                child: Image.asset(
                                  "assets/images/thuhoach-icon.png",
                                  width: 80,
                                  height: 80,
                                ),
                              ),
                              Expanded(
                                flex: 6,
                                child: Text(
                                  "Thu hoạch và bảo quản",
                                  style: ptTitle(context),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              child: Image.asset(
                "assets/images/home_bottom.png",
                width: deviceWidth(context),
                fit: BoxFit.cover,
                alignment: Alignment.bottomCenter,
                // height: 130,
              ),
            ),
          ],
        ));
  }
}
