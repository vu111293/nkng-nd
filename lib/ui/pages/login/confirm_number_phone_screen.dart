import 'package:loctroi/utils/app_store.dart';

enum PinForgetState { PHONE_ENTER, CODE_ENTER }

class ConfirmNumberPhoneScreen extends StatefulWidget {
  ConfirmNumberPhoneScreen({
    Key key,
  }) : super(key: key);

  ConfirmNumberPhoneScreenState createState() => ConfirmNumberPhoneScreenState();
}

class ConfirmNumberPhoneScreenState extends State<ConfirmNumberPhoneScreen> {
  final _formKey = GlobalKey<FormState>();

  PinForgetState state = PinForgetState.PHONE_ENTER;
  String phone;
  ApplicationBloc _appBloc;
  StreamSubscription _authStream;
  TextEditingController fieldController = TextEditingController();

  String thisText = "";
  int pinLength = 4;

  bool hasError = false;
  String errorMessage;
  bool _isLoading = false;
  Timer _timer;
  final _timeCountSubject = BehaviorSubject<int>.seeded(30);

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    _registerFbAuthResult();
    _startTimer();
    super.initState();
  }

  _startTimer() {
    _disableResent();
    _timeCountSubject.sink.add(30);
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      int count = _timeCountSubject.stream.value;
      if (count >= 0) {
        count -= 1;
        _timeCountSubject.sink.add(count);
      } else {
        _timer.cancel();
      }
    });
  }

  _disableResent() {
    if (_timer != null) {
      _timer.cancel();
    }
    _timeCountSubject.sink.add(-1);
  }

  _registerFbAuthResult() {
    _authStream?.cancel();
    _authStream = PhoneAuthHelper().authStream.listen((result) async {
      if (_isLoading) {
        _isLoading = false;
        Navigator.pop(context);
      }
      if (result.status == AuthStatus.CodeSent) {
//        Toast.show("Mã xác thực đã được gửi đến bạn. Vui lòng kiểm tra", context);
        _startTimer();

        setState(() {
          phone = fieldController.text;
          fieldController.text = '';
          state = PinForgetState.CODE_ENTER;
        });
//        Routing().navigate2(context, ConfirmNumberPhoneScreen(phone: fieldController.text));
      } else if (result.status == AuthStatus.Verified) {
        // do login and cache token
        String uid = result.user.uid;
        String token = (await result.user.getIdToken()).token;
        print('FB|$uid|$token');

        Routing().navigate2(context, ChangePasswordScreen(token));
//        Farmer user = await UserService().login('FB|$uid|$token');
//        _appBloc.getAuthBloc().updateUserAction(user);
//        _authStream?.cancel();
//        Routing().popToRoot(context);
//        if (user.isClientFilledInfo) {
//          Routing().navigate2(context, HomeScreen(), routeName: '/homepage');
//        } else {
//          Routing().navigate2(context, UpdateInfoLoginScreen());
//        }
      } else if (result.status == AuthStatus.Timeout) {
        print('TIME OUT');
      } else {
        showAlertDialog(context, 'Xãy ra lỗi khi đăng nhập');
      }
    });
  }

  @override
  void dispose() {
    _authStream.cancel();
    _timer?.cancel();
    _timeCountSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScaleUtil.instance = ScaleUtil.getInstance()..init(context);
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
          // backgroundColor: Colors.white,

          body: Stack(fit: StackFit.expand, children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage('assets/images/bg_login.png'), fit: BoxFit.cover),
          ),
        ),
        SingleChildScrollView(
          child: Container(
            child: Form(
              key: _formKey,
              child: Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 60),
                      padding: EdgeInsets.symmetric(horizontal: ScaleUtil.getInstance().setWidth(30)),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(bottom: 124),
                            child: Image.asset(
                              'assets/images/app_logo_slogan.png',
                              fit: BoxFit.contain,
                              width: 110,
                            ),
                          ),
                          Text(
                            state == PinForgetState.CODE_ENTER
                                ? 'Vui lòng kiểm tra tin nhắn sms từ số điện thoại ${phone}'
                                : 'Vui lòng nhập số điện thoại',
                            textAlign: TextAlign.center,
                            style: ptCaption(context).copyWith(color: Colors.white),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 63, top: 21),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                                color: Colors.white,
                                border: Border.all(color: HexColor(appBorderColor), width: 1.5)),
                            child: Row(children: <Widget>[
                              Expanded(
                                  flex: 2,
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 12),
                                    decoration: BoxDecoration(
                                      border: Border(
                                        right: BorderSide(
                                          color: HexColor(appText60),
                                          width: 1,
                                        ),
                                      ),
                                    ),
                                    child: Text(
                                      state == PinForgetState.PHONE_ENTER ? "Số điện thoại" : "CODE",
                                      style: ptBody1(context).copyWith(color: HexColor(appText60)),
                                    ),
                                  )),
                              Expanded(
                                flex: 3,
                                child: TextFormField(
                                  controller: fieldController,
                                  maxLines: 1,
                                  style: ptBody1(context).copyWith(fontWeight: FontWeight.bold),
                                  keyboardType:
                                      state == PinForgetState.PHONE_ENTER ? TextInputType.phone : TextInputType.number,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.symmetric(horizontal: 8.5),
                                    border: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                  ),
                                ),
                              ),
                            ]),
                          ),
                          Visibility(
                            visible: hasError,
                            child: Padding(
                              padding: EdgeInsets.only(top: ScaleUtil.getInstance().setHeight(10)),
                              child: Text(
                                "Sai mã pin!",
                                style: TextStyle(color: Colors.red),
                              ),
                            ),
                          ),
                          Container(
                            width: deviceWidth(context),
                            padding: EdgeInsets.only(top: 27),
                            child: FlatButton(
                              onPressed: _handleAgreeFlow,
                              textTheme: ButtonTextTheme.primary,
                              color: _formKey?.currentState?.validate() == true ? HexColor(appColor2) : Colors.white70,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                              child: Padding(
                                padding: EdgeInsets.symmetric(vertical: 12.0),
                                child: Text("Đồng ý".toUpperCase(),
                                    style: _formKey?.currentState?.validate() == true
                                        ? ptButton(context)
                                        : ptButton(context)),
                              ),
                            ),
                          ),
                          state == PinForgetState.CODE_ENTER
                              ? StreamBuilder<int>(
                                  stream: _timeCountSubject.stream,
                                  builder: (context, snapshot) {
                                    String seconds = '';
                                    bool enable = false;
                                    if (!snapshot.hasData) {
                                    } else {
                                      if (snapshot.data <= 0) {
                                        enable = true;
                                        seconds = '';
                                      } else {
                                        enable = false;
                                        seconds = '(${snapshot.data}s)';
                                      }
                                    }
//
                                    return InkWell(
                                        child: Container(
                                          padding: EdgeInsets.only(
                                              top: ScaleUtil.getInstance().setHeight(24),
                                              right: ScaleUtil.getInstance().setWidth(10)),
                                          child: RichText(
                                              text: TextSpan(children: [
                                            TextSpan(
                                                text: 'Chưa nhận được OTP? ',
                                                style: ptCaption(context).copyWith(color: Colors.white)),
                                            TextSpan(
                                              text: enable ? "Đã gửi lại" : "Gửi lại sau $seconds",
                                              style: ptCaption(context).copyWith(
                                                color: enable ? HexColor(appColor2) : Colors.red,
                                              ),
                                            )
                                          ])),
                                        ),
                                        onTap: enable
                                            ? () {
                                                _isLoading = true;
                                                _disableResent();
                                                showWaitingDialog(context);
                                                PhoneAuthHelper().verifyPhoneNumber(phone);
                                              }
                                            : null);
                                  },
                                )
                              : Container(),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          top: 35,
          left: 16,
          child: IconBack(isBackground: false),
        ),
      ])),
    );
  }

//  Routing().navigate2(context, ChangePasswordScreen());
  _handleAgreeFlow() async {
    String value = fieldController.text;
    if (state == PinForgetState.PHONE_ENTER) {
      _isLoading = true;
      _disableResent();
      showWaitingDialog(context);
      await UserService().checkPhoneExists(value).then((isValidate) {
        print(isValidate);
        if (isValidate) {
          PhoneAuthHelper().verifyPhoneNumber(value);
        } else {
          if (_isLoading) {
            _isLoading = false;
            Navigator.pop(context);
          }
          showAlertDialog(context, 'Số điện thoại không có trong hệ thống');
        }
      });
    } else {
      if (value.length == 6) {
        _isLoading = true;
        showWaitingDialog(context);
        PhoneAuthHelper().validateCode(value);
      }
    }
  }
}
