import 'package:loctroi/utils/app_store.dart';

class LoginScreen extends StatefulWidget {
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  ApplicationBloc _appBloc;
  TextEditingController phoneController = TextEditingController();
  TextEditingController codeController = TextEditingController();
//  StreamSubscription _phoneAuthStream;
  bool _isLoading = false;

  String phone = '';

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
//    _registerFbAuthResult();
    super.initState();
  }

//  _registerFbAuthResult() {
//    _phoneAuthStream?.cancel();
//    _phoneAuthStream = PhoneAuthHelper().authStream.listen((result) async {
//      print(result.status);
//      print(result.msg);
//      if (_isLoading) {
//        Navigator.pop(context);
//        _isLoading = false;
//      }
//      if (result.status == AuthStatus.CodeSent) {
//        _phoneAuthStream?.cancel();
//        Routing().navigate2(context, ConfirmNumberPhoneScreen(phone: phoneController.text, state: PinForgetState.CODE_ENTER)).then((v) {
//          _registerFbAuthResult();
//        });
//      } else if (result.status == AuthStatus.Verified) {
//        String uid = result.user.uid;
//        String token = (await result.user.getIdToken()).token;
//        print('FB|$uid|$token');
//        Farmer user = await UserService().login('FB|$uid|$token');
//        _appBloc.getAuthBloc().updateUserAction(user);
//        _phoneAuthStream?.cancel();
//
//        if (user.isClientFilledInfo) {
//          Routing().popToRoot(context);
//          Routing().navigate2(context, HomeScreen(), routeName: '/homepage', replace: true);
//        } else {
//          Routing().navigate2(context, UpdateInfoLoginScreen(), replace: true);
//        }
//      } else if (result.status == AuthStatus.Timeout) {
////        showAlertDialog(context, 'Lỗi khi kết nối với máy chủ. Vui lòng thử lại');
//      } else {
//        showAlertDialog(context, 'Lỗi khi đăng nhập. Vui lòng thử lại');
//      }
//    });
//  }

  @override
  void dispose() {
//    _phoneAuthStream?.cancel();
    super.dispose();
  }

  showNow() {
    showGeneralDialog(
      context: context,
      barrierColor: Colors.black12.withOpacity(0.6), // background color
      barrierDismissible: false, // should dialog be dismissed when tapped outside
      barrierLabel: "Dialog", // label for barrier
      transitionDuration: Duration(milliseconds: 400), // how long it takes to popup dialog after button click
      pageBuilder: (_, __, ___) {
        // your widget implementation
        return Container(
          width: deviceWidth(context),
          height: deviceHeight(context),
          color: Colors.white,
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: kToolbarHeight / 2, left: 10, right: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Điều kiện và điều khoản sử dụng ',
                            style: ptTitle(context),
                            softWrap: true,
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: CircleAvatar(
                            backgroundColor: Colors.white54,
                            child: Icon(
                              Icons.close,
                              color: Colors.black,
                              size: 24,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          children: <Widget>[
                            Image.asset(
                              "assets/pdf/chinhsachNongGia-1.jpg",
                              fit: BoxFit.contain,
                              width: deviceWidth(context),
                            ),
                            Image.asset(
                              "assets/pdf/chinhsachNongGia-2.jpg",
                              fit: BoxFit.contain,
                              width: deviceWidth(context),
                            ),
                            Image.asset(
                              "assets/pdf/chinhsachNongGia-3.jpg",
                              fit: BoxFit.contain,
                              width: deviceWidth(context),
                            ),
                            // Image.asset(
                            //   "assets/pdf/chinhsachNongGia-4.jpg",
                            //   fit: BoxFit.contain,
                            //   width: deviceWidth(context),
                            // ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    ScaleUtil.instance = ScaleUtil.getInstance()..init(context);
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
          // backgroundColor: Colors.white,
          body: Stack(fit: StackFit.expand, children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage('assets/images/bg_login.png'), fit: BoxFit.cover),
          ),
        ),
        SingleChildScrollView(
          child: Container(
            child: Form(
              key: _formKey,
              child: Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 60),
                      padding: EdgeInsets.symmetric(horizontal: ScaleUtil.getInstance().setWidth(30)),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(bottom: 50),
                            child: Image.asset(
                              'assets/images/app_logo_slogan.png',
                              fit: BoxFit.contain,
                              width: 110,
                            ),
                          ),
                          Text(
                            'Nhập số điện thoại và mã PIN đã đăng ký để đăng nhập',
                            textAlign: TextAlign.center,
                            style: ptCaption(context).copyWith(color: Colors.white),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 13, top: 21),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                                color: Colors.white,
                                border: Border.all(color: HexColor(appBorderColor), width: 1.5)),
                            child: Row(children: <Widget>[
                              Expanded(
                                  flex: 1,
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 12),
                                    decoration: BoxDecoration(
                                      border: Border(
                                        right: BorderSide(
                                          color: HexColor(appText60),
                                          width: 1,
                                        ),
                                      ),
                                    ),
                                    child: Text(
                                      "Số điện thoại",
                                      style: ptBody1(context).copyWith(color: HexColor(appText60)),
                                    ),
                                  )),
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  controller: phoneController,
                                  maxLines: 1,
                                  style: ptBody1(context).copyWith(fontWeight: FontWeight.bold),
                                  keyboardType: TextInputType.phone,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.symmetric(horizontal: 8.5),
                                    border: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                  ),
                                  // validator: (text) {
                                  //   return text.isEmpty || text.trim() == '' ? 'Vui lòng nhập số điện thoại' : null;
                                  // },
                                ),
                              ),
                            ]),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 22),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                                color: Colors.white,
                                border: Border.all(color: HexColor(appBorderColor), width: 1.5)),
                            child: Row(children: <Widget>[
                              Expanded(
                                  flex: 1,
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 12),
                                    decoration: BoxDecoration(
                                      border: Border(
                                        right: BorderSide(
                                          color: HexColor(appText60),
                                          width: 1,
                                        ),
                                      ),
                                    ),
                                    child: Text(
                                      'Mã PIN',
                                      textAlign: TextAlign.left,
                                      style: ptBody1(context).copyWith(color: HexColor(appText60)),
                                    ),
                                  )),
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  controller: codeController,
                                  maxLines: 1,
                                  obscureText: true,
                                  style: ptBody1(context).copyWith(fontWeight: FontWeight.bold),
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.symmetric(horizontal: 8.5),
                                    border: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                  ),
                                  // validator: (text) {
                                  //   return text.isEmpty || text.trim() == '' ? 'Vui lòng mã PIN' : null;
                                  // },
                                ),
                              ),
                            ]),
                          ),
                          GestureDetector(
                            onTap: () {
                              showNow();
                            },
                            child: RichText(
                                text: TextSpan(style: ptCaption(context).copyWith(color: Colors.white), children: [
                              TextSpan(
                                text: 'Tôi đồng ý với ',
                              ),
                              TextSpan(
                                  text: "điều kiện và điều khoản sử dụng",
                                  style: ptCaption(context).copyWith(color: HexColor(appColor2))),
                              TextSpan(
                                text: ' của Lộc Trời',
                              ),
                            ])),
                          ),
                          Container(
                            width: deviceWidth(context),
                            padding: EdgeInsets.only(top: 27),
                            child: FlatButton(
                              onPressed: _handlePhoneLogin,
                              textTheme: ButtonTextTheme.primary,
                              color: _formKey?.currentState?.validate() == true ? HexColor(appColor2) : Colors.white70,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                              child: Padding(
                                  padding: EdgeInsets.symmetric(vertical: 12.0),
                                  child: Text('ĐĂNG NHẬP',
                                      style: _formKey?.currentState?.validate() == true
                                          ? ptButton(context)
                                          : ptButton(context))),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  Routing().navigate2(context, ConfirmNumberPhoneScreen());
                                },
                                child: Container(
                                  margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 24.0),
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    "Quên mã PIN?",
                                    textAlign: TextAlign.right,
                                    style: ptCaption(context).copyWith(
                                      color: HexColor(appColor2),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
//        Positioned(
//          top: 35,
//          left: 16,
//          child: GestureDetector(
//            onTap: () {
//              Navigator.pop(context);
//            },
//            child: Icon(
//              Icons.chevron_left,
//              color: Colors.white,
//            ),
//          ),
//        ),
      ])),
    );
  }

  _handlePhoneLogin() async {
    _isLoading = true;
    String phone = phoneController.text.trim();
    String code = codeController.text.trim();
    if (phone != null && phone.length > 8 && code.isNotEmpty && code.length > 0) {
      showWaitingDialog(context);
      try {
        Farmer farmer = await UserService().loginByPhone(phone, code);
        _appBloc.getAuthBloc().updateUserAction(farmer);
        Navigator.pop(context);
        Routing().navigate2(context, HomeScreen(), routeName: '/homepage', replace: true);
      } catch (e) {
        Navigator.pop(context);
        showAlertDialog(context, 'Thông tin không chính xác. Vui lòng nhập lại');
      }
    } else {
      showAlertDialog(context, 'Vui lòng nhập thông tin đăng nhập');
    }
  }
}
