import 'package:loctroi/utils/app_store.dart';

class UpdateInfoLoginScreen extends StatefulWidget {
  UpdateInfoLoginScreenState createState() => UpdateInfoLoginScreenState();
}

class UpdateInfoLoginScreenState extends State<UpdateInfoLoginScreen> {
  final _formKey = GlobalKey<FormState>();
  ApplicationBloc _appBloc;
  TextEditingController nameController;
  TextEditingController addressController;
  // TextEditingController areaController;
  TextEditingController phoneController;

  String dropdownValue = 'One';
  Farmer me;

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);

    me = _appBloc.getAuthBloc().getUser;
    nameController = TextEditingController(text: me?.fullname ?? '');
    phoneController = TextEditingController(text: me?.phone?.first ?? '');
    addressController = TextEditingController(text: me?.address ?? '');
    // areaController = TextEditingController(text: '');

    super.initState();
  }

  SizedBox sizeBox() {
    return SizedBox(
      height: 10.0,
    );
  }

  @override
  Widget build(BuildContext context) {
    ScaleUtil.instance = ScaleUtil.getInstance()..init(context);
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          elevation: 0,
          // leading: GestureDetector(
          //   onTap: () {
          //     Navigator.pop(context);
          //   },
          //   child: Icon(Icons.chevron_left),
          // ),
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(top: 16),
            child: Form(
              key: _formKey,
              child: Container(
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'assets/images/app_logo_slogan2.png',
                      fit: BoxFit.contain,
                      width: 100,
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30),
                      padding: EdgeInsets.symmetric(horizontal: ScaleUtil.getInstance().setWidth(30)),
                      child: Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(bottom: 13),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                                color: Colors.white,
                                border: Border.all(color: HexColor(appBorderColor), width: 1.5)),
                            child: Row(children: <Widget>[
                              Expanded(
                                  flex: 1,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      border: Border(
                                        right: BorderSide(
                                          color: HexColor(appText60),
                                          width: 1,
                                        ),
                                      ),
                                    ),
                                    child: Text(
                                      "Họ và tên",
                                      textAlign: TextAlign.center,
                                      style: ptBody1(context).copyWith(color: HexColor(appText60)),
                                    ),
                                  )),
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  controller: nameController,
                                  maxLines: 1,
                                  style: ptBody1(context).copyWith(fontWeight: FontWeight.bold),
                                  inputFormatters: [BlacklistingTextInputFormatter(RegExp(r"\s\s"))],
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.symmetric(horizontal: 8.5),
                                    border: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                  ),
                                  validator: (text) {
                                    return text.isEmpty || text.trim() == '' ? 'Vui lòng nhập họ tên' : null;
                                  },
                                ),
                              ),
                            ]),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 13),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                                color: Colors.white,
                                border: Border.all(color: HexColor(appBorderColor), width: 1.5)),
                            child: Row(children: <Widget>[
                              Expanded(
                                  flex: 1,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      border: Border(
                                        right: BorderSide(
                                          color: HexColor(appText60),
                                          width: 1,
                                        ),
                                      ),
                                    ),
                                    child: Text(
                                      "Số điện thoại",
                                      textAlign: TextAlign.center,
                                      style: ptBody1(context).copyWith(color: HexColor(appText60)),
                                    ),
                                  )),
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  controller: phoneController,
                                  maxLines: 1,
                                  style: ptBody1(context).copyWith(fontWeight: FontWeight.bold),
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.symmetric(horizontal: 8.5),
                                    border: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                  ),
                                  validator: (text) {
                                    return text.isEmpty || text.trim() == '' ? 'Vui lòng nhập số điện thoại' : null;
                                  },
                                ),
                              ),
                            ]),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 13),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                                color: Colors.white,
                                border: Border.all(color: HexColor(appBorderColor), width: 1.5)),
                            child: Row(children: <Widget>[
                              Expanded(
                                  flex: 1,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      border: Border(
                                        right: BorderSide(
                                          color: HexColor(appText60),
                                          width: 1,
                                        ),
                                      ),
                                    ),
                                    child: Text(
                                      'Địa chỉ liên hệ',
                                      textAlign: TextAlign.center,
                                      style: ptBody1(context).copyWith(color: HexColor(appText60)),
                                    ),
                                  )),
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  controller: addressController,
                                  maxLines: 1,
                                  style: ptBody1(context).copyWith(fontWeight: FontWeight.bold),
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.symmetric(horizontal: 8.5),
                                    border: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                  ),
                                  validator: (text) {
                                    return text.isEmpty || text.trim() == '' ? 'Vui lòng nhập địa chỉ' : null;
                                  },
                                ),
                              ),
                            ]),
                          ),
//                          Container(
//                            decoration: BoxDecoration(
//                                borderRadius: BorderRadius.all(Radius.circular(5)),
//                                color: Colors.white,
//                                border: Border.all(color: HexColor(appBorderColor), width: 1.5)),
//                            child: Row(children: <Widget>[
//                              Expanded(
//                                  flex: 1,
//                                  child: Container(
//                                    decoration: BoxDecoration(
//                                      border: Border(
//                                        right: BorderSide(
//                                          color: HexColor(appText60),
//                                          width: 1,
//                                        ),
//                                      ),
//                                    ),
//                                    child: Text(
//                                      "Diện tích nuôi",
//                                      textAlign: TextAlign.center,
//                                      style: ptBody1(context).copyWith(color: HexColor(appText60)),
//                                    ),
//                                  )),
//                              Expanded(
//                                flex: 2,
//                                child: TextFormField(
//                                  controller: areaController,
//                                  maxLines: 1,
//                                  style: ptBody1(context).copyWith(fontWeight: FontWeight.bold),
//                                  decoration: InputDecoration(
//                                    contentPadding: EdgeInsets.symmetric(horizontal: 8.5),
//                                    border: InputBorder.none,
//                                    enabledBorder: InputBorder.none,
//                                    disabledBorder: InputBorder.none,
//                                    focusedBorder: InputBorder.none,
//                                    suffixText: 'm²',
//                                  ),
//                                  inputFormatters: [
//                                    WhitelistingTextInputFormatter(RegExp(r'^[0-9]*(\.[0-9]*)?')),
//                                  ],
//                                  validator: (text) {
//                                    if (text.isEmpty) return 'Vui lòng nhập diện tích canh tác';
//                                    try {
//                                      double.parse(text);
//                                    } catch (e) {
//                                      return 'Giá trị không hợp lệ';
//                                    }
//                                    return null;
//                                  },
//                                ),
//                              ),
//                            ]),
//                          ),
                          Container(
                            width: deviceWidth(context),
                            padding: EdgeInsets.only(top: 29.0),
                            child: FlatButton(
                              onPressed: _handleUpdate,
                              textTheme: ButtonTextTheme.primary,
                              color:
                                  _formKey?.currentState?.validate() == true ? ptPrimaryColor(context) : Colors.black12,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                              child: Text(
                                "Xác nhận".toUpperCase(),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  bool _isLoading = false;
  _handleUpdate() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      String name = nameController.text;
      String address = addressController.text;
//      String area = areaController.text;

      _isLoading = true;
      showWaitingDialog(context);
      await UserService().update(me.copyWith(
//              fullName: name?.trim(),
//              address: address?.trim(),
//              area: area == null || area.isEmpty ? 0 : double.parse(area)
          )).then((me) {
        Navigator.pop(context);
        _appBloc.getAuthBloc().updateUserAction(me);
        Routing().navigate2(context, HomeScreen(), routeName: '/homepage', replace: true);
      }).catchError((e) {
        Navigator.pop(context);
        Routing().navigate2(context, HomeScreen(), routeName: '/homepage', replace: true);
        return;
      });
    }
  }
}
