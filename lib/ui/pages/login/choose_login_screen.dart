import 'package:loctroi/utils/app_store.dart';

class ChooseLoginScreen extends StatefulWidget {
  ChooseLoginScreenState createState() => ChooseLoginScreenState();
}

class ChooseLoginScreenState extends State<ChooseLoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage('assets/images/bg_splash.png'), fit: BoxFit.cover),
            ),
            // child: BackdropFilter(
            //   filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
            //   child: Container(
            //     decoration: BoxDecoration(color: ptPrimaryColor(context).withOpacity(0.1)),
            //   ),
            // ),
          ),
          Container(
            width: deviceWidth(context),
            height: deviceHeight(context),
            child: Column(
              children: <Widget>[
                Expanded(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 0),
                      child: Image.asset(
                        'assets/images/app_logo_slogan.png',
                        fit: BoxFit.contain,
                        width: deviceWidth(context) * 0.35,
                        height: deviceWidth(context) * 0.35,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 25),
                      child: Text(
                        "ỨNG DỤNG KẾT NỐI NÔNG GIA",
                        style: ptTitle(context).copyWith(
                            fontWeight: FontWeight.bold,
                            color: ptPrimaryColor(context),
                            fontSize: ptTitle(context).fontSize * 0.8),
                      ),
                    ),
                  ],
                )),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(top: 30),
                    child: Column(
                      children: <Widget>[
                        RaisedButton(
                          color: Colors.white,
                          onPressed: () async {},
                          shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.white, width: 4, style: BorderStyle.solid),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          padding: EdgeInsets.symmetric(horizontal: 90, vertical: 15),
                          child: Text(
                            "Thử kết nối lại".toUpperCase(),
                            style: ptButton(context).copyWith(color: ptPrimaryColor(context)),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 20.0),
                          child: RaisedButton(
                            onPressed: () async {},
                            shape: RoundedRectangleBorder(
                                side: BorderSide(color: ptPrimaryColor(context), width: 4, style: BorderStyle.solid),
                                borderRadius: BorderRadius.circular(5)),
                            padding: EdgeInsets.symmetric(horizontal: 90, vertical: 15),
                            child: Text(
                              "Thử kết nối lại".toUpperCase(),
                              style: ptButton(context).copyWith(color: Colors.white, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
