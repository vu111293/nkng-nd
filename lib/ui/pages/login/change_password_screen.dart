import 'package:loctroi/utils/app_store.dart';
import '../home_screen.dart';

class ChangePasswordScreen extends StatefulWidget {
  final String token;
  ChangePasswordScreen(this.token);

  ChangePasswordScreenState createState() => ChangePasswordScreenState();
}

class ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController newPwdController = TextEditingController();
  TextEditingController confirmNewPwdController = TextEditingController();
  ApplicationBloc _appBloc;
  TextEditingController emailController = TextEditingController();
  TextEditingController currentPwdController = TextEditingController();
//  StreamSubscription _phoneAuthStream;

  // For check password strength
  RegExp regex1 = new RegExp(r'^(?=.*?[A-Z])');
  RegExp regex2 = new RegExp(r'^(?=.*?[a-z])');
  RegExp regex3 = new RegExp(r'^(?=.*?[0-9])');
  RegExp regex4 = new RegExp(r'^(?=.*?[!@#\$&*~]).{1,}$');

  bool checkPass1 = false;
  bool checkPass2 = false;
  bool checkPass3 = false;
  bool checkPass4 = false;
  bool newPwdShow = false;
  bool confirmNewPwdShow = false;
  bool get isChangePin => widget.token == null;

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    super.initState();
//    _registerFbAuthResult();
  }

  @override
  void dispose() {
//    _phoneAuthStream.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScaleUtil.instance = ScaleUtil.getInstance()..init(context);
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
          // backgroundColor: Colors.white,

          body: Stack(fit: StackFit.expand, children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage('assets/images/bg_login.png'), fit: BoxFit.cover),
          ),
        ),
        SingleChildScrollView(
          child: Container(
            child: Form(
              key: _formKey,
              child: Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 60),
                      padding: EdgeInsets.symmetric(horizontal: ScaleUtil.getInstance().setWidth(30)),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(bottom: 50),
                            child: Image.asset(
                              'assets/images/app_logo_slogan.png',
                              fit: BoxFit.contain,
                              width: 110,
                            ),
                          ),
                          Text(
                            isChangePin ? '' : 'Nhập số điện thoại và mã PIN đã đăng ký để đăng nhập',
                            textAlign: TextAlign.center,
                            style: ptCaption(context).copyWith(color: Colors.white),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 13, top: 21),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                                color: Colors.white,
                                border: Border.all(color: HexColor(appBorderColor), width: 1.5)),
                            child: Row(children: <Widget>[
                              Expanded(
                                  flex: 1,
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 12),
                                    decoration: BoxDecoration(
                                      border: Border(
                                        right: BorderSide(
                                          color: HexColor(appText60),
                                          width: 1,
                                        ),
                                      ),
                                    ),
                                    child: Text(
                                      "Mã PIN mới",
                                      style: ptBody1(context).copyWith(color: HexColor(appText60)),
                                    ),
                                  )),
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  controller: newPwdController,
                                  maxLines: 1,
                                  obscureText: !newPwdShow,
                                  style: ptBody1(context).copyWith(fontWeight: FontWeight.bold),
                                  maxLength: 6,
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    counterText: "",
                                    suffixIcon: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          newPwdShow = !newPwdShow;
                                        });
                                      },
                                      icon: Icon(
                                        newPwdShow ? Icons.panorama_fish_eye : Icons.remove_red_eye,
                                        color: Colors.black38,
                                      ),
                                    ),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 8.5, vertical: 16),
                                    border: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                  ),
                                ),
                              ),
                            ]),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 22),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(5)),
                                color: Colors.white,
                                border: Border.all(color: HexColor(appBorderColor), width: 1.5)),
                            child: Row(children: <Widget>[
                              Expanded(
                                  flex: 1,
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 12),
                                    decoration: BoxDecoration(
                                      border: Border(
                                        right: BorderSide(
                                          color: HexColor(appText60),
                                          width: 1,
                                        ),
                                      ),
                                    ),
                                    child: Text(
                                      'Nhập lại',
                                      textAlign: TextAlign.left,
                                      style: ptBody1(context).copyWith(color: HexColor(appText60)),
                                    ),
                                  )),
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  controller: confirmNewPwdController,
                                  maxLines: 1,
                                  obscureText: !confirmNewPwdShow,
                                  style: ptBody1(context).copyWith(fontWeight: FontWeight.bold),
                                  keyboardType: TextInputType.number,
                                  textAlignVertical: TextAlignVertical.center,
                                  maxLength: 6,
                                  decoration: InputDecoration(
                                    counterText: "",
                                    suffixIcon: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          confirmNewPwdShow = !confirmNewPwdShow;
                                        });
                                      },
                                      icon: Icon(
                                        confirmNewPwdShow ? Icons.panorama_fish_eye : Icons.remove_red_eye,
                                        color: Colors.black38,
                                      ),
                                    ),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 8.5),
                                    border: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                  ),
                                ),
                              ),
                            ]),
                          ),
                          RichText(
                              text: TextSpan(style: ptCaption(context).copyWith(color: Colors.white), children: [
                            TextSpan(
                              text: 'Tôi đồng ý với ',
                            ),
                            TextSpan(
                                text: "điều kiện và điều khoản sử dụng ",
                                style: ptCaption(context).copyWith(color: HexColor(appColor2))),
                            TextSpan(
                              text: 'của Lộc Trời',
                            ),
                          ])),
                          Container(
                            width: deviceWidth(context),
                            padding: EdgeInsets.only(top: 27),
                            child: FlatButton(
                              onPressed: _handleChangePassword,
                              textTheme: ButtonTextTheme.primary,
                              color: _formKey?.currentState?.validate() == true ? HexColor(appColor2) : Colors.white70,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                              child: Text('CẬP NHẬT',
                                  style: _formKey?.currentState?.validate() == true
                                      ? ptButton(context)
                                      : ptButton(context)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(top: 35, left: 16, child: IconBack()),
      ])),
    );
  }

//  _registerFbAuthResult() {
//    _phoneAuthStream?.cancel();
//    _phoneAuthStream = PhoneAuthHelper().authStream.listen((result) async {
//      if (result.status == AuthStatus.Verified) {
//        Navigator.pop(context);
//
//        // Enter current password ok
//        try {
//          String newPwd = newPwdController.text;
//          print('NEW PASSWORD: $newPwd');
//          showWaitingDialog(context);
//          await UserService().changePassword(userId: _appBloc.getProfile.id, newPwd: newPwd);
//          Navigator.pop(context);
//
//          _appBloc.getAuthBloc().updateUserAction(_appBloc.getProfile.copyWith());
////          _appBloc.getAuthBloc().updateUserAction(_appBloc.getProfile.copyWith(password: newPwd));
//          // Notify to user
//          showAlertDialog(context, 'Đổi mật khẩu thành công', confirmTap: () {
//            Navigator.pop(context);
//            Navigator.pop(context);
//          });
//        } catch (e) {
//          Navigator.pop(context);
//          showAlertDialog(context, 'Xãy ra lỗi: ${e.toString()}');
//        }
//      } else {
//        Navigator.pop(context);
//        showAlertDialog(context, 'Email hoặc mật khẩu không đúng. Vui lòng thử lại');
//      }
//    });
//  }

  _handleChangePassword() async {
    if (_formKey.currentState.validate() == false) {
      return Future;
    }

    String pwd = newPwdController.text;
    String confimpwd = confirmNewPwdController.text;
    if (pwd.isEmpty || pwd.trim() == '' || pwd.length != 6) {
      return showAlertDialog(context, 'Vui lòng nhập mã PIN gồm 6 số');
    } else if (confimpwd.isEmpty || confimpwd.trim() == '') {
      return showAlertDialog(context, 'Vui lòng nhập lại mã PIN');
    } else if (confimpwd != pwd) {
      return showAlertDialog(context, 'Mã PIN không trùng khớp');
    }

    try {
      if (isChangePin) {
        await UserService().changePassword(pwd);
      } else {
        Farmer user = await UserService().forgotPassword(widget.token, pwd);
        _appBloc.getAuthBloc().updateUserAction(user);
      }
      showAlertDialog(context, 'Đã cập nhật mã pin thành công', confirmTap: () {
        if (!isChangePin) {
          Routing().popToRoot(context);
          Routing().navigate2(context, HomeScreen(), routeName: '/homepage');
        } else {
          Navigator.popUntil(context, ModalRoute.withName('/menupage'));
        }
      });
    } catch (e) {
      showAlertDialog(context, 'Xảy ra lỗi khi giao tiếp server. Vui lòng thử lại sau');
    }
    return Future;
  }
}
