import 'package:flutter_native_web/flutter_native_web.dart';
import 'package:loctroi/utils/app_store.dart';

class SpinWheelPage extends StatefulWidget {
  String url;
  SpinWheelPage(this.url);

  @override
  SpinWheelPageState createState() => SpinWheelPageState();
}

class SpinWheelPageState extends State<SpinWheelPage> {
  ApplicationBloc _appBloc;
  WebController webController;

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    FlutterNativeWeb flutterWebView = new FlutterNativeWeb(
      onWebCreated: onWebCreated,
      gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
        Factory<OneSequenceGestureRecognizer>(
          () => TapGestureRecognizer(),
        ),
      ].toSet(),
    );

    return BaseHeader(
        title: "Vòng quay",
        showNotification: false,
        body: SingleChildScrollView(
          child: new Column(
            children: <Widget>[
              new Container(
                  child: flutterWebView,
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width),
            ],
          ),
        ));
  }

  void onWebCreated(webController) {
    this.webController = webController;
    this.webController.loadUrl(widget.url);
    this.webController.onPageStarted.listen((url) {
      print("Loading $url");
      if (url == 'http://gobackapp') {
        Navigator.pop(context);
      }
    });
    this.webController.onPageFinished.listen((url) => print("Finished loading $url"));
  }
}
