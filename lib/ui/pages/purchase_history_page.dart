import 'package:loctroi/utils/app_store.dart';

class PurchaseHistoryPage extends StatefulWidget {
  PurchaseHistoryPage({Key key}) : super(key: key);

  @override
  PurchaseHistoryPageState createState() => PurchaseHistoryPageState();
}

class PurchaseHistoryPageState extends State<PurchaseHistoryPage> {
  ApplicationBloc _appBloc;

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseHeader(
      title: 'Lịch sử mua hàng',
      body: StreamBuilder<List<OrderDetail>>(
        stream: _appBloc.orderListStream,
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Container(alignment: Alignment.center, child: Text('Đang tải'));
          if (snapshot.data == null || snapshot.data.isEmpty) {
            return Container(alignment: Alignment.center, child: Text('Không có lịch sử mua hàng'));
          }
          return ItemHistoryPurchase(snapshot.data);
        },
      ),
    );
  }
}

class ItemHistoryPurchase extends StatelessWidget {
  final List<OrderDetail> items;
  ItemHistoryPurchase(this.items);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: items.length,
      itemBuilder: (context, index) {
        OrderDetail order = items[index];
        return RenderItem(order: order);
      },
    );
  }
}

class RenderItem extends StatefulWidget {
  final OrderDetail order;

  const RenderItem({Key key, @required this.order}) : super(key: key);
  @override
  _RenderItemState createState() => _RenderItemState();
}

class _RenderItemState extends State<RenderItem> {
  bool isShow = false;

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(color: ptPlaceholder(context)),
        margin: EdgeInsets.symmetric(vertical: 10),
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: <Widget>[
            ListTile(
              contentPadding: EdgeInsets.symmetric(vertical: 9, horizontal: 12),
              title: RichText(
                  overflow: TextOverflow.ellipsis,
                  text: TextSpan(children: [
                    TextSpan(
                      text: 'HD #${widget.order.billNumber}'.toUpperCase(),
                      style: ptTitle(context).copyWith(),
                    ),
                    TextSpan(
                      text: " - " + formatDate(widget.order.createdAt),
                      style: ptSubtitle(context),
                    )
                  ])),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 4),
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                      Text(
                        'Tổng loại SP: ${widget.order.totalItem}',
                        style: TextStyle(fontSize: 13),
                      ),
                      Text(
                        'Tổng số lượng SP: ${widget.order.totalAmount}',
                        style: TextStyle(
                          fontSize: 13,
                        ),
                      ),
                      widget?.order?.cancelDate != null
                          ? Padding(
                              padding: EdgeInsets.symmetric(vertical: 3.0),
                              child: RichText(
                                  overflow: TextOverflow.ellipsis,
                                  text: TextSpan(children: [
                                    TextSpan(
                                      text: 'Ngày hủy: ',
                                      style: TextStyle(fontSize: 13, color: HexColor(appText2)),
                                    ),
                                    TextSpan(
                                      text: '${formatDate(widget.order.cancelDate)}',
                                      style: TextStyle(
                                          fontSize: 13,
                                          color: widget.order.type == 'success' ? Colors.green : Colors.red),
                                    )
                                  ])),
                            )
                          : Container(),
                    ]),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        isShow = !isShow;
                      });
                    },
                    child: Row(
                      children: <Widget>[
                        Text(
                          'Xem chi tiết',
                          style: TextStyle(color: HexColor(appColor), fontSize: 11),
                        ),
                        !isShow
                            ? Icon(
                                Icons.keyboard_arrow_down,
                                color: ptPrimaryColor(context),
                                size: 15,
                              )
                            : Icon(
                                Icons.keyboard_arrow_up,
                                color: ptPrimaryColor(context),
                                size: 15,
                              )
                      ],
                    ),
                  )
                ],
              ),
              trailing: widget.order.type == 'success'
                  ? Container(
                      margin: EdgeInsets.only(top: 8),
                      constraints: BoxConstraints(maxWidth: 150, maxHeight: 40),
                      padding: EdgeInsets.symmetric(vertical: 6, horizontal: 6),
                      decoration: BoxDecoration(
                          color: ptPrimaryColor(context), borderRadius: BorderRadius.all(Radius.circular(5.0))),
                      child: Text(
                        '${formatPrice(widget.order.totalPoint)}',
                        style: ptButton(context).copyWith(color: Colors.white),
                        textAlign: TextAlign.right,
                      ),
                    )
                  : Container(
                      margin: EdgeInsets.only(top: 8),
                      padding: EdgeInsets.symmetric(vertical: 6, horizontal: 6),
                      decoration: BoxDecoration(
                          color: HexColor(appColor3), borderRadius: BorderRadius.all(Radius.circular(5.0))),
                      child: Text(
                        'Đơn đã bị hủy',
                        style: ptButton(context).copyWith(color: Colors.white),
                        textAlign: TextAlign.right,
                      ),
                    ),
            ),
            isShow
                ? Column(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: 12, right: 12),
                        color: Colors.black12,
                        height: 1,
                      ),
                      ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: widget.order.bonusPointItems.length,
                        itemBuilder: (context, index) {
                          Product product = widget.order.bonusPointItems[index];
                          return Container(
                            margin: EdgeInsets.only(left: 6),
                            color: ptPlaceholder(context),
                            child: ListTile(
                              contentPadding: EdgeInsets.symmetric(vertical: 4, horizontal: 12),
                              title: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "● ${product.name}",
                                    style: ptTitle(context).copyWith(fontSize: 13, color: ptPrimaryColor(context)),
                                  ),
                                  Container(
                                    alignment: Alignment.centerRight,
                                    width: 85,
                                    child: Text(
                                      '${formatPrice(product.totalPoint)}',
                                      style: ptTitle(context).copyWith(fontSize: 13, color: ptPrimaryColor(context)),
                                      textAlign: TextAlign.right,
                                    ),
                                  ),
                                ],
                              ),
                              subtitle: Container(
                                padding: EdgeInsets.only(top: 4, bottom: 5, left: 10),
                                child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                  Text(
                                    'Giá: ${formatPrice(product.point)}',
                                    style: TextStyle(fontSize: 13),
                                  ),
                                  Text(
                                    'Số lượng: ${product.amount}',
                                    style: TextStyle(fontSize: 13),
                                  ),
                                ]),
                              ),
                            ),
                          );
                        },
                      )
                    ],
                  )
                : Container()
          ],
        ));
  }
}
