import 'package:loctroi/ui/pages/introduce/intro_team_detail.dart';
import 'package:loctroi/utils/app_store.dart';

class ListIntroTeam extends StatefulWidget {
  bool isMap;
  ListIntroTeam({Key key, this.isMap = false}) : super(key: key);

  ListIntroTeamState createState() => ListIntroTeamState();
}

class ListIntroTeamState extends State<ListIntroTeam> with AutomaticKeepAliveClientMixin {
  ApplicationBloc _appBloc;
  LDoctor _currentDoctor;
  LHospital _currentHospital;

  bool refreshing = false;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
  }

  loadMore() async {
    isLoading = true;
  }

  popup2(String url) {
    Routing().navigate2(context, PicSwiper(0, <PicSwiperItem>[PicSwiperItem(url)]));
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (widget.isMap) {
      return Scaffold(
        body: Container(
          color: Colors.white,
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(
                      top: ScaleUtil.getInstance().setHeight(10),
                      bottom: ScaleUtil.getInstance().setHeight(20),
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(
                        top: BorderSide(
                          color: ptPrimaryColor(context),
                          width: ScaleUtil.getInstance().setHeight(8),
                        ),
                        bottom: BorderSide(
                          color: ptPrimaryColor(context),
                          width: ScaleUtil.getInstance().setHeight(8),
                        ),
                      ),
                    ),
                    child: Container(
                      height: ScaleUtil.getInstance().setHeight(40),
                      width: deviceWidth(context),
                      padding: EdgeInsets.symmetric(horizontal: ScaleUtil.getInstance().setHeight(16)),
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topRight,
                              end: Alignment.bottomLeft,
                              colors: [ptPrimaryColor(context), HexColor('#3FB424')])),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Sơ đồ Hệ thống bệnh viện',
                          style: ptTitle(context).copyWith(color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => const ZoomImageAsset(
                                imageProvider: AssetImage("assets/images/hehthongbenhvien.jpeg"),
                              ),
                            ),
                          );
                        },
                        child: Image.asset(
                          "assets/images/hehthongbenhvien.jpeg",
                          width: deviceWidth(context),
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: 150,
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text(
                        'hệ thống bệnh viện cây ăn quả tại các tỉnh thành phía nam'.toUpperCase(),
                        textAlign: TextAlign.center,
                        style: ptCaption(context).copyWith(),
                      ),
                    ),
                  ),
                ],
              ),
              Positioned(
                bottom: 0.0,
                child: Image.asset(
                  "assets/images/home_bottom.png",
                  width: deviceWidth(context),
                  fit: BoxFit.contain,
                ),
              )
            ],
          ),
        ),
      );
    } else {
      return Stack(
        children: <Widget>[
          Container(
            height: deviceHeight(context),
            // padding: EdgeInsets.only(bottom: 150),
            child: ListView.builder(
              padding: EdgeInsets.only(bottom: 150),
              itemCount: _appBloc.getHospitals.length,
              itemBuilder: (BuildContext context, int index) {
                LHospital hospitalModel = _appBloc.getHospitals[index];
                // print("phat hospitalModel ${hospitalModel.toJson()}");
                return Container(
                  decoration: BoxDecoration(
                    border: Border(bottom: BorderSide(width: 1, color: Colors.grey.withOpacity(0.2))),
                  ),
                  child: ListTile(
                    onTap: () {
                      Routing().navigate2(context, IntroTeamDetail(hospitalModel: hospitalModel));
                    },

                    leading: Image.asset(
                      hospitalModel?.avatar ?? "assets/images/logo2.png",
                      width: 60,
                      height: 60,
                    ),
                    title: Text(
                      hospitalModel?.name?.trim() ?? 'Tên bệnh viện đang cập nhật',
                      style: ptBody1(context).copyWith(fontSize: ptBody1(context).fontSize * 1.2),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    subtitle: Text(
                      hospitalModel?.address?.trim() ?? 'Địa chỉ đang cập nhật',
                      style: ptCaption(context).copyWith(color: HexColor(appColor)),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 4),
                          child: GestureDetector(
                              onTap: () {
                                if (hospitalModel.isPhoneAvailable) {
                                  openWebBrowserhURL(
                                      "tel:${hospitalModel?.phone?.trim().toString().replaceAll(' ', '')}");
                                }
                              },
                              child: Icon(Icons.phone,
                                  color: hospitalModel.isPhoneAvailable
                                      ? HexColor(appColor)
                                      : Colors.grey.withAlpha(100))),
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: 4),
                          child: GestureDetector(
                              onTap: hospitalModel.isLocationAvailable
                                  ? () {
                                      MapUtils.openMap(hospitalModel.lat, hospitalModel.long);
                                    }
                                  : null,
                              child: Icon(
                                Icons.pin_drop,
                                color:
                                    hospitalModel.isLocationAvailable ? HexColor(appColor) : Colors.grey.withAlpha(100),
                              )),
                        ),
                        Icon(Icons.chevron_right)
                      ],
                    ),

                    contentPadding: EdgeInsets.all(20),
                    // isThreeLine: true,
                  ),
                );
              },
            ),
          ),
          // Positioned(
          //   bottom: 0,
          //   child: Image.asset(
          //     "assets/images/home_bottom.png",
          //     width: deviceWidth(context) + 100,
          //     fit: BoxFit.cover,
          //     alignment: Alignment.bottomCenter,
          //     height: 130,
          //   ),
          // ),
        ],
      );
    }
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
