import 'package:loctroi/ui/pages/introduce/intro_team_list.dart';
import 'package:loctroi/utils/app_store.dart';

class IntroTeamScreen extends StatefulWidget {
  bool isMap;
  IntroTeamScreen({Key key, this.isMap = false}) : super(key: key);

  IntroTeamScreenState createState() => IntroTeamScreenState();
}

class IntroTeamScreenState extends State<IntroTeamScreen> {
  ApplicationBloc _appBloc;
  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Padding(
            padding: EdgeInsets.all(14.0),
            child: Image.asset(
              "assets/images/ic_back.png",
              fit: BoxFit.cover,
              width: 24,
              height: 24,
            ),
          ),
        ),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
//              Routing().navigate2(context, CreatQuestionScreen());
            },
            child: Padding(
              padding: EdgeInsets.all(3),
              child: ExtendedImage.asset(
                "assets/images/question.png",
              ),
            ),
          ),
        ],
        brightness: Brightness.light,
        title: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Hệ thống bệnh viện',
                style: ptHeadline(context).copyWith(color: Colors.red, fontWeight: FontWeight.bold)),
            Text('www.benhviencayanqua.vn',
                style: ptSubhead(context).copyWith(color: ptPrimaryColor(context), fontWeight: FontWeight.bold)),
          ],
        ),
        elevation: 2,
        backgroundColor: Colors.white,
        bottom: PreferredSize(
            child: Container(
              color: ptPrimaryColor(context),
              height: 8,
            ),
            preferredSize: Size.fromHeight(8)),
      ),
      body: ListIntroTeam(
        isMap: widget.isMap,
      ),
    );
  }
}
