import 'package:loctroi/ui/pages/introduce/intro_team_list.dart';
import 'package:loctroi/utils/app_store.dart';

class IntroduceScreen extends StatefulWidget {
  IntroduceScreen({Key key}) : super(key: key);

  _IntroduceScreenState createState() => _IntroduceScreenState();
}

class _IntroduceScreenState extends State<IntroduceScreen>
    with SingleTickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  ApplicationBloc _appBloc;

  bool refreshing = false;
  bool isLoading = false;

  ScrollController _scrollController;
  TabController _tabController;

  TextEditingController _searchController = new TextEditingController();
  String searchText;
  Timer _debounce;
  String introTeam = '';
  String diagramTeam = '';

  @override
  void dispose() {
    _tabController.dispose();
    _scrollController.dispose();
    _searchController.removeListener(_onSearchChanged);
    _searchController.dispose();
    super.dispose();
  }

  _onSearchChanged() {
    print("ok");
  }

  _onTabChange() {
    if (searchText.isNotEmpty) {
      // _appBloc.getLoadingBloc().onLoadingSearch(true);
      // searchText = _searchController.text;
      // print("Index tab: " + _tabController.index.toString());
      // if (_searchController.text.isNotEmpty) {
      //   _appBloc.getJobsBloc().searchFirstLoad(currentJobType[_tabController.index], _searchController.text).then((_) {
      //     _appBloc.getLoadingBloc().onLoadingSearch(false);
      //     print("ok loaded");
      //   }).catchError((err) {
      //     print(err);
      //     _appBloc.getLoadingBloc().onLoadingSearch(false);
      //   });
      // }
    }
  }

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    _tabController = TabController(length: 3, vsync: this);
    _scrollController = ScrollController();
    _tabController.addListener(_onTabChange);

    _searchController.addListener(_onSearchChanged);
    searchText = "";
    loadHtml();
    super.initState();
  }

  loadHtml() async {
    String introTeamHtml = await IntroTeamService().getIntroTeam();
    String diagramTeamHtml = await IntroTeamService().getDiagramTeam();
    setState(() {
      introTeam = introTeamHtml;
      diagramTeam = diagramTeamHtml;
    });
  }

  searchLoadMore() {
    // _appBloc.getJobsBloc().searchLoadMore(currentJobType[_tabController.index], searchText).then((_) {});
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Padding(
            padding: EdgeInsets.all(14.0),
            child: Image.asset(
              "assets/images/ic_back.png",
              fit: BoxFit.cover,
              width: 24,
              height: 24,
            ),
          ),
        ),
        brightness: Brightness.light,
        title: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Giới thiệu tổ chức',
                style: ptHeadline(context).copyWith(color: Colors.red, fontWeight: FontWeight.bold)),
            Text('www.benhviencayanqua.vn',
                style: ptSubhead(context).copyWith(color: ptPrimaryColor(context), fontWeight: FontWeight.bold)),
          ],
        ),
        elevation: 2,
        backgroundColor: Colors.white,
        actions: <Widget>[
          GestureDetector(
            onTap: () {
//              Routing().navigate2(context, CreatQuestionScreen());
            },
            child: Padding(
              padding: EdgeInsets.all(3),
              child: ExtendedImage.asset(
                "assets/images/question.png",
              ),
            ),
          ),
        ],
        bottom: PreferredSize(
            child: Container(
              color: ptPrimaryColor(context),
              height: 8,
            ),
            preferredSize: Size.fromHeight(8)),
      ),
      body: DefaultTabController(
        length: 3,
        child: Column(
          children: <Widget>[
            Container(
                constraints: BoxConstraints.expand(height: ScaleUtil.getInstance().setHeight(60)),
                margin: EdgeInsets.only(top: ScaleUtil.getInstance().setHeight(10)),
                child: Stack(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border(
                          top: BorderSide(
                            color: ptPrimaryColor(context),
                            width: ScaleUtil.getInstance().setHeight(8),
                          ),
                          bottom: BorderSide(
                            color: ptPrimaryColor(context),
                            width: ScaleUtil.getInstance().setHeight(8),
                          ),
                        ),
                      ),
                      child: Container(
                        height: ScaleUtil.getInstance().setHeight(40),
                        width: deviceWidth(context),
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.topRight,
                                end: Alignment.bottomLeft,
                                colors: [ptPrimaryColor(context), HexColor('#3FB424')])),
                      ),
                    ),
                    TabBar(
                      indicatorWeight: 3.0,
                      indicatorColor: Colors.transparent,
                      labelColor: HexColor('#FEF04C'),
                      labelStyle: ptButton(context).copyWith(color: Colors.white, fontWeight: FontWeight.bold),
                      unselectedLabelColor: Colors.white,
                      labelPadding: EdgeInsets.only(top: ScaleUtil.getInstance().setHeight(8)),
                      tabs: <Widget>[
                        Tab(
                          child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: Text("Giới thiệu chung"),
                            ),
                          ),
                          // text: "Giới thiệu chung",
                        ),
                        Tab(
                          child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: Text("Sơ đồ tổ chức"),
                            ),
                          ),
                          // text: "Sơ đồ tổ chức",
                        ),
                        Tab(
                          child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: Text("Đội ngũ Bác sĩ"),
                            ),
                          ),
                          // text: "Đội ngũ Bác sĩ",
                        ),
                      ],
                      controller: _tabController,
                    ),
                  ],
                )),
            Expanded(
              child: Container(
                color: Colors.white,
                child: Stack(
                  children: <Widget>[
                    TabBarView(controller: _tabController, physics: NeverScrollableScrollPhysics(), children: <Widget>[
                      ZoomWebView(html: introTeam, paddingBottom: 80),
                      ZoomWebView(html: diagramTeam, paddingBottom: 80),
                      ListIntroTeam()
                    ]),
                    Positioned(
                      bottom: 0,
                      child: Image.asset(
                        "assets/images/home_bottom.png",
                        width: deviceWidth(context),
                        fit: BoxFit.cover,
                        alignment: Alignment.bottomCenter,
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
