import 'package:loctroi/utils/app_store.dart';

class IntroTeamDetail extends StatefulWidget {
  LHospital hospitalModel;
  IntroTeamDetail({Key key, this.hospitalModel}) : super(key: key);
  @override
  State<StatefulWidget> createState() => IntroTeamStateDetail();
}

class IntroTeamStateDetail extends State<IntroTeamDetail> with AutomaticKeepAliveClientMixin {
  ApplicationBloc _appBloc;
  LDoctor _currentDoctor;

  bool refreshing = false;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    // if (widget.currentJobType == EnumToString.parse(JobState.APPROVED)) {
    //   _appBloc.getFuncBloc().onRefreshJob(_onRefresh);
    // }
  }

  Future<void> _onRefresh() async {
    // print("Refresh: " + widget.currentJobType);
    setState(() {
      refreshing = true;
    });
    Completer<Null> completer = new Completer();
    // _appBloc.getJobsBloc().firstLoad(widget.currentJobType).then((_) {
    //   setState(() {
    //     refreshing = false;
    //     completer.complete();
    //   });
    // }).catchError((err) {
    //   setState(() {
    //     refreshing = false;
    //     completer.complete();
    //   });
    // });
  }

  loadMore() async {
    isLoading = true;
    // await _appBloc.getJobsBloc().loadMore(widget.currentJobType).then((_) {
    //   setState(() {
    //     isLoading = false;
    //   });
    // }).catchError((_) {
    //   setState(() {
    //     isLoading = false;
    //   });
    // });
  }

  showNow(LDoctor doctor) {
    showGeneralDialog(
      context: context,
      barrierColor: Colors.black12.withOpacity(0.6), // background color
      barrierDismissible: false, // should dialog be dismissed when tapped outside
      barrierLabel: "Dialog", // label for barrier
      transitionDuration: Duration(milliseconds: 400), // how long it takes to popup dialog after button click
      pageBuilder: (_, __, ___) {
        // your widget implementation
        return Container(
          width: deviceWidth(context),
          height: deviceHeight(context),
          color: Colors.white,
          child: Stack(
            children: <Widget>[
              Container(
                height: 150,
                child: Image.asset(
                  "assets/images/bg-document.jpg",
                  fit: BoxFit.cover,
                  width: deviceWidth(context),
                  height: 150,
                ),
              ),
              SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.bottomRight,
                      height: kToolbarHeight + 20,
                      padding: EdgeInsets.only(right: 20),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: CircleAvatar(
                          backgroundColor: Colors.white54,
                          child: Icon(
                            Icons.close,
                            color: Colors.black,
                            size: 20,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: deviceWidth(context),
                      height: deviceHeight(context) - kToolbarHeight - 30,
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            // mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Align(
                                  child: Column(
                                children: <Widget>[
                                  ImagePickerWidget(
                                      context: context,
                                      size: 86,
                                      quality: 100,
                                      positionUser: 'doctor',
                                      resourceUrl: doctor.avatar),
                                  Padding(
                                    padding: EdgeInsets.symmetric(vertical: 8.0),
                                    child: Text(
                                      doctor?.fullName?.trim() ?? 'Chưa cập nhật',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: ptTitle(context).copyWith(
                                          color: HexColor(appColor), fontSize: ptBody1(context).fontSize * 1.2),
                                    ),
                                  ),
                                  RaisedButton(
                                    onPressed: () {
                                      if (doctor?.phone?.trim() != '' && doctor.phone.isNotEmpty) {
                                        openWebBrowserhURL(
                                            "tel:${doctor?.phone?.trim().toString().replaceAll(' ', '')}");
                                      }
                                    },
                                    color: doctor?.phone?.trim() != '' && doctor.phone.isNotEmpty
                                        ? ptPrimaryColor(context)
                                        : Colors.grey.withAlpha(100),
                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScaleUtil.getInstance().setWidth(20),
                                        vertical: ScaleUtil.getInstance().setHeight(15)),
                                    child: Text(
                                      "Gọi điện thoại".toUpperCase(),
                                      style: ptButton(context).copyWith(color: Colors.white),
                                    ),
                                  ),
                                ],
                              )),
                              SizedBox(height: 5),
                              RichText(
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  text: TextSpan(children: [
                                    TextSpan(
                                      text: 'Chức vụ: ',
                                      style: ptBody1(context).copyWith(color: HexColor(appColor)),
                                    ),
                                    TextSpan(
                                        text: doctor?.position?.trim() != '' && doctor?.position?.trim() != null
                                            ? doctor?.position?.trim()
                                            : "Chưa cập nhật",
                                        style: ptBody1(context).copyWith(height: 1.5))
                                  ])),
                              SizedBox(height: 5),
                              RichText(
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  text: TextSpan(children: [
                                    TextSpan(
                                      text: 'Học vị: ',
                                      style: ptBody1(context).copyWith(color: HexColor(appColor)),
                                    ),
                                    TextSpan(
                                        text: doctor?.degree?.trim() != '' && doctor?.degree?.trim() != null
                                            ? doctor?.degree?.trim()
                                            : "Chưa cập nhật",
                                        style: ptBody1(context).copyWith(height: 1.5))
                                  ])),
                              SizedBox(height: 5),
                              RichText(
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  text: TextSpan(children: [
                                    TextSpan(
                                      text: 'Bộ môn: ',
                                      style: ptBody1(context).copyWith(color: HexColor(appColor)),
                                    ),
                                    TextSpan(
                                        text: doctor?.subject?.trim() != '' && doctor?.subject?.trim() != null
                                            ? doctor?.subject?.trim()
                                            : "Chưa cập nhật",
                                        style: ptBody1(context).copyWith(height: 1.5))
                                  ])),
                              SizedBox(height: 5),
                              RichText(
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  text: TextSpan(children: [
                                    TextSpan(
                                      text: 'Chuyên ngành: ',
                                      style: ptBody1(context).copyWith(color: HexColor(appColor)),
                                    ),
                                    TextSpan(
                                        text: doctor?.specialized?.trim() != '' && doctor?.specialized?.trim() != null
                                            ? doctor?.specialized?.trim()
                                            : "Chưa cập nhật",
                                        style: ptBody1(context).copyWith(height: 1.5))
                                  ])),
                              SizedBox(height: 5),
                              RichText(
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  text: TextSpan(children: [
                                    TextSpan(
                                      text: 'Số điện thoại: ',
                                      style: ptBody1(context).copyWith(color: HexColor(appColor)),
                                    ),
                                    TextSpan(
                                        text: doctor?.phone?.trim() != '' && doctor?.phone?.trim() != null
                                            ? doctor?.phone?.trim()
                                            : "Chưa cập nhật",
                                        style: ptBody1(context).copyWith(height: 1.5))
                                  ])),
                              SizedBox(height: 5),
                              Text(
                                'Mô tả: ',
                                style: ptBody1(context).copyWith(color: HexColor(appColor)),
                              ),
                              SizedBox(height: 5),
                              Text(
                                doctor?.intro?.trim() != '' && doctor?.intro?.trim() != null
                                    ? doctor?.intro?.trim()
                                    : "Chưa cập nhật",
                                style: ptBody1(context).copyWith(),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    ScaleUtil.instance = ScaleUtil.getInstance()..init(context);

    super.build(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        brightness: Brightness.light,
        elevation: 2,
        bottom: PreferredSize(
            preferredSize: Size.fromHeight(28),
            child: Container(
              height: kToolbarHeight + 20,
              padding: EdgeInsets.symmetric(horizontal: ScaleUtil.getInstance().setHeight(10)),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                  bottom: BorderSide(
                    color: ptPrimaryColor(context),
                    width: 8.0,
                  ),
                ),
              ),
              child: Row(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Image.asset(
                        "assets/images/ic_back.png",
                        fit: BoxFit.cover,
                        width: 24,
                        height: 24,
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListTile(
                      leading: ImagePickerWidget(
                        context: context,
                        avatar: true,
                        size: kToolbarHeight * 0.9,
                        resourceUrl: widget.hospitalModel?.avatar,
                        quality: 100,
                      ),
                      title: Text(
                        widget?.hospitalModel?.name?.trim() ?? "Chưa cập nhật",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: ptBody1(context).copyWith(fontWeight: FontWeight.w600),
                      ),
                      subtitle: Text(
                        widget?.hospitalModel?.address?.trim() ?? "Chưa cập nhật",
                        style: ptCaption(context).copyWith(color: HexColor(appColor)),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      // contentPadding: EdgeInsets.all(20),
                      // isThreeLine: true,
                    ),
                  ),
                ],
              ),
            )),
      ),
      backgroundColor: Colors.white,
      body: widget.hospitalModel.doctors == null || widget.hospitalModel.doctors.length == 0
          ? Container(alignment: Alignment.center, child: Text('Chưa có bác sĩ'))
          : ListView.builder(
              itemCount: widget.hospitalModel.doctors.length,
              itemBuilder: (BuildContext context, int index) {
                LDoctor doctorModel = widget.hospitalModel.doctors.elementAt(index);
                print('phat doctorModel ${doctorModel.toJson()}');
                return InkWell(
                  onTap: () {
                    showNow(doctorModel);
                  },
                  // padding: EdgeInsets.only(top: 19),
                  // margin: EdgeInsets.symmetric(horizontal: 20),
                  child: Container(
                      padding: EdgeInsets.all(10.0),
                      child: Card(
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Row(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(right: 16),
                                child: ImagePickerWidget(
                                  context: context,
                                  size: 86,
                                  positionUser: 'doctor',
                                  resourceUrl: doctorModel.avatar,
                                  quality: 100,
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: EdgeInsets.only(right: 10.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        doctorModel?.fullName?.trim() ?? "Chưa cập nhật",
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: ptTitle(context).copyWith(
                                            color: HexColor(appColor), fontSize: ptBody1(context).fontSize * 1.2),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 5, bottom: 10),
                                        child: RichText(
                                            maxLines: 3,
                                            overflow: TextOverflow.ellipsis,
                                            text:
                                                doctorModel?.degree?.trim() != '' && doctorModel?.degree?.trim() != null
                                                    ? TextSpan(children: [
                                                        TextSpan(
                                                          text: 'Học vị: ',
                                                          style: ptBody1(context).copyWith(color: HexColor(appColor)),
                                                        ),
                                                        TextSpan(
                                                            text: doctorModel?.degree?.trim() != '' &&
                                                                    doctorModel?.degree?.trim() != null
                                                                ? doctorModel?.degree?.trim()
                                                                : "Chưa cập nhật",
                                                            style: ptBody1(context).copyWith(height: 1.5))
                                                      ])
                                                    : TextSpan(children: [])),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(right: 16),
                                child: GestureDetector(
                                  onTap: () {
                                    openWebBrowserhURL(
                                        "tel:${doctorModel?.phone?.trim().toString().replaceAll(' ', '')}");
                                  },
                                  child: Icon(
                                    Icons.phone,
                                    color: doctorModel.phone.isNotEmpty && doctorModel?.phone?.trim() != ''
                                        ? HexColor(appColor)
                                        : Colors.grey.withAlpha(100),
                                  ),
                                ),
                              ),
                              // ListTile(
                              //   leading: Image.asset(
                              //     "assets/images/bg_splash.png",
                              //     width: 86,
                              //     height: 200,
                              //     fit: BoxFit.cover,
                              //   ),
                              //   title: Text(
                              //     'TS. NGUYỄN VĂN HÒA $index ',
                              //     maxLines: 1,
                              //     style: ptBody1(context).copyWith(fontSize: ptBody1(context).fontSize * 1.2),
                              //   ),
                              //   subtitle:  SizedBox(height: 5),RichText(
                              //       maxLines: 3,
                              //       text: TextSpan(children: [
                              //         TextSpan(
                              //           text: 'Ch��c vụ: ',
                              //           style: ptCaption(context).copyWith(color: HexColor(appColor)),
                              //         ),
                              //         TextSpan(
                              //             text: "Phó viện trưởng Viện CAQ MN, Giám đốc Bệnh viện cây trồng Trung tâm…",
                              //             style: ptCaption(context).copyWith(height: 1.5))
                              //       ])),

                              //   // trailing: Icon(Icons.chevron_right),
                              //   // contentPadding: EdgeInsets.all(20),
                              //   // isThreeLine: true,
                              // ),
                            ],
                          ),
                        ),
                      )),
                );
              },
            ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
