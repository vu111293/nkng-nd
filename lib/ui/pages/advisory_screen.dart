import 'package:loctroi/utils/app_store.dart';

class AdvisoryScreen extends StatefulWidget {
  AdvisoryScreen({Key key}) : super(key: key);

  _AdvisoryScreenState createState() => _AdvisoryScreenState();
}

class _AdvisoryScreenState extends State<AdvisoryScreen>
    with SingleTickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  ApplicationBloc _appBloc;
  bool refreshing = false;
  bool isLoading = false;
  ScrollController _scrollController;
  TabController _tabController;
  int sortCheck = 0;

  TextEditingController _searchController = TextEditingController();
  String searchText;
  final _typingSearchSubject = PublishSubject<String>();

  @override
  void dispose() {
    _tabController.dispose();
    _scrollController.dispose();
    _searchController.dispose();
    _typingSearchSubject.close();
    super.dispose();
  }

  _onSearchChanged(String kw) {
    print(kw);
    _appBloc.filterDoctorQuestion(_appBloc.getDoctorFilterLatest.copyWith(keyword: kw.toLowerCase()));
  }

  _onTabChange() {
    if (searchText.isNotEmpty) {}
  }

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    _scrollController = ScrollController();
    _tabController.addListener(_onTabChange);
    sortCheck = 0;
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    _appBloc.loadUserData().then((v) {
      _checkQuestionDetailInNoti();
    }).catchError((e) {});
    searchText = "";
    super.initState();

    _registerFirebaseMessage();
  }

  Future<void> _onRefresh() async {
    Completer<Null> completer = new Completer();
    await _appBloc.loadUserData();
    setState(() {
      completer.complete();
    });
  }

  _checkQuestionDetailInNoti() async {
    if (_appBloc.actionInNoti != null) {
      NotifyAction action = _appBloc.actionInNoti;
      _appBloc.addNotifyActionWhenOpen(null);
      if (action.type == NotifyType.ISSUE) {
        LQuestionResponse q = _appBloc.allQuestionList.firstWhere((item) => item.id == action.id, orElse: () => null);
        if (q != null) {
//          await Future.delayed(Duration(milliseconds: 500), () {
//            Routing().navigate2(context, QuestionDetailScreen(question: q));
//          });
        } else {
          if (mounted) {
            Toast.show('Câu hỏi đã bị thay đổi hoặc không tồn tại', context);
          }
        }
      } else {
        /// Please handle another type
      }
    } else {
      print('can not get id');
    }
  }

  _registerFirebaseMessage() async {
    String token = await FirebaseMessaging().getToken();
    print('device id ${_appBloc.deviceId}');
    print('fcm token $token');
    NotiService().registerFcm(_appBloc.deviceId, token);
  }

  void _showSortPopup() {
    FocusScope.of(context).requestFocus(FocusNode());
    showModalBottomSheet<void>(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (BuildContext context) {
          return SingleChildScrollView(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0)),
                color: Colors.white,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0)),
                      color: ptPrimaryColor(context),
                    ),
                    padding: EdgeInsets.all(20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Sắp xếp',
                          style: ptSubtitle(context).copyWith(color: Colors.white),
                        ),
//                        InkWell(
//                          onTap: () {
//                            Navigator.pop(context);
//                          },
//                          child: Text(
//                            '|    Áp dụng',
//                            style: ptSubtitle(context).copyWith(color: Colors.white),
//                          ),
//                        ),
                      ],
                    ),
                  ),
                  sortItem(context, "Câu hỏi mới nhất", 0),
                  sortItem(context, "Câu hỏi lâu nhất", 1),
                ],
              ),
            ),
          );
        });
  }

  void _showFilterPopup() {
    FocusScope.of(context).requestFocus(FocusNode());
    showModalBottomSheet<void>(
        isScrollControlled: true,
        context: context,
        backgroundColor: Colors.transparent,
        builder: (BuildContext context) {
          return DoctorFilterQuestion();
        });
  }

  Widget sortItem(BuildContext context, String title, int check) {
    return InkWell(
      onTap: () {
        updateSortCheck(check);
      },
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 18.0, bottom: 10, left: 20, right: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  title,
                  maxLines: 1,
                  style: ptSubtitle(context).copyWith(),
                ),
                check == sortCheck
                    ? Icon(Icons.check, color: ptPrimaryColor(context), size: ptSubtitle(context).fontSize * 1.5)
                    : Container()
              ],
            ),
          ),
          Divider(),
        ],
      ),
    );
  }

  updateSortCheck(int check) {
    setState(() {
      sortCheck = check;
    });
    _appBloc.filterDoctorQuestion(_appBloc.getDoctorFilterLatest.copyWith(sort: sortCheck));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    Routing().setContext(context);
    return WillPopScope(
      child: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Scaffold(
              body: RefreshIndicator(
            key: ValueKey('NotificationScreen_refresher'),
            onRefresh: _onRefresh,
            color: Theme.of(context).primaryColor,
            child: Column(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.symmetric(vertical: kToolbarHeight / 2 + 5, horizontal: 10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: TextField(
                            controller: _searchController,
                            style: Theme.of(context).textTheme.body1,
                            onChanged: (text) {
                              _onSearchChanged(text);
                              _typingSearchSubject.sink.add(text);
                            },
                            decoration: InputDecoration(
                              suffixIcon: StreamBuilder<String>(
                                  stream: _typingSearchSubject.stream.debounceTime(Duration(milliseconds: 100)),
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData == false || snapshot.data.isEmpty) return SizedBox();
                                    return Container(
                                      child: GestureDetector(
                                        onTap: () {
                                          _onSearchChanged('');
                                          _typingSearchSubject.sink.add('');
                                          _searchController.clear();
                                        },
                                        child: Icon(
                                          Icons.cancel,
                                          color: Colors.white70,
                                          size: ptTitle(context).fontSize * 1.5,
                                        ),
                                      ),
                                    );
                                  }),
                              hintText: "Tìm kiếm... ",
                              contentPadding: EdgeInsets.only(
                                left: 20,
                                right: _searchController.text.isNotEmpty ? 0 : 20,
                                top: 16,
                                bottom: 16,
                              ),
                              hintStyle: Theme.of(context).textTheme.caption.copyWith(fontSize: 15.0),
                              fillColor: Theme.of(context).primaryColor,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(100.0)),
                                borderSide: BorderSide(color: Theme.of(context).primaryColor, width: 2.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(100.0)),
                                borderSide: BorderSide(color: HexColor(appBorderColor), width: 2.0),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: 12.0),
                        InkWell(
                          child: Padding(
                            padding: EdgeInsets.all(12.0),
                            child: ImageIcon(
                              AssetImage("assets/icons/notification.png"),
                              color: Colors.black,
                            ),
                          ),
                          onTap: () {
                            Routing().navigate2(context, NotificationScreen());
                          },
                        ),
                        InkWell(
                          child: Padding(
                            padding: EdgeInsets.all(12.0),
                            child: Icon(Icons.settings),
                          ),
                          onTap: () {
                            Routing().navigate2(context, MenuScreen(), routeName: '/menupage');
                          },
                        )
                      ],
                    )),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                        onTap: _showSortPopup,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.format_list_bulleted,
                              color: ptSubtitle(context).color,
                              size: ptSubtitle(context).fontSize * 1.5,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 8.0),
                              child: Text(
                                "Sắp xếp: ",
                                style: ptSubtitle(context).copyWith(),
                              ),
                            ),
                            Text(
                              sortCheck == 0 ? "Mới nhất" : "Lâu nhất",
                              style: ptSubtitle(context).copyWith(color: HexColor(appColor)),
                            ),
                          ],
                        ),
                      ),
                      GestureDetector(
                        onTap: _showFilterPopup,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.filter_list,
                              color: ptSubtitle(context).color,
                              size: ptSubtitle(context).fontSize * 1.5,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 8.0),
                              child: Text(
                                "Bộ lọc",
                                style: ptSubtitle(context).copyWith(),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: StreamBuilder<List<LQuestionResponse>>(
                    stream: _appBloc.doctorQuestionFilterResultEvent,
                    builder: (context, snapshot) {
                      if (snapshot.hasData == false) {
                        return Container(alignment: Alignment.center, child: Text('Đang tải...'));
                      }

                      if (snapshot.data == null || snapshot.data.length == 0) {
                        return Container(alignment: Alignment.center, child: Text('Danh sách rỗng'));
                      }

                      return ListView.builder(
                        itemBuilder: (BuildContext context, int index) {
                          LQuestionResponse question = snapshot.data[index];
                          print("LQuestionResponse ${question.toJson()}");
                          return InkWell(
                              onTap: () {
//                                Routing().navigate2(context, QuestionDetailScreen(question: question));
                              },
                              child: Container(
                                margin: EdgeInsets.only(top: 19),
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(children: <Widget>[
                                      Expanded(
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(vertical: 8.0),
                                          child: Text(
                                            question.description ?? 'None',
                                            maxLines: 3,
                                            textAlign: TextAlign.justify,
                                            style: ptBody2(context).copyWith(height: 1.4),
                                          ),
                                        ),
                                      ),
                                      question.doctorCommented
                                          ? Padding(
                                              padding: EdgeInsets.only(left: 8.0),
                                              child: Image.asset('assets/images/doctor-comment.png', width: 32.0))
                                          : Container()
                                    ]),
                                    question != null &&
                                            question.images != null &&
                                            question.images != [] &&
                                            question.images.toString() != "[]"
                                        ? Container(
                                            width: deviceWidth(context),
                                            margin: EdgeInsets.symmetric(vertical: 10),
                                            height: 60,
                                            child: ListView(
                                                shrinkWrap: true,
                                                scrollDirection: Axis.horizontal,
                                                children: question.images
                                                    .map<Widget>(
                                                      (item) => item != '' && item != null
                                                          ? ImagePickerWidget(
                                                              context: context,
                                                              size: 65,
                                                              listImage: question.images,
                                                              resourceUrl: item,
                                                            )
                                                          : SizedBox(),
                                                    )
                                                    .toList()),
                                          )
                                        : Container(),
                                    Text(question.qtyComment == null || question.qtyComment == 0
                                        ? 'Chưa có bình luận'
                                        : 'Có ${question.qtyComment} bình luận'),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 6,
                                          child: Text(
                                            "Cập nhật:  ${formatTime(question.createdAt)}",
                                            style:
                                                ptButton(context).copyWith(color: HexColor(appText60), fontSize: 10.0),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 4,
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Padding(
                                                padding: EdgeInsets.only(right: 3.0),
                                                child: Text(
                                                  "Xem chi tiết",
                                                  style: ptButton(context).copyWith(color: HexColor(appColor)),
                                                ),
                                              ),
                                              Icon(
                                                Icons.chevron_right,
                                                color: HexColor(appColor),
                                                size: ptButton(context).fontSize * 1.5,
                                              ), // icon
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(top: 19),
                                      // margin: EdgeInsets.only(bottom: 23),
                                      child: Divider(
                                        thickness: 3,
                                        color: HexColor(appBorderColor),
                                      ),
                                    )
                                  ],
                                ),
                              ));
                        },
                        itemCount: snapshot.data.length,
                      );
                    },
                  ),
                ),
              ],
            ),
          ))),
      onWillPop: () => showDialog<bool>(
        context: context,
        builder: (c) => AlertDialog(
          title: Text('Thông báo'),
          content: Text('Bạn có muốn thoát ứng dụng'),
          actions: [
            FlatButton(
              child: Text('Đồng ý'),
              onPressed: () => Platform.isIOS ? exit(0) : SystemNavigator.pop(),
            ),
            FlatButton(
              child: Text('Hủy'),
              onPressed: () => Navigator.pop(c, false),
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
