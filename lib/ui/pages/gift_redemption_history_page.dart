import 'package:loctroi/utils/app_store.dart';

class GiftRedemptionHistoryPage extends StatefulWidget {
  GiftRedemptionHistoryPage({Key key}) : super(key: key);

  @override
  _GiftRedemptionHistoryPageState createState() => _GiftRedemptionHistoryPageState();
}

class _GiftRedemptionHistoryPageState extends State<GiftRedemptionHistoryPage> {
  ApplicationBloc _appBloc;

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseHeader(
      title: 'Lịch sử đổi quà',
      body: StreamBuilder<List<ExchangePoint>>(
        stream: _appBloc.bonusExchangePointListStream,
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Container(alignment: Alignment.center, child: Text('Đang tải'));
          if (snapshot.data == null || snapshot.data.isEmpty) {
            return Container(alignment: Alignment.center, child: Text('Không có lịch sử đổi quà'));
          }
          return ItemHistoryGift(snapshot.data);
        },
      ),
    );
  }
}

class ItemHistoryGift extends StatefulWidget {
  final List<ExchangePoint> items;

  ItemHistoryGift(this.items);

  @override
  _ItemHistoryGiftState createState() => _ItemHistoryGiftState();
}

class _ItemHistoryGiftState extends State<ItemHistoryGift> {
  final dateTimeFormat = DateFormat('dd-MM-yyyy');

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: widget.items.length,
      itemBuilder: (context, index) {
        ExchangePoint exPoint = widget.items[index];
        return Container(
          decoration:
              BoxDecoration(color: ptPlaceholder(context), borderRadius: BorderRadius.all(Radius.circular(5.0))),
          margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          child: ListTile(
            contentPadding: EdgeInsets.symmetric(vertical: 9, horizontal: 12),
            title: RichText(
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
                text: TextSpan(children: [
                  TextSpan(
                    text: 'DQ${exPoint.count - index - 1} '.toUpperCase(),
                    style: ptTitle(context).copyWith(),
                  ),
                  TextSpan(
                    text: (exPoint.productCampaignCode != null && exPoint.productCampaignCode != ''
                            ? " - " + exPoint.productCampaignCode
                            : '') +
                        " - " +
                        formatDate(exPoint.createdAt),
                    style: ptSubtitle(context),
                  )
                ])),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(vertical: 4),
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                    Text(
                      'Đại lý: ${exPoint.agencyName != null && exPoint.agencyName != '' ? exPoint.agencyName : 'Chưa cập nhật'}',
                      style: TextStyle(fontSize: 13),
                    ),
                    Text(
                      'Giải thưởng: ${exPoint.giftName}',
                      style: TextStyle(fontSize: 13),
                    ),
                    Text(
                      'Doanh số đổi quà còn lại: ${formatPrice(exPoint.farmerPoint - exPoint.spinPoint)}',
                      style: TextStyle(fontSize: 13),
                    ),
                    
                  ]),
                ),
              ],
            ),
            trailing: Container(
              margin: EdgeInsets.only(top: 16),
              constraints: BoxConstraints(maxWidth: 90, maxHeight: 40),
              padding: EdgeInsets.symmetric(vertical: 6, horizontal: 6),
              decoration:
                  BoxDecoration(color: HexColor(appColor3), borderRadius: BorderRadius.all(Radius.circular(5.0))),
              child: Text(
                '-${formatPrice(exPoint.spinPoint)}',
                style: ptButton(context).copyWith(color: Colors.white),
                textAlign: TextAlign.right,
              ),
            ),
          ),
        );
      },
    );
  }
}
