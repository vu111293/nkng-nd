import 'package:loctroi/utils/app_store.dart';

List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }

  return result;
}

class HomeScreen extends StatefulWidget {
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  ApplicationBloc _appBloc;
  StreamSubscription _bannerSub;
  String version = "";
  String buildNumber = "";
  int _current = 0;

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      setState(() {
        version = packageInfo.version;
        buildNumber = packageInfo.buildNumber;
      });
    });
    _appBloc.loadUserData().then((v) {
      _checkQuestionDetailInNoti();
    }).catchError((e) {
      print(e);
    });

    _bannerSub = _appBloc.bannerListStream.listen((bannerList) {
      handleShowDialog(bannerList);
      _bannerSub.cancel();
    });
    super.initState();
    _registerFireBaseMessage();
  }

  handleShowDialog(List<BannerAds> list) {
    if (list.length == 0) return;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return BannerItem(list: list);
      },
    );
  }

  _checkQuestionDetailInNoti() async {
    if (_appBloc.actionInNoti != null) {
      NotifyAction action = _appBloc.actionInNoti;
      _appBloc.addNotifyActionWhenOpen(null);
      if (action.type == NotifyType.ISSUE) {
        LQuestionResponse q = _appBloc.allQuestionList.firstWhere((item) => item.id == action.id, orElse: () => null);
        if (q != null) {
//          await Future.delayed(Duration(milliseconds: 500), () {
//            Routing().navigate2(context, QuestionDetailScreen(question: q));
//          });
        } else {
          if (mounted) {
            Toast.show('Câu hỏi đã bị thay đổi hoặc không tồn tại', context);
          }
        }
      } else if (action.type == NotifyType.NEWS) {
        Future.delayed(Duration(milliseconds: 500), () {
          Routing().navigate2(context, NotificationScreen());
        });
      } else if (action.type == NotifyType.POST) {
        Future.delayed(Duration(milliseconds: 500), () {
          Navigator.popUntil(context, ModalRoute.withName('/home'));
          Routing().navigate2(Routing().latestContext, DocumentDetailScreen(postId: action.id));
        });
      }
    } else {
      print('can not get id');
    }
  }

  _registerFireBaseMessage() async {
    String token = await FirebaseMessaging().getToken();
    print('device id ${_appBloc.deviceId}');
    print('fcm token $token');
    NotiService().registerFcm(_appBloc.deviceId, token);
  }

  @override
  void dispose() {
    _bannerSub.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Routing().setContext(context);
    ScaleUtil.instance = ScaleUtil.getInstance()..init(context);
    return WillPopScope(
      child: BaseHeader(
        title: 'Trang chủ',
        showBack: false,
        body: Container(
          padding: EdgeInsets.only(top: kToolbarHeight / 2, bottom: ScaleUtil.getInstance().setHeight(8)),
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Column(
                children: <Widget>[
                  InkWell(
                    child: Image.asset(
                      "assets/images/app_logo_slogan2.png",
                      width: ScaleUtil.getInstance().setWidth(120),
                      fit: BoxFit.contain,
                    ),
                    onTap: () {
                      // Routing().navigate2(
                      //     context,
                      //     WebViewScreen(
                      //       url: 'https://tracuu-loctroi.web.app/',
                      //     ));
                      openWebBrowserhURL('https://tracuu-loctroi.web.app/');
                      // _bannerSub = _appBloc.bannerListStream.listen((bannerList) {
                      //   handleShowDialog(bannerList);
                      // });
                    },
                  ),
                  StreamBuilder<Farmer>(
                    stream: _appBloc.getAuthBloc().userInfoEvent,
                    builder: (context, snapshot) {
                      if (!snapshot.hasData || snapshot.data == null) return Container();
                      Farmer user = snapshot.data;
                      return Card(
                        margin: EdgeInsets.all(24.0),
                        elevation: 4,
                        child: Container(
                          padding: EdgeInsets.all(8.0),
                          child: Row(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(right: 24.0),
                                alignment: Alignment.centerRight,
                                width: 120.0,
                                child: QrImage(
                                  data: user.code,
                                  version: QrVersions.auto,
                                  size: 100.0,
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(user.fullname ?? '',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold,
                                            color: ptPrimaryColor(context))),
                                    Padding(
                                      padding: EdgeInsets.only(top: 4.0),
                                      child: Text('Tổng doanh số: ${formatPrice(user.point)}',
                                          style: ptBody1(context).copyWith(color: HexColor(appText2))),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(bottom: 1.0),
                                        child: Text('Doanh số đổi quà còn lại : ${formatPrice(user.exchangePoint)}',
                                            style: ptBody1(context).copyWith(color: HexColor(appText2)))),
                                    Padding(
                                        padding: EdgeInsets.only(bottom: 8.0),
                                        child: Text('Điểm quy đổi: ${user.pointAfter != null ? user.pointAfter.toString() : '0'}',
                                            style: ptBody1(context).copyWith(color: HexColor(appText2)))),
                                    InkWell(
                                        child: Container(
                                            padding: EdgeInsets.symmetric(vertical: 6.0, horizontal: 12.0),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(Radius.circular(45.0)),
                                                color: ptPrimaryColor(context)),
                                            child: Text('Xem thông tin',
                                                style: TextStyle(fontSize: 10.0, color: Colors.white))),
                                        onTap: () {
                                          Routing().navigate2(context, InfoUserScreen());
                                        }),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Card(
                        color: ptPrimaryColor(context),
                        shape: RoundedRectangleBorder(
                            // side: new BorderSide(color: ptPrimaryColor(context), width: 3.0),
                            borderRadius: BorderRadius.circular(5)),
                        child: InkWell(
                          splashColor: ptPrimaryColor(context).withOpacity(0.2),
                          highlightColor: ptPrimaryColor(context).withOpacity(0.2),
                          onTap: () {
                            Routing().navigate2(context, ExchangeGiftsPage());
                          },
                          child: Container(
                              padding: EdgeInsets.all(deviceWidth(context) * 0.03),
                              width: deviceWidth(context) * 0.45,
                              height: ScaleUtil.getInstance().setWidth(100),
                              child: Row(
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(right: 8),
                                    child: Image.asset(
                                      "assets/icons/dashboard/gift.png",
                                      width: ScaleUtil.getInstance().setWidth(44),
                                      height: ScaleUtil.getInstance().setWidth(44),
                                    ),
                                  ),
                                  Expanded(
                                    child: Text(
                                      'Đổi quà',
                                      style: ptTitle(context).copyWith(color: Colors.white),
                                    ),
                                  ),
                                ],
                              )),
                        ),
                      ),
                      Card(
                        color: ptPrimaryColor(context),
                        shape: RoundedRectangleBorder(
                            // side: new BorderSide(color: ptPrimaryColor(context), width: 3.0),
                            borderRadius: BorderRadius.circular(5)),
                        child: InkWell(
                          splashColor: ptPrimaryColor(context).withOpacity(0.2),
                          highlightColor: ptPrimaryColor(context).withOpacity(0.2),
                          onTap: () {
                            Routing().navigate2(
                                context,
                                DocumentList(
                                  titleHeader: "Tin tức",
                                  type: DocumentType.TINTUC,
                                  // listDocument: _appBloc.getTechnicalList,
                                ));
                          },
                          child: Container(
                              padding: EdgeInsets.all(deviceWidth(context) * 0.03),
                              width: deviceWidth(context) * 0.45,
                              height: ScaleUtil.getInstance().setWidth(100),
                              child: Row(
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(right: 8),
                                    child: Image.asset(
                                      "assets/icons/dashboard/document.png",
                                      width: ScaleUtil.getInstance().setWidth(44),
                                      height: ScaleUtil.getInstance().setWidth(44),
                                    ),
                                  ),
                                  Expanded(
                                    child: Text(
                                      'Tin tức',
                                      style: ptTitle(context).copyWith(color: Colors.white),
                                    ),
                                  ),
                                ],
                              )),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Card(
                        color: ptPrimaryColor(context),
                        shape: RoundedRectangleBorder(
                            // side: new BorderSide(color: ptPrimaryColor(context), width: 3.0),
                            borderRadius: BorderRadius.circular(5)),
                        child: InkWell(
                          splashColor: ptPrimaryColor(context).withOpacity(0.2),
                          highlightColor: ptPrimaryColor(context).withOpacity(0.2),
                          onTap: () {
                            Routing().navigate2(context, GiftRedemptionHistoryPage());
                          },
                          child: Container(
                              padding: EdgeInsets.all(deviceWidth(context) * 0.03),
                              width: deviceWidth(context) * 0.45,
                              height: ScaleUtil.getInstance().setWidth(100),
                              child: Row(
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(right: 8),
                                    child: Image.asset(
                                      "assets/icons/dashboard/file.png",
                                      width: ScaleUtil.getInstance().setWidth(44),
                                      height: ScaleUtil.getInstance().setWidth(44),
                                    ),
                                  ),
                                  Expanded(
                                    child: Text(
                                      'Lịch sử đổi quà',
                                      style: ptTitle(context).copyWith(color: Colors.white),
                                    ),
                                  ),
                                ],
                              )),
                        ),
                      ),
                      Card(
                        color: ptPrimaryColor(context),
                        shape: RoundedRectangleBorder(
                            // side: new BorderSide(color: ptPrimaryColor(context), width: 3.0),
                            borderRadius: BorderRadius.circular(5)),
                        child: InkWell(
                          splashColor: ptPrimaryColor(context).withOpacity(0.2),
                          highlightColor: ptPrimaryColor(context).withOpacity(0.2),
                          onTap: () {
                            Routing().navigate2(context, PurchaseHistoryPage());
                          },
                          child: Container(
                              padding: EdgeInsets.all(deviceWidth(context) * 0.03),
                              width: deviceWidth(context) * 0.45,
                              height: ScaleUtil.getInstance().setWidth(100),
                              child: Row(
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(right: 8),
                                    child: Image.asset(
                                      "assets/icons/dashboard/paste.png",
                                      width: ScaleUtil.getInstance().setWidth(44),
                                      height: ScaleUtil.getInstance().setWidth(44),
                                    ),
                                  ),
                                  Expanded(
                                    child: Text(
                                      'Lịch sử mua hàng',
                                      style: ptTitle(context).copyWith(color: Colors.white),
                                    ),
                                  ),
                                ],
                              )),
                        ),
                      ),
                    ],
                  )
                ],
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    'Phiên bản $version',
                    style: ptCaption(context).copyWith(color: HexColor(appText2)),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      onWillPop: () => showDialog<bool>(
        context: context,
        builder: (c) => AlertDialog(
          title: Text('Thông báo'),
          content: Text('Bạn có muốn thoát ứng dụng'),
          actions: [
            FlatButton(
              child: Text('Đồng ý'),
              onPressed: () => Platform.isIOS ? exit(0) : SystemNavigator.pop(),
            ),
            FlatButton(
              child: Text('Hủy'),
              onPressed: () => Navigator.pop(c, false),
            ),
          ],
        ),
      ),
    );
  }
}

class BannerItem extends StatefulWidget {
  final List list;
  const BannerItem({
    Key key,
    this.list,
  }) : super(key: key);

  @override
  _BannerItemState createState() => _BannerItemState();
}

class _BannerItemState extends State<BannerItem> {
  int _current = 0;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Center(
            child: Stack(
          children: <Widget>[
            Container(
              width: deviceWidth(context) - 64,
              child: CarouselSlider(
                viewportFraction: 1.0,
                height: 370,
                autoPlay: true,
                enableInfiniteScroll: false,
                onPageChanged: (index) {
                  setState(() {
                    _current = index;
                  });
                },
                items: widget.list.map((banner) {
                  return Builder(
                    builder: (BuildContext context) {
                      return Stack(
                        children: <Widget>[
                          Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    ClipRRect(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                                      child: Container(
                                          width: double.infinity,
                                          height: 180,
                                          color: Colors.black.withOpacity(0.05),
                                          child: Image.network(
                                            banner.picture ??
                                                'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAA1BMVEX///+nxBvIAAAASElEQVR4nO3BgQAAAADDoPlTX+AIVQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwDcaiAAFXD1ujAAAAAElFTkSuQmCC',
                                            fit: BoxFit.cover,
                                          )),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(top: 9, left: 12, right: 12),
                                      child: Text(
                                        banner.title ?? 'Thông báo không có tiêu đề ',
                                        style: ptTitle(context).copyWith(color: ptPrimaryColor(context)),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 4,
                                      ),
                                    ),
//                                    Container(
//                                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
//                                      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
//                                        Text( banner.message
//                                          'Từ 03/03/2020 đến ngày 03/06/2020 tất cả người dùng app Lộc Trời đều được tham gia chương trình quay số trúng thưởng với hà...',
//                                          style: TextStyle(
//                                            fontSize: 13,
//                                            color: HexColor(appText2),
//                                          ),
//                                          maxLines: 3,
//                                        ),
//                                      ]),
//                                    ),
                                  ],
                                ),
                                Expanded(
                                  child: Container(
                                    margin: EdgeInsets.fromLTRB(36, 4, 36, 16),
                                    alignment: Alignment.bottomCenter,
                                    child: RaisedButton(
                                      shape: StadiumBorder(),
                                      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 36),
                                      onPressed: () {
                                        Routing().navigate2(context, DetailNotificationScreen(banner: banner));
                                      },
                                      child: Text('Xem chi tiết'),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Positioned(
                            top: 10,
                            right: 10,
                            child: GestureDetector(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: CircleAvatar(
                                backgroundColor: Colors.white54,
                                child: Icon(
                                  Icons.close,
                                  color: Colors.black,
                                  size: 20,
                                ),
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                  );
                }).toList(),
              ),
            ),
            Positioned(
                bottom: 0.0,
                left: 0.0,
                right: 0.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: map<Widget>(widget.list, (index, url) {
                    return Container(
                      width: 8.0,
                      height: 8.0,
                      margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: _current == index ? Color.fromRGBO(0, 0, 0, 0.9) : Color.fromRGBO(0, 0, 0, 0.4),
                      ),
                    );
                  }),
                )),
          ],
        ))
      ],
    );
  }
}
