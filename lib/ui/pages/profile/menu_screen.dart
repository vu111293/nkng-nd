import 'package:loctroi/utils/app_store.dart';

class MenuScreen extends StatefulWidget {
  MenuScreen({Key key}) : super(key: key);

  MenuScreenState createState() => MenuScreenState();
}

class MenuScreenState extends State<MenuScreen> with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  ApplicationBloc _appBloc;
  String version = "";
  String buildNumber = "";

  TextEditingController _searchController = new TextEditingController();
  String searchText;

  @override
  void initState() {
    super.initState();
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      setState(() {
        version = packageInfo.version;
        buildNumber = packageInfo.buildNumber;
      });
    });
  }

  changePin() {
    Routing().navigate2(context, ChangePasswordScreen(null));
  }

  Widget description(String title, String content) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "$title",
          style: ptTitle(context).copyWith(color: HexColor(appText)).copyWith(
                fontSize: 16,
                height: 2,
              ),
        ),
        Text(
          "$content",
          style: Theme.of(context).textTheme.body2,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    ScaleUtil.instance = ScaleUtil(width: 375, height: 667)..init(context);
    return BaseHeader(
      onlyBack: true,
      title: 'Thiết lập',
      body: Column(
        children: [
          Expanded(
              child: SingleChildScrollView(
                  child: Column(children: <Widget>[
            StreamBuilder<Farmer>(
                stream: _appBloc.getAuthBloc().userInfoEvent,
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  print(snapshot.data);
                  if (snapshot.hasData == false) {
                    return Container(alignment: Alignment.center, child: Text('Loading...'));
                  }

                  Farmer currentUser = snapshot.data;
                  String avatarUrl = null; // currentUser != null ? currentUser.avatar : null;
                  String telesaleName = currentUser != null ? currentUser.fullname : '';

                  return Container(
                      color: Colors.white,
                      padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
                      // padding: EdgeInsets.only(top: 50.0, left: 20, right: 20),
                      child: Column(
                        children: <Widget>[
                          // Material(
                          //   animationDuration: Duration(milliseconds: 500),
                          //   elevation: 2.0,
                          //   borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          //   shadowColor: HexColor(appShadowColor),
                          //   child: Container(
                          //     padding: EdgeInsets.all(
                          //       ScaleUtil.getInstance().setWidth(10),
                          //     ),
                          //     child: Row(
                          //       crossAxisAlignment: CrossAxisAlignment.center,
                          //       children: <Widget>[
                          //         ImagePickerWidget(
                          //           context: context,
                          //           size: 80.0,
                          //           quality: 100,
                          //           avatar: true,
                          //           resourceUrl: avatarUrl,
                          //           onFileChanged: (fileUri, fileType) {
                          //             setState(() {
                          //               avatarUrl = fileUri;
                          //             });
                          //           },
                          //         ),
                          //         Expanded(
                          //           child: Padding(
                          //             padding: const EdgeInsets.fromLTRB(20.0, 0.0, 2.0, 0.0),
                          //             child: Text(telesaleName,
                          //                 maxLines: 2,
                          //                 overflow: TextOverflow.ellipsis,
                          //                 style: ptTitle(context).copyWith(color:HexColor(appText))),
                          //           ),
                          //         ),
                          //       ],
                          //     ),
                          //   ),
                          // ),
                          SizedBox(width: double.infinity, height: ScaleUtil.getInstance().setHeight(10)),
                          Material(
                            animationDuration: Duration(milliseconds: 500),
                            elevation: 2.0,
                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                            shadowColor: HexColor(appShadowColor),
                            child: Column(
                              children: <Widget>[
                                SizedBox(
                                  height: 10,
                                ),
                                Column(
                                  children: <Widget>[
                                    ListTile(
                                      onTap: () {
                                        Routing().navigate2(context, InfoUserScreen());
                                      },
                                      contentPadding: EdgeInsets.symmetric(
                                        horizontal: 30.0,
                                      ),
                                      leading: Icon(
                                        Icons.info,
                                        color: ptPrimaryColor(context),
                                        size: ScaleUtil.getInstance().setWidth(25),
                                      ),
                                      title: Text("Thông tin cá nhân",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: ptTitle(context).copyWith(color: HexColor(appText))),
                                    ),
                                    ListTile(
                                      onTap: () {
                                        changePin();
                                      },
                                      contentPadding: EdgeInsets.symmetric(
                                        horizontal: 30.0,
                                      ),
                                      leading: Icon(
                                        Icons.lock,
                                        color: ptPrimaryColor(context),
                                        size: ScaleUtil.getInstance().setWidth(25),
                                      ),
                                      title: Text("Đổi mã PIN",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: ptTitle(context).copyWith(color: HexColor(appText))),
                                    ),
                                    ListTile(
                                      onTap: () {
                                        Routing().navigate2(context, NotificationScreen());
                                      },
                                      contentPadding: EdgeInsets.symmetric(
                                        horizontal: 30.0,
                                      ),
                                      leading: Icon(
                                        Icons.notifications,
                                        color: ptPrimaryColor(context),
                                        size: ScaleUtil.getInstance().setWidth(25),
                                      ),
                                      title: Text("Thông báo",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: ptTitle(context).copyWith(color: HexColor(appText))),
                                    ),
//                                          ListTile(
//                                            onTap: () {
//                                              MyNavigator.goToSetting(context);
//                                            },
//                                            contentPadding: EdgeInsets.symmetric(
//                                              horizontal: 30.0,
//                                            ),
//                                            leading: Icon(
//                                              Icons.settings,
//                                              color: ptPrimaryColor(context),
//                                              size: ScaleUtil.getInstance().setWidth(25),
//                                            ),
//                                            title: Text("Cài đặt",
//                                                maxLines: 1,
//                                                overflow: TextOverflow.ellipsis,
//                                                style: ptTitle(context).copyWith(color:HexColor(appText))),
//                                          ),
                                    ListTile(
                                      onTap: () {
                                        StoreRedirect.redirect(
                                            androidAppId: "mcom.asia.nonggia", iOSAppId: "1505579851");
                                      },
                                      contentPadding: EdgeInsets.symmetric(
                                        horizontal: 30.0,
                                      ),
                                      leading: Icon(
                                        Icons.stars,
                                        color: ptPrimaryColor(context),
                                        size: ScaleUtil.getInstance().setWidth(25),
                                      ),
                                      title: Text("Đánh giá",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: ptTitle(context).copyWith(color: HexColor(appText))),
                                    ),
//                                      _appBloc.isDoctor
//                                          ? ListTile(
//                                              onTap: () {
//                                                Routing().navigate2(context, ChangePasswordScreen());
//                                              },
//                                              contentPadding: EdgeInsets.symmetric(
//                                                horizontal: 30.0,
//                                              ),
//                                              leading: Icon(
//                                                Icons.security,
//                                                color: ptPrimaryColor(context),
//                                                size: ScaleUtil.getInstance().setWidth(25),
//                                              ),
//                                              title: Text("Đổi mật khẩu",
//                                                  maxLines: 1,
//                                                  overflow: TextOverflow.ellipsis,
//                                                  style: ptTitle(context).copyWith(color:HexColor(appText))),
//                                            )
//                                          : Container(),
                                    ListTile(
                                      onTap: _logoutConfirm,
                                      contentPadding: EdgeInsets.symmetric(
                                        horizontal: 30.0,
                                      ),
                                      leading: Icon(
                                        Icons.cancel,
                                        color: ptPrimaryColor(context),
                                        size: ScaleUtil.getInstance().setWidth(25),
                                      ),
                                      title: Text("Đăng xuất",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: ptTitle(context).copyWith(color: HexColor(appText))),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ));
                })
          ]))),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  _handleLogout() async {
    NotiService().unregisterFcm();

    // clean cache
    LoopBackAuth().clear();
    Routing().popToRoot(context);
    Routing().navigate2(context, LoginScreen(), replace: true);
  }

  _logoutConfirm() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              "Thông báo",
              style: ptTitle(context).copyWith(color: HexColor(appText)),
            ),
            content: Text(
              "Bạn chắc chắn muốn đăng xuất?",
              style: ptSubtitle(context),
            ),
            actions: <Widget>[
              RawMaterialButton(
                fillColor: ptPrimaryColor(context),
                onPressed: () {
                  Navigator.pop(context, true);
                },
                child: Text(
                  "Đóng",
                  style: ptSubtitle(context).copyWith(color: Colors.white),
                ),
              ),
              RawMaterialButton(
                onPressed: _handleLogout,
                child: Text(
                  "Đăng xuất",
                  style: ptSubtitle(context),
                ),
              )
            ],
          );
        });
  }
}
