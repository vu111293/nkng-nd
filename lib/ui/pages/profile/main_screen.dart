import 'package:loctroi/ui/pages/introduce/introduce_screen.dart';
import 'package:loctroi/utils/app_store.dart';

import 'menu_screen.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key key}) : super(key: key);

  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  ApplicationBloc _appBloc;
  List<BottomNavigationBarItem> _items;
  int _index = 0;
  PageController _pageController;

  List<Widget> _widgetOptions;

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    _appBloc.loadUserData();

    _pageController = PageController();
    setupPages();
    super.initState();

    _appBloc.mainScreenBloc.switchPageEvent.listen((pageIndex) {
      if (_index != pageIndex && mounted) {
        setState(() {
          _index = pageIndex;
          navigationTapped(_index);
        });
      }
    });
  }

  setupPages() {
    _items = List();
    if (_appBloc.isDoctor) {
      _items.add(
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text(
            'Tư vấn',
            style: TextStyle(
              fontFamily: "OpenSans-Regular",
              fontSize: 10.0,
              // color: HexColor(appText),
            ),
          ),
        ),
      );
      _items.add(
        BottomNavigationBarItem(
          icon: Icon(Icons.menu),
          title: Text(
            'Liên hệ',
            style: TextStyle(
              fontFamily: "OpenSans-Regular",
              fontSize: 10.0,
              // color: HexColor(appText),
            ),
          ),
        ),
      );
    } else {
      _items.add(
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text(
            'Tư vấn',
            style: TextStyle(
              fontFamily: "OpenSans-Regular",
              fontSize: 10.0,
              // color: HexColor(appText),
            ),
          ),
        ),
      );
      _items.add(
        BottomNavigationBarItem(
          icon: Icon(Icons.library_books),
          title: Text(
            'Tài liệu',
            style: TextStyle(
              fontFamily: "OpenSans-Regular",
              fontSize: 10.0,
              // color: HexColor(appText),
            ),
          ),
        ),
      );
      _items.add(
        BottomNavigationBarItem(
          icon: Icon(Icons.people),
          title: Text(
            'Giới thiệu',
            style: TextStyle(
              fontFamily: "OpenSans-Regular",
              fontSize: 10.0,
              // color: HexColor(appText),
            ),
          ),
        ),
      );
      _items.add(
        BottomNavigationBarItem(
          icon: Icon(Icons.menu),
          title: Text(
            'Liên hệ',
            style: TextStyle(
              fontFamily: "OpenSans-Regular",
              fontSize: 10.0,
              // color: HexColor(appText),
            ),
          ),
        ),
      );
    }
  }

  void navigationTapped(int page) {
    _pageController.jumpToPage(page);
  }

  void _onPageChange(int page) {
    setState(() {
      this._index = page;
    });
  }

  preBuildPage() {
    if (_appBloc.isDoctor) {
      _widgetOptions = <Widget>[AdvisoryScreen(), MenuScreen()];
    } else {
      _widgetOptions = <Widget>[AdvisoryScreen(), DocumentScreen(), IntroduceScreen(), MenuScreen()];
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_widgetOptions == null) {
      preBuildPage();
    }

    return WillPopScope(
      child: Scaffold(
        body: PageView(
          controller: _pageController,
          onPageChanged: _onPageChange,
          // width: MediaQuery.of(context).size.width - 10,
          physics: NeverScrollableScrollPhysics(),
          children: _widgetOptions,
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: _items,
          unselectedItemColor: HexColor(appText60),
          fixedColor: ptPrimaryColor(context),
          currentIndex: _index,
          onTap: (int item) {
            navigationTapped(item);
          },
        ),
      ),
      onWillPop: () => showDialog<bool>(
        context: context,
        builder: (c) => AlertDialog(
          title: Text('Thông báo'),
          content: Text('Bạn có muốn thoát ứng dụng'),
          actions: [
            FlatButton(
              child: Text('Đồng ý'),
              onPressed: () => Platform.isIOS ? exit(0) : SystemNavigator.pop(),
            ),
            FlatButton(
              child: Text('Hủy'),
              onPressed: () => Navigator.pop(c, false),
            ),
          ],
        ),
      ),
    );
  }
}
