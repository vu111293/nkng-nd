import 'package:loctroi/utils/app_store.dart';

class DetailNotificationScreen extends StatefulWidget {
  final BannerAds banner;
  final NotificationData notify;
  DetailNotificationScreen({this.notify, this.banner});

  @override
  DetailNotificationScreenState createState() => DetailNotificationScreenState();
}

class DetailNotificationScreenState extends State<DetailNotificationScreen> with AutomaticKeepAliveClientMixin {
  ApplicationBloc _appBloc;
  String header = '';
  String title = '';
  String message = '';
  String coverURL = '';
  String createdAt;

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);

    if (this.widget.banner != null) {
      header = 'Thông báo';
      title = widget.banner.title;
      message = widget.banner.message ?? '';
      coverURL = widget.banner.picture;
      createdAt = widget.banner.createdAt;

    } else if (widget.notify != null) {
      header = 'Thông báo';
      title = widget.notify.title;
      message = widget.notify.message ?? '';
      coverURL = widget.notify.picture;
      createdAt = widget.notify.createdAt;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseHeader(
      title: header,
      showNotification: false,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Image.network(
              coverURL ??
                  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAA1BMVEX///+nxBvIAAAASElEQVR4nO3BgQAAAADDoPlTX+AIVQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwDcaiAAFXD1ujAAAAAElFTkSuQmCC',
              fit: BoxFit.fitWidth,
              width: deviceWidth(context),
            ),
            Container(
              width: double.infinity,
              padding: EdgeInsets.fromLTRB(16, 16, 16, 8),
              child: Text(
                DateFormat("hh:mm:ss dd-MM-yyyy").format(DateTime.parse(createdAt)),
                style: ptSubtitle(context),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 5),
              child: Text(
                title ?? 'Thông báo không có tiêu đề',
                style: ptCaption(context).copyWith(color: Colors.green, fontSize: ptBody1(context).fontSize * 1.1),
                textAlign: TextAlign.justify,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 5),
              child: Text(
                message,
                style: ptBody1(context).copyWith(fontSize: ptBody1(context).fontSize * 1.1),
                textAlign: TextAlign.justify,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
