import 'package:loctroi/apis/net/business_service.dart';
import 'package:loctroi/utils/app_store.dart';

class NotificationScreen extends StatefulWidget {
  NotificationScreen({Key key}) : super(key: key);

  @override
  NotificationScreenState createState() => NotificationScreenState();
}

class NotificationScreenState extends State<NotificationScreen> with AutomaticKeepAliveClientMixin {
  ApplicationBloc _appBloc;

  final _searchBarSubject = BehaviorSubject<String>.seeded('');

  Stream<List<NotificationData>> _notiStream;

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    _notiStream = Observable.combineLatest2<List<NotificationData>, String, List<NotificationData>>(
        _appBloc.notificationListStream, _searchBarSubject.stream.debounceTime(Duration(milliseconds: 500)), (a, b) {
      if (a == null || a.isEmpty) return [];
      if (b == null || b.isEmpty) return a;
      return a.where((item) => item.message.toLowerCase().contains(b.toLowerCase())).toList();
    });

    super.initState();
  }

  @override
  void dispose() {
    _searchBarSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BaseHeader(
      title: 'Thông báo',
      showNotification: false,
      body: Column(
        children: <Widget>[
          Container(
            // height: ScaleUtil.getInstance().setHeight(50),
            width: deviceWidth(context),
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(color: Colors.black12, borderRadius: BorderRadius.all(Radius.circular(10))),
            child: TextField(
              // controller: _searchController,
              style: ptBody1(context),
              // onChanged: _searchChangeBehavior.sink.add,
              decoration: InputDecoration(
                hintText: "Tìm kiếm...",
                suffixIcon: StreamBuilder<String>(
                  // stream: _searchChangeBehavior.stream.debounceTime(Duration(milliseconds: 100)),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) return Container(alignment: Alignment.center, child: Text('Đang tải'));
                    if (snapshot.data == null || snapshot.data.isEmpty) {
                      return Container(alignment: Alignment.center, child: Text('Không có thông báo'));
                    }
                    return GestureDetector(
                      onTap: () {
                        // _searchChangeBehavior.sink.add('');
                        // _searchController.clear();
                      },
                      child: Icon(
                        Icons.cancel,
                        color: Colors.white70,
                        size: ptBody1(context).fontSize,
                      ),
                    );
                  },
                ),
                contentPadding: EdgeInsets.only(
                    left: ScaleUtil.getInstance().setHeight(20),
                    right: ScaleUtil.getInstance().setHeight(20),
                    top: ptBody1(context).fontSize * 1.1),
                hintStyle: ptBody1(context),
                border: InputBorder.none,
                enabledBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
              ),
              onChanged: _searchBarSubject.sink.add,
            ),
          ),
          Expanded(
              child: StreamBuilder<List<NotificationData>>(
            stream: _notiStream,
            builder: (context, snapshot) {
              if (!snapshot.hasData || snapshot.data == null) return Container(child: Text('Đang tải...'));
              return NotificationList(snapshot.data);
            },
          )),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class NotificationList extends StatefulWidget {
  final List<NotificationData> items;
  NotificationList(this.items);

  @override
  NotificationListState createState() => NotificationListState();
}

class NotificationListState extends State<NotificationList> {
  final dateFormat = DateFormat("hh:mm:ss dd-MM-yyyy");
  ApplicationBloc _appBloc;

  @override
  Widget build(BuildContext context) {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    return ListView.builder(
      shrinkWrap: true,
      itemCount: widget.items.length,
      itemBuilder: (context, index) {
        NotificationData noti = widget.items[index];
        return Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 10, left: 10),
              child: CircleAvatar(
                child: Image.asset('assets/images/logo.png'),
              ),
            ),
            GestureDetector(
              onTap: () {
                BusinessService().seen(noti.notifyId);
                Routing()
                    .navigate2(context, DetailNotificationScreen(notify: noti))
                    .then((_) => _appBloc.getAuthBloc().getUnreadCount());
              },
              child: Container(
                width: deviceWidth(context) - 80,
                decoration: BoxDecoration(
                    color: ptPlaceholder(context),
                    border: Border.all(color: ptPlaceholder(context), width: 1),
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: Column(
                  children: <Widget>[
//                     Column(
//                       children: <Widget>[
//                         ClipRRect(
//                           borderRadius:
//                               BorderRadius.only(topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0)),
//                           child: Image.network(noti.picture),
//                         ),
//                         Padding(
//                           padding: EdgeInsets.only(top: 9, left: 12, right: 12),
//                           child: Text(
//                             noti.message,
// //                          'Mini Game: QUAY ĐỀU TAY - QUÀ TẶNG TRAO NGAY',
//                             style: ptTitle(context).copyWith(color: ptPrimaryColor(context)),
//                           ),
//                         )
//                       ],
//                     ),
                    ClipRRect(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0)),
                      child: Image.network(
                        noti.picture ??
                            'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAA1BMVEX///+nxBvIAAAASElEQVR4nO3BgQAAAADDoPlTX+AIVQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwDcaiAAFXD1ujAAAAAElFTkSuQmCC',
                        fit: BoxFit.fitWidth,
                        width: deviceWidth(context) - 80,
                      ),
                    ),
                    ListTile(
                      title: Text(
                        noti.title ?? 'Thông báo không có tiêu đề',
                        style: TextStyle(
                          fontSize: 13,
                          color: HexColor(appText),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        );
      },
    );
  }
}
