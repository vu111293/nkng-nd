import 'package:loctroi/utils/app_store.dart';

class InfoUserScreen extends StatefulWidget {
  InfoUserScreenState createState() => InfoUserScreenState();
}

class InfoUserScreenState extends State<InfoUserScreen> {
  final _formKey = GlobalKey<FormState>();
  ApplicationBloc _appBloc;
  TextEditingController nameController;
  TextEditingController addressController;
  TextEditingController areaController;
  TextEditingController phoneController;

  String dropdownValue = 'One';
  Farmer me;
  String avatar;

  bool isUpdateInfo = false;

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);

    me = _appBloc.getAuthBloc().getUser;
//    avatar = me.avatar ?? 'https://via.placeholder.com/150';
    avatar = 'https://via.placeholder.com/150';
    nameController = TextEditingController(text: '');
    phoneController = TextEditingController(text: me?.phone?.first ?? '');
    addressController = TextEditingController(text: me.address ?? '');
    areaController = TextEditingController(text: '');

    super.initState();
  }

  SizedBox sizeBox() {
    return SizedBox(
      height: 10.0,
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseHeader(
      title: 'Thông tin nông dân',
      onlyBack: true,
      body: StreamBuilder<Farmer>(
          stream: _appBloc.getAuthBloc().userInfoEvent,
          builder: (context, snapshot) {
            if (!snapshot.hasData || snapshot.data == null) return Container();
            Farmer user = snapshot.data;
            return Form(
              key: _formKey,
              child: SingleChildScrollView(
                physics: ClampingScrollPhysics(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Card(
                            color: ptPrimaryColor(context),
                            elevation: 4,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16))),
                            child: Container(
                              width: deviceWidth(context),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.all(16),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.max,
                                      children: <Widget>[
                                        Text('Họ tên', style: TextStyle(fontSize: 13.0, color: Colors.white)),
                                        Text(user.fullname,
                                            style: TextStyle(
                                                fontSize: 24, fontWeight: FontWeight.bold, color: Colors.white)),
                                        Padding(
                                          padding: EdgeInsets.only(top: 4.0),
                                          child: Text('Tổng doanh số: ${formatPrice(user.point)}',
                                              style: ptBody1(context).copyWith(color: Colors.white)),
                                        ),
                                        Padding(
                                            padding: EdgeInsets.only(bottom: .0),
                                            child: Text('Doanh số đổi quà còn lại: ${formatPrice(user.exchangePoint)}',
                                                style: ptBody1(context).copyWith(color: Colors.white))),
                                        Padding(
                                            padding: EdgeInsets.only(bottom: 4.0),
                                            child: Text('Điểm quy đổi: ${user.pointAfter != null?user.pointAfter.toString(): '0'}',
                                                style: ptBody1(context).copyWith(color: Colors.white))),
                                      ],
                                    ),
                                  ),
//                            Container(
//                              decoration: BoxDecoration(
//                                  borderRadius: BorderRadius.vertical(bottom: Radius.circular(16.0)),
//                                  color: Colors.white),
//                              child: Row(
//                                mainAxisSize: MainAxisSize.min,
//                                children: <Widget>[
//                                  Expanded(
//                                    child: InkWell(
//                                      onTap: () {},
//                                      child: Container(
//                                          padding: EdgeInsets.symmetric(vertical: 6.0, horizontal: 12.0),
//                                          child: Text('Bán hàng',
//                                              textAlign: TextAlign.center,
//                                              style: TextStyle(
//                                                color: ptPrimaryColor(context),
//                                              ))),
//                                    ),
//                                  ),
//                                  Container(
//                                    color: ptPrimaryColor(context),
//                                    width: 1.5,
//                                    height: 32,
//                                  ),
//                                  Expanded(
//                                    child: InkWell(
//                                      onTap: () {},
//                                      child: Container(
//                                          padding: EdgeInsets.symmetric(vertical: 6.0, horizontal: 12.0),
//                                          child: Text('Lịch sử mua hàng',
//                                              textAlign: TextAlign.center,
//                                              style: TextStyle(
//                                                color: ptPrimaryColor(context),
//                                              ))),
//                                    ),
//                                  ),
//                                ],
//                              ),
//                            )
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: 24),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              FieldInfo(
                                'Mã QR:',
                                Icons.settings_ethernet,
                                user.code,
                              ),
                              FieldInfo(
                                'Mã số thẻ:',
                                Icons.featured_play_list,
                                user.cardNumber?.isNotEmpty == true ? user.cardNumber : "Chưa cập nhật",
                              ),
                              FieldInfo(
                                'Số CMND:',
                                Icons.assignment_ind,
                                user.idcard?.isNotEmpty == true ? user.idcard : "Chưa cập nhật",
                              ),
                              FieldInfo(
                                'Nơi cấp:',
                                Icons.location_on,
                                user.cardAddress?.isNotEmpty == true ? user.cardAddress : "Chưa cập nhật",
                              ),
                              FieldInfo(
                                'Ngày cấp:',
                                Icons.today,
                                user.idcardDate?.isNotEmpty == true ? user.idcardDate : "Chưa cập nhật",
                              ),
                              FieldInfo(
                                'Địa chỉ:',
                                Icons.location_on,
                                user.address?.isNotEmpty == true ? user.address : "Chưa cập nhật",
                              ),
                              // FieldInfo(
                              //   'Điểm quy đổi:',
                              //   Icons.stars,
                              //   user.pointAfter.toString(),
                              // ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }
}
