import 'package:loctroi/utils/app_store.dart';

class GoogleMapScreen extends StatefulWidget {
  @override
  _GoogleMapScreenState createState() => _GoogleMapScreenState();
}

class _GoogleMapScreenState extends State<GoogleMapScreen> {
  ApplicationBloc _appBloc;
  Set<Marker> markers = Set();

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    super.initState();

    // Encode bitmap and build marker
    Future.delayed(Duration(seconds: 1), () {
      _buildMarker().then((v) {}).catchError((e) {
        print(e);
      });
    });
  }

  GoogleMapController _controller;

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(10.7894763, 106.7009849),
//    zoom: 4.0,
  );

  @override
  Widget build(BuildContext context) {
    ScaleUtil.instance = ScaleUtil.getInstance()..init(context);
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Padding(
            padding: EdgeInsets.all(14.0),
            child: Image.asset(
              "assets/images/ic_back.png",
              fit: BoxFit.cover,
              width: 24,
              height: 24,
            ),
          ),
        ),
        automaticallyImplyLeading: true,
        iconTheme: IconThemeData(color: ptPrimaryColor(context)),
        brightness: Brightness.light,
        title: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Liên hệ', style: ptHeadline(context).copyWith(color: Colors.red, fontWeight: FontWeight.bold)),
            Text('www.benhviencayanqua.vn',
                style: ptSubhead(context).copyWith(color: ptPrimaryColor(context), fontWeight: FontWeight.bold)),
          ],
        ),
        elevation: 2,
        backgroundColor: Colors.white,
        bottom: PreferredSize(
            child: Container(
              color: ptPrimaryColor(context),
              height: 8,
            ),
            preferredSize: Size.fromHeight(8)),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
//              Routing().navigate2(context, CreatQuestionScreen());
            },
            child: Padding(
              padding: EdgeInsets.all(3),
              child: ExtendedImage.asset(
                "assets/images/question.png",
              ),
            ),
          ),
          // IconButton(
          //   icon: Icon(Icons.web),
          //   onPressed: () {
          //     openWebBrowerhURL('http://www.loctroi.vn/lien-he');
          //   },
          // )
        ],
      ),
      // floatingActionButtonLocation: FloatingActionButtonLocation.miniStartTop,
      // floatingActionButton: FloatingActionButton(
      //   heroTag: "btn1",
      //   child: Icon(Icons.refresh),
      //   onPressed: () {
      //     setState(() {
      //       // markerlocationStream.stream.listen((onData) {
      //       //   setState(() {
      //       //     currenLat = onData.latitude;
      //       //     currenLng = onData.longitude;
      //       //   });
      //       // });
      //       markers = List.from(markers);
      //     });
      //   },
      // ),
      body: Column(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  top: ScaleUtil.getInstance().setHeight(10),
                  bottom: ScaleUtil.getInstance().setHeight(10),
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                    top: BorderSide(
                      color: ptPrimaryColor(context),
                      width: ScaleUtil.getInstance().setHeight(8),
                    ),
                    bottom: BorderSide(
                      color: ptPrimaryColor(context),
                      width: ScaleUtil.getInstance().setHeight(8),
                    ),
                  ),
                ),
                child: Container(
                  height: ScaleUtil.getInstance().setHeight(40),
                  width: deviceWidth(context),
                  padding: EdgeInsets.symmetric(horizontal: ScaleUtil.getInstance().setHeight(16)),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft,
                          colors: [ptPrimaryColor(context), HexColor('#3FB424')])),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Trụ sở chính',
                      style: ptTitle(context).copyWith(color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: ScaleUtil.getInstance().setWidth(16)),
                child: Text(
                  'Bệnh viên cây ăn quả trung tâm',
                  style: ptTitle(context).copyWith(color: ptPrimaryColor(context)),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: ScaleUtil.getInstance().setWidth(16),
                    right: ScaleUtil.getInstance().setWidth(16),
                    top: ScaleUtil.getInstance().setHeight(8),
                    bottom: ScaleUtil.getInstance().setHeight(10)),
                child: Text(
                  'KM1975 QL1A, ấp Đông, xã Long Định, huyện Châu Thành, Tiền Giang',
                  style: ptSubtitle(context),
                ),
              ),
            ],
          ),
          Expanded(
            child: Stack(
              children: <Widget>[
                Container(
                  width: deviceWidth(context),
                  height: deviceHeight(context),
                  child: _buildGoogleMap(),
                ),
                Positioned(
                    bottom: 0.0,
                    left: 0.0,
                    right: 0.0,
                    child: Image.asset(
                      "assets/images/home_bottom.png",
                      width: deviceWidth(context),
                      fit: BoxFit.cover,
                    ))
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildGoogleMap() {
    return GoogleMap(
      mapType: MapType.normal,
      initialCameraPosition: _kGooglePlex,
      onMapCreated: (GoogleMapController controller) {
        _controller = controller;
        _moveToHCM();
      },
      markers: markers,
    );
  }

  Future _buildMarker() async {
//    final ImageConfiguration imageConfiguration = createLocalImageConfiguration(context, size: Size(30.0, 30.0));
//    final bitmap = await BitmapDescriptor.fromAssetImage(imageConfiguration, 'assets/images/app_logo_doctor.png');
    _appBloc.getHospitals.forEach((hospital) {
      print(hospital.toJson());
      markers.add(
        Marker(
//          anchor: AnchorPos.align(AnchorAlign.center),
//            icon: bitmap,
            markerId: MarkerId(hospital.id),
            position: LatLng(hospital.lat, hospital.long),
            onTap: () => _touchToMarker(hospital)),
      );
    });

    setState(() {});
  }

  _moveToHCM() {
    _controller.moveCamera(
      CameraUpdate.newLatLngZoom(
        const LatLng(10.7894763, 106.7009849),
        7.0,
      ),
    );
  }

  _touchToMarker(LHospital hospital) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text(
                hospital?.name ?? '',
                style: Theme.of(context).textTheme.title,
              ),
              content: Text(
                hospital?.address ?? '',
                style: Theme.of(context).textTheme.subtitle,
              ),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                    MapUtils.openMap(hospital.lat, hospital.long);
                  },
                  child: Text('Mở bản đồ'),
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Đóng'),
                ),
              ],
            ));
    print('hospital ${hospital.name} ');
    print('hospital vi tri ${hospital.lat} ${hospital.long}');
  }
}
