import 'package:loctroi/utils/app_store.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'login/update_info_login_screen.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen();

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  ApplicationBloc _appBloc;
  NetworkCheck _connectivity = NetworkCheck.instance;
  bool isConnect = false;
  StreamSubscription _netStreamSub;

  @override
  void initState() {
    timeago.setLocaleMessages('vi', timeago.ViMessages());
    _connectivity.initialise();

    // Listen network change
    _netStreamSub = _connectivity.networkChangedEvent.listen((online) async {
      setState(() {
        this.isConnect = online;
      });
      if (online) {
        await prepareData();
      }
    });
    super.initState();
  }

  Future prepareData() async {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);

    await _appBloc.loadRemoteConfig();

//    await _appBloc.loadBaseData();
    await _cacheDeviceId();
//    _setupStateStream = _appBloc.setupStateEvent.listen(
//      (s) async {
//        if (s == 'done') {
    try {
      // make auto login or show login page
      AccessStatus tokenState = await LoopBackAuth().loadAccessToken();
      if (tokenState != AccessStatus.TOKEN_VALID) {
        _openLoginScreen();
      } else {
        Farmer user = await UserService().getProfile(LoopBackAuth().userId);
        _appBloc.getAuthBloc().updateUserAction(user);
        // if (user.isClientFilledInfo) {
        Routing().navigate2(context, HomeScreen(), routeName: '/homepage');
        // } else {
        // Routing().navigate2(context, UpdateInfoLoginScreen(), replace: true);
        // }
      }
    } catch (e) {
      LoopBackAuth().clear();
      _openLoginScreen();
    }
    return Future;
  }

  _cacheDeviceId() async {
    String deviceId;
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await DeviceInfoPlugin().androidInfo;
      deviceId = androidInfo.androidId;
    } else {
      IosDeviceInfo iOSInfo = await DeviceInfoPlugin().iosInfo;
      deviceId = iOSInfo.identifierForVendor;
    }
    _appBloc.setDeviceIdAction(deviceId);
  }

  _openLoginScreen() {
    Routing().navigate2(context, LoginScreen(), replace: true);
  }

  @override
  void dispose() {
    _netStreamSub.cancel();
    _connectivity.disposeStream();
//    _setupStateStream.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScaleUtil.instance = ScaleUtil(width: 375, height: 667)..init(context);

    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage('assets/images/bg_splash.png'), fit: BoxFit.cover),
            ),
            // child: BackdropFilter(
            //   filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
            //   child: Container(
            //     decoration: BoxDecoration(color: ptPrimaryColor(context).withOpacity(0.1)),
            //   ),
            // ),
          ),
          Center(
            child: Container(
              width: deviceWidth(context),
              height: deviceHeight(context),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 200),
                    child: Image.asset(
                      'assets/images/app_logo_slogan.png',
                      fit: BoxFit.contain,
                      width: deviceWidth(context) * 0.6,
                      height: deviceWidth(context) * 0.6,
                    ),
                  ),
                  // Padding(
                  //   padding: EdgeInsets.only(top: 25),
                  //   child: Text(
                  //     "ỨNG DỤNG KẾT NỐI NÔNG GIA",
                  //     style: ptTitle(context).copyWith(fontWeight: FontWeight.bold, color: ptPrimaryColor(context)),
                  //   ),
                  // ),
                  isConnect
                      ? Container()
                      : Padding(
                          padding: EdgeInsets.only(top: 8.0),
                          child: RaisedButton(
                            onPressed: () async {
                              bool online = await _connectivity.check();
                              if (online) {
                                await prepareData();
                              } else {
                                Toast.show('Không có kết nối internet', context);
                              }
                            },
                            shape: RoundedRectangleBorder(
                                side: BorderSide(color: ptPrimaryColor(context), width: 4, style: BorderStyle.solid),
                                borderRadius: BorderRadius.circular(10)),
                            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                            child: Text(
                              "Thử kết nối lại".toUpperCase(),
                              style: ptBody1(context).copyWith(color: Colors.white),
                            ),
                          ),
                        )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
