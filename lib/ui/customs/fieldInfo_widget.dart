import 'package:loctroi/utils/app_store.dart';

Widget FieldInfo(String lable, IconData icon, String value) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(left: 4),
        child: Text(
          lable,
          style: TextStyle(
            fontSize: 16,
            color: HexColor(appColor),
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      Container(
        margin: EdgeInsets.only(top: 8, bottom: 16),
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.black.withOpacity(0.03),
          borderRadius: BorderRadius.all(Radius.circular(40)),
        ),
        child: Row(
          children: <Widget>[
            Opacity(
              opacity: 0.7,
              child: Icon(
                icon,
                color: HexColor(appColor),
              ),
            ),
            SizedBox(width: 16),
            Expanded(
              child: Text(
                value,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  color: HexColor(appColor),
                ),
              ),
            ),
          ],
        ),
      ),
    ],
  );
}
