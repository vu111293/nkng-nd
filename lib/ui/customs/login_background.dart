import 'package:loctroi/utils/app_store.dart';

class LoginBackground extends StatelessWidget {
  final showIcon;
  final image;
  LoginBackground({this.showIcon = true, this.image});

  Widget topHalf(BuildContext context) {
    ScaleUtil.instance = ScaleUtil(width: 375, height: 667)..init(context);

    return Flexible(
      flex: 2,
      child: ClipPath(
        clipper: ArcClipper(),
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  ptPrimaryColor(context),
                  ptPrimaryColor(context).withAlpha(90),
                ],
              )),
            ),
            showIcon
                ? Container(
                    alignment: Alignment.topCenter,
                    padding: EdgeInsets.only(top: ScaleUtil.getInstance().setHeight(40)),
                    child: Image.asset(
                      "assets/images/app_logo.png",
                      width: ScaleUtil.getInstance().setWidth(50.0),
                      height: ScaleUtil.getInstance().setWidth(50.0),
                    ),
                  )
                : Container(
                    alignment: Alignment.topCenter,
                    padding: EdgeInsets.only(top: ScaleUtil.getInstance().setHeight(40)),
                  )
          ],
        ),
      ),
    );
  }

  final bottomHalf = Flexible(
    flex: 3,
    child: Container(),
  );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[topHalf(context), bottomHalf],
    );
  }
}
