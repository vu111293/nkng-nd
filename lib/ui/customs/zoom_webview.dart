import 'package:loctroi/utils/app_store.dart';
import 'package:html/dom.dart' as dom;

class ZoomWebView extends StatefulWidget {
  String html;
  double paddingBottom;
  ZoomWebView({Key key, this.html = '', this.paddingBottom = 10}) : super(key: key);

  _ZoomWebViewState createState() => _ZoomWebViewState();
}

class _ZoomWebViewState extends State<ZoomWebView> {
  final _htmlFontBehavior = BehaviorSubject<double>.seeded(22);
  double fontSlider = 22;
  static final double MIN_FONT = 22;
  static final double MAX_FONT = 41;

  ScrollController scrollController;

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    scrollController = ScrollController();
    scrollController.addListener(() => setState(() {}));
  }

  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
          child: StreamBuilder<double>(
              stream: _htmlFontBehavior.stream,
              builder: (context, snapshot) {
                if (snapshot.hasData == false) return Container();
                return Html(
                  onImageTap: (image) {
                    Routing().navigate2(
                        context,
                        PicSwiper(0, [
                          image ?? 'https://picolas.de/wp-content/uploads/2015/12/picolas-picture-not-available.jpg'
                        ]));
                  },
                  // shrinkToFit: true,
                  padding: EdgeInsets.only(left: 16, right: 16, bottom: 100),
                  backgroundColor: Colors.white70,
                  defaultTextStyle: TextStyle(fontSize: snapshot.data),
                  customTextAlign: (dom.Node node) {
                    String alignAttr = node.attributes?.keys?.contains('style') == true ? node.attributes['style'] : '';
                    if (alignAttr.isNotEmpty) {
                      if (alignAttr.contains('text-align:center'))
                        return TextAlign.center;
                      else if (alignAttr.contains('text-align:left'))
                        return TextAlign.left;
                      else if (alignAttr.contains('text-align:right'))
                        return TextAlign.right;
                      else
                        return TextAlign.justify;
                    } else {
                      return TextAlign.justify;
                    }
                  },
                  data: widget.html ?? """</br></br></br></br><center>Đang tải...</center>""",
                );
              }),
        ),
        Positioned(
            bottom: widget.paddingBottom,
            left: 50.0,
            right: 50.0,
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 12.0),
                decoration: BoxDecoration(color: Colors.white60, borderRadius: BorderRadius.all(Radius.circular(10.0))),
                child: Row(children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      if (fontSlider > 17) {
                        setState(() {
                          fontSlider = fontSlider - 5;
                        });
                        _htmlFontBehavior.sink.add(fontSlider - 5);
                      }
                    },
                    child: Icon(Icons.zoom_out, size: 30, color: Colors.black54),
                  ),
                  Expanded(
                    child: Slider(
                        activeColor: ptPrimaryColor(context),
                        inactiveColor: ptPrimaryColor(context).withOpacity(0.25),
                        divisions: 5,
                        min: 17,
                        max: 47,
                        value: fontSlider,
                        onChanged: (v) {
                          setState(() {
                            fontSlider = v;
                          });
                          _htmlFontBehavior.sink.add(fontSlider);
                        }),
                  ),
                  GestureDetector(
                    onTap: () {
                      if (fontSlider < 47) {
                        setState(() {
                          fontSlider = fontSlider + 5;
                        });
                        _htmlFontBehavior.sink.add(fontSlider + 5);
                      }
                    },
                    child: Icon(Icons.zoom_in, size: 30, color: Colors.black54),
                  ),
                ])))
      ],
    );
  }
}
