import 'package:loctroi/utils/app_store.dart';

class BaseScreen extends StatelessWidget {
  final Widget bottomBar;
  final Widget body;

  BaseScreen({this.bottomBar, this.body});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(children: <Widget>[
        Expanded(child: SingleChildScrollView(child: body ?? Container())),
        bottomBar ?? Container()
      ]),
    );
  }
}
