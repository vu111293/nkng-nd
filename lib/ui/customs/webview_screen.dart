import 'package:loctroi/utils/app_store.dart';

class WebViewScreen extends StatefulWidget {
  String url;
  WebViewScreen({Key key, this.url}) : super(key: key);

  WebViewScreenState createState() => WebViewScreenState();
}

class WebViewScreenState extends State<WebViewScreen> {
  final _key = UniqueKey();
  num _stackToView = 1;

  WebViewController _controller;
  final Set<Factory> gestureRecognizers = [
    Factory(() => EagerGestureRecognizer()),
  ].toSet();

  @override
  void initState() {
    super.initState();
    _stackToView = 1;
  }

  @override
  Widget build(BuildContext context) {
    return IndexedStack(
      index: _stackToView,
      children: <Widget>[
        WebView(
          key: _key,
          gestureRecognizers: gestureRecognizers,
          initialUrl: widget.url,
          debuggingEnabled: true,
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            _controller = webViewController;
          },
          onPageFinished: (finish) {
            setState(() {
              _stackToView = 0;
            });
          },
        ),
        Container(
          color: Colors.white,
          child: Center(
              child: Text(
            'Đang tải...',
            style: ptTitle(context),
          )),
        )
      ],
    );
  }
}
