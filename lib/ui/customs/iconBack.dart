import 'package:loctroi/utils/app_store.dart';

class IconBack extends StatelessWidget {
  final bool isBackground;
  IconBack({this.isBackground = false});

  @override
  Widget build(BuildContext context) {
    return isBackground
        ? GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(40)),
                  color: Colors.white54,
                ),
                margin: EdgeInsets.only(top: 8.0, bottom: 8, left: 16, right: 8),
                child: Icon(
                  Icons.chevron_left,
                  color: ptPrimaryColor(context),
                  size: 32,
                )),
          )
        : GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.chevron_left,
              color: Colors.white,
              size: 32,
            ),
          );
  }
}
