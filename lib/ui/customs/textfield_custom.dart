import 'package:flutter/material.dart';

typedef OnButtonTap();
typedef OnSubmit(String value);
typedef OnValidate(String value);
typedef OnHitEnter(String value);

class TextFieldCustom extends StatefulWidget {
  final String hint;
  final String label;
  final TextEditingController controller;
  final IconData icon;
  final EdgeInsets padding;
  final bool isPassword;
  final Widget leading;
  final TextInputType inputType;
  final String error;
  final bool isLoading;
  final TextAlign align;
  final OnSubmit submit;
  final bool enable;
  final OnValidate validate;
  final OnHitEnter hitEnter;
  final TextInputAction inputAction;
  final FocusNode focusNode;

  TextFieldCustom(
      {this.enable,
      this.submit,
      this.align = TextAlign.left,
      this.hint = '',
      this.label = '',
      this.controller,
      this.icon,
      this.leading,
      this.inputType = TextInputType.text,
      this.padding = EdgeInsets.zero,
      this.isPassword = false,
      this.error = null,
      this.isLoading = false,
      this.validate,
      this.hitEnter,
      this.inputAction = TextInputAction.done,
      this.focusNode});

  @override
  State<StatefulWidget> createState() {
    return _TextFieldCustomState();
  }
}

class _TextFieldCustomState extends State<TextFieldCustom> {
  @override
  Widget build(BuildContext context) {
    Widget leading = widget.leading;
    if (leading == null && widget.icon != null) {
      leading = Icon(widget.icon);
//      leading = Padding(padding: EdgeInsets.symmetric(horizontal: 20.0), child: Icon(widget.icon));
    }

    Widget trailing = widget.isLoading
        ? Container(
            width: 20.0,
            child: Center(
              child: SizedBox(width: 15.0, height: 15.0, child: CircularProgressIndicator()),
            ))
        : Container(
            width: 0.0,
          );

    return TextFormField(
        validator: (text) {
          if (widget.validate != null) {
            return widget.validate(text);
          }
          return null;
        },
        enabled: widget.enable != null ? widget.enable : widget.isLoading == false,
        obscureText: widget.isPassword,
        controller: widget.controller,
        textAlign: widget.align,
        focusNode: widget.focusNode,
        textInputAction: widget.inputAction,
        style: TextStyle(fontSize: 17.0, color: Colors.black54),
        keyboardType: widget.inputType,
        onSaved: widget.submit,
        onFieldSubmitted: widget.hitEnter,
        decoration: InputDecoration(
          errorText: widget.error,
          // contentPadding: EdgeInsets.only(top: 18.0),
          prefixIcon: leading,
          suffixIcon: trailing,
          hintText: widget.hint,
          hintStyle: TextStyle(color: Color(0xFF8A8A8F), fontSize: 15.0),
          enabledBorder: const UnderlineInputBorder(
            // width: 0.0 produces a thin "hairline" border
            borderSide: BorderSide(color: Color(0xffc8c7cc), width: 0.5),
          ),
          labelText: widget.label,
          contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          labelStyle: Theme.of(context).textTheme.caption.copyWith(fontSize: 15.0),
          fillColor: Theme.of(context).primaryColor,
          focusedBorder: OutlineInputBorder(
            // borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(color: Theme.of(context).primaryColor, width: 1.0),
          ),
          // enabledBorder: OutlineInputBorder(
          //   // borderRadius: BorderRadius.circular(10.0),
          //   borderSide: BorderSide(color: HexColor("#E2E0E0"), width: 2.0),
          // ),
//            border: UnderlineInputBorder(
//                borderRadius: BorderRadius.all(Radius.circular(0.0)),
//                borderSide: BorderSide(color:ptPrimaryColor(context), width: 0.5, style: BorderStyle.solid))
//          border: OutlineInputBorder(borderSide: BorderSide(color: Color(0xFFC8C7CC), width: 0.1, style: BorderStyle.solid)),
        ));
  }
}
