import 'package:loctroi/utils/app_store.dart';

class BaseHeader extends StatefulWidget {
  final String title;
  final bool showNotification;
  final bool showBack;
  final bool onlyBack;
  final Widget body;
  BaseHeader({this.title = '', this.showNotification = true, this.showBack = true, this.onlyBack = false, this.body});

  @override
  State<StatefulWidget> createState() {
    return BaseHeaderState();
  }
}

class BaseHeaderState extends State<BaseHeader> {
  ApplicationBloc _appBloc;

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        appBar: AppBar(
          elevation: 5,
          backgroundColor: ptPrimaryColor(context),
          brightness: Brightness.light,
          leading: widget.showBack ? IconBack() : Container(),
          title: Text(
            widget.title,
          ),
          actions: widget.onlyBack
              ? <Widget>[]
              : <Widget>[
                  widget.showNotification
                      ? Stack(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(top: 4),
                              child: IconButton(
                                splashColor: Colors.transparent,
                                icon: Icon(
                                  Icons.notifications,
                                  color: Colors.white,
                                ),
                                onPressed: () {
                                  Routing().navigate2(context, NotificationScreen()).then((_) {
                                    _appBloc.getAuthBloc().getUnreadCount();
                                  });
                                },
                              ),
                            ),
                            StreamBuilder<int>(
                              stream: _appBloc.getAuthBloc().unreadStream,
                              builder: (context, snapshot) {
                                int counter = snapshot.hasData ? snapshot.data : 0;
                                return Positioned(
                                  top: 5,
                                  right: 5,
                                  child: counter == 0
                                      ? Container()
                                      : Container(
                                          padding: EdgeInsets.symmetric(vertical: 3, horizontal: 6),
                                          decoration: BoxDecoration(
                                              color: Colors.red, borderRadius: BorderRadius.all(Radius.circular(10))),
                                          child: Text(
                                            '$counter',
                                            style: TextStyle(
                                                color: Colors.white, fontSize: 11.0, fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                );
                              },
                            ),
                          ],
                        )
                      : Container(),
                  Container(
                    margin: EdgeInsets.only(left: 8, right: 16),
                    child: GestureDetector(
                      onTap: () {
                        Routing().navigate2(context, MenuScreen(), routeName: '/menupage');
                      },
                      child: Icon(
                        Icons.settings,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
          centerTitle: true,
        ),
        body: widget.body ?? Container(),
      ),
    );
  }

  showBanner(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Stack(
          children: <Widget>[
            Center(
                child: Stack(
              children: <Widget>[
                Card(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                  child: Container(
                    width: deviceWidth(context) - 64,
                    height: 370,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                          child: Image.network('https://dautucophieu.net/wp-content/uploads/2017/03/loctroigroup.jpg'),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 9, left: 12, right: 12),
                          child: Text(
                            'Mini Game: QUAY ĐỀU TAY - QUÀ TẶNG TRAO NGAY',
                            style: ptTitle(context).copyWith(color: ptPrimaryColor(context)),
                            maxLines: 2,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
                          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                            Text(
                              'Từ 03/03/2020 đến ngày 03/06/2020 tất cả người dùng app Lộc Trời đều được tham gia chương trình quay số trúng thưởng với hà...',
                              style: TextStyle(
                                fontSize: 13,
                                color: HexColor(appText2),
                              ),
                              maxLines: 3,
                            ),
                          ]),
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: RaisedButton(
                            shape: StadiumBorder(),
                            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 36),
                            onPressed: () {},
                            child: Text('Xem chi tiết'),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: 10,
                  right: 10,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: CircleAvatar(
                      backgroundColor: Colors.white54,
                      child: Icon(
                        Icons.close,
                        color: Colors.black,
                        size: 20,
                      ),
                    ),
                  ),
                ),
              ],
            ))
          ],
        );
      },
    );
  }
}
