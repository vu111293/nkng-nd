import 'package:loctroi/utils/app_store.dart';

class DoctorFilterQuestion extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DoctorFilterQuestionState();
  }
}

class _DoctorFilterQuestionState extends State<DoctorFilterQuestion> {
  ApplicationBloc _appBloc;
  LHospital hospitalValue;
  List<LHospital> _hospitals;

  LDoctor doctorValue;
  List<LDoctor> _doctors;

  LTree treeValue;
  List<LTree> _trees;

  bool _myQuestion;
  bool _noneAnswer;

  @override
  void initState() {
    _appBloc = BlocProvider.of<ApplicationBloc>(context);

    _hospitals = List.from(_appBloc.getHospitals);
    _hospitals.insert(0, LHospital(name: 'Tất cả', id: '', doctors: []));

    _doctors = List.from(_appBloc.getDoctors);
    _doctors.insert(0, LDoctor(fullName: 'Tất cả', id: ''));
    doctorValue = _doctors.first;

    _trees = List.from(_appBloc.getTrees);
    _trees.insert(0, LTree(name: 'Tất cả', id: ''));
    treeValue = _trees.first;

    _myQuestion = false;
    _noneAnswer = false;

    super.initState();

    setState(() {
      setup();
    });
  }

  setup() {
    // load previous filter
    DoctorQuestionFilter filter = _appBloc.getDoctorFilterLatest;
    if (filter != null) {
      hospitalValue = _hospitals.firstWhere((item) => item.id == filter.hospitalId, orElse: () => _hospitals.first);
      if (hospitalValue != null && hospitalValue.id?.isNotEmpty == true) {
        _doctors = List.from(hospitalValue.doctors);
        _doctors.insert(0, LDoctor(fullName: 'Tất cả', id: ''));
      }
      doctorValue = _doctors.firstWhere((item) => item.id == filter.doctorId, orElse: () => _doctors.first);
      treeValue = _trees.firstWhere((item) => item.id == filter.plantId, orElse: () => _trees.first);
      _myQuestion = filter.myQuestion;
      _noneAnswer = filter.noneAnswer;
    }
  }

  _applyFilter() {
    DoctorQuestionFilter filter = _appBloc.getDoctorFilterLatest;
    filter = filter.copyWith(
      hospitalId: hospitalValue.id,
      doctorId: doctorValue.id,
      plantId: treeValue.id,
      myQuestion: _myQuestion,
      noneAnswer: _noneAnswer,
    );
    _appBloc.filterDoctorQuestion(filter);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Container(
        margin: EdgeInsets.only(top: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0)),
          color: Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0)),
                color: ptPrimaryColor(context),
              ),
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                      child: Text(
                    'Bộ lọc',
                    style: ptSubtitle(context).copyWith(color: Colors.white),
                  )),
                  Text(
                    '   |   ',
                    style: ptSubtitle(context).copyWith(color: Colors.white),
                  ),
                  InkWell(
                    onTap: _applyFilter,
                    child: Container(
                        child: Text(
                          'Áp dụng',
                          style: ptSubtitle(context).copyWith(color: Colors.white),
                        ),
                        padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 14.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          border: Border.all(width: 1, color: Colors.white),
                        )),
                  ),
                ],
              ),
            ),
            Container(
                padding: EdgeInsets.symmetric(vertical: 12.0),
                child: Row(children: <Widget>[
                  Switch(
                    activeColor: ptPrimaryColor(context),
                    value: _myQuestion,
                    onChanged: (v) {
                      setState(() {
                        _myQuestion = v;
                      });
                    },
                  ),
                  Expanded(
                      child: Text('Chỉ hiển thị câu hỏi của tôi',
                          style: ptSubtitle(context).copyWith(color: ptPrimaryColor(context))))
                ])),
            Container(
                padding: EdgeInsets.symmetric(vertical: 12.0),
                child: Row(children: <Widget>[
                  Switch(
                    activeColor: ptPrimaryColor(context),
                    value: _noneAnswer,
                    onChanged: (v) {
                      setState(() {
                        _noneAnswer = v;
                      });
                    },
                  ),
                  Expanded(
                      child: Text('Câu hỏi chưa trả lời',
                          style: ptSubtitle(context).copyWith(color: ptPrimaryColor(context))))
                ])),
            filterItem<LHospital>(context, "Chọn trung tâm", hospitalValue, _hospitals, onChanged: (v) {
              setState(() {
                hospitalValue = v;
                _doctors = List.from(hospitalValue.doctors);
                _doctors.insert(0, LDoctor(fullName: 'Tất cả', id: ''));
                doctorValue = _doctors.first;
              });
            }),
            filterItem<LDoctor>(context, "Chọn bác sĩ", doctorValue, _doctors, onChanged: (v) {
              setState(() {
                doctorValue = v;
              });
            }),
            filterItem<LTree>(context, "Chọn cây trồng", treeValue, _trees, onChanged: (v) {
              setState(() {
                treeValue = v;
              });
            }),
          ],
        ),
      ),
    );
  }

  Widget filterItem<T>(BuildContext context, String title, T dropdownValue, List<T> listItem, {Function(T) onChanged}) {
    return InkWell(
      onTap: () {
        // updateSortCheck(check);
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 10.0, bottom: 10, left: 20, right: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  maxLines: 1,
                  style: ptSubtitle(context).copyWith(color: ptPrimaryColor(context)),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      border: Border.all(width: 1, color: HexColor('#00000005'))),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    child: DropdownButton<T>(
                      value: dropdownValue,
                      isExpanded: true,
                      icon: Icon(Icons.arrow_drop_down),
                      iconSize: 30,
                      style: TextStyle(color: HexColor(appText)),
                      underline: Container(
                        height: 0,
                        color: Colors.deepPurpleAccent,
                      ),
                      onChanged: onChanged,
                      items: listItem.map<DropdownMenuItem<T>>((T value) {
                        return DropdownMenuItem<T>(
                          value: value,
                          child: Text(value.toString()),
                        );
                      }).toList(),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Divider(),
        ],
      ),
    );
  }
}
