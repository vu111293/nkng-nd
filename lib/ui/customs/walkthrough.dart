import 'package:loctroi/utils/app_store.dart';

class Walkthrough extends StatefulWidget {
  final String title;
  final String subTitle;
  final Color backgroundColor;
  final String content;
  final imageUrl;
  // final imageIcon;
  final imagecolor;

  Walkthrough(
      {this.title,
      this.backgroundColor,
      this.subTitle,
      this.content,
      this.imageUrl,
      // this.imageIcon,
      this.imagecolor = Colors.redAccent});

  @override
  WalkthroughState createState() {
    return WalkthroughState();
  }
}

class WalkthroughState extends State<Walkthrough> with SingleTickerProviderStateMixin {
  Animation animation;
  AnimationController animationController;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    animation =
        Tween(begin: -250.0, end: 0.0).animate(CurvedAnimation(parent: animationController, curve: Curves.easeInOut));

    animation.addListener(() => setState(() {}));

    animationController.forward();
  }

  @override
  void dispose() {
    super.dispose();
    animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: deviceWidth(context),
        decoration: BoxDecoration(color: widget.backgroundColor),
        // padding: EdgeInsets.all(20.0),

        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Material(
          animationDuration: Duration(milliseconds: 500),
          elevation: 2.0,
          borderRadius: BorderRadius.all(
            Radius.circular(5.0),
          ),
          child: Container(
              padding: EdgeInsets.only(top: 40, bottom: 20, left: 20, right: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Image.network(
                    widget.imageUrl,
                    width: 150.0,
                    height: 150,
                    fit: BoxFit.cover,
                  ),
                  // Image.asset(widget.imageUrl, width: 130.0),
                  SizedBox(height: 20),
                  Transform(
                    transform: Matrix4.translationValues(animation.value, 0.0, 0.0),
                    child: Text(widget.title,
                        maxLines: 2, textAlign: TextAlign.center, style: ptHeadline(context).copyWith(fontSize: 22.0)),
                  ),
                  // Transform(
                  //   transform: Matrix4.translationValues(animation.value, 0.0, 0.0),
                  //   child: Text(
                  //     widget.subTitle,
                  //     textAlign: TextAlign.center,
                  //     style: Theme.of(context).textTheme.title.copyWith(height: 1.5, fontWeight: FontWeight.bold),
                  //   ),
                  // ),
                  SizedBox(height: 10),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Transform(
                          transform: Matrix4.translationValues(animation.value, 0.0, 0.0),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20.0),
                            child: Text(widget.content,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 20,
                                softWrap: true,
                                textAlign: TextAlign.center,
                                style: Theme.of(context).textTheme.body2),
                          )),
                    ),
                  ),
                ],
              )),
        ));
  }
}
