import 'package:loctroi/utils/app_store.dart';

import 'app.dart';

void main() {
  // Set `enableInDevMode` to true to see reports while in debug mode
  // This is only to be used for confirming that reports are being
  // submitted as expected. It is not intended to be used for everyday
  // development.
  Crashlytics.instance.enableInDevMode = const bool.fromEnvironment('dart.vm.product') == true;
  GetIt.instance.registerLazySingleton<AppConfig>(() => AppConfig());

  // Pass all uncaught errors from the framework to Crashlytics.
  FlutterError.onError = Crashlytics.instance.recordFlutterError;
  runZoned<Future<void>>(() async {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light
        .copyWith(statusBarColor: Colors.transparent, statusBarIconBrightness: Brightness.dark));
    runApp(MyApp(
      appConf: AppConfig(appName: 'Kết Nối Nông Gia - Nông Dân', appType: AppType.FARMER),
    ));
  }, onError: Crashlytics.instance.recordError);
}
